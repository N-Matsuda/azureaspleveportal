﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebApplication.Models
{
    //データテーブル作成クラス ゲートウェイユニット、モバイルルーターどちらから取得した在庫データも取り出せるようにするクラス
    public class DataUnitTable
    {
        private DataTable DataUnitDT;
        private string cmpnyname;
        private const int SKKCodeColNo = 0;     //SKKコード
        private const int DataSw = 1;           //在庫データ書き込みSW
        private const int DataBuf1 = 2;         //在庫データ書き込みBuffer1
        private const int DataBuf2 = 3;         //在庫データ書き込みBuffer1
        private const int SSNameColNo = 4;     //SS名


        //コンストラクター
        public DataUnitTable()
        {
            DataTableCtrl.InitializeTable(DataUnitDT);
        }

        //指定した会社名で登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTable(string cmpny)
        {
            try
            {
                //string dbpath;
                cmpnyname = cmpny;
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string sqlstr = "";
                if (cmpny == "*")
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable";
                else
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable WHERE 会社名=(N'" + cmpny + "')";
                DBCtrl.ExecSelectAndFillTable(sqlstr, DataUnitDT);

#if false //ConnectPortを使用する場合
                if (cmpny == "*")
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable";
                else
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable WHERE 会社名=(N'" + cmpny + "')";
                DataTable tempdt = new DataTable();
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(tempdt);
                for (int i = 0; i < tempdt.Rows.Count; i++)
                {
                    DataUnitDT.ImportRow(tempdt.Rows[i]);
                }
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードの配列で登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTableSkkcodes(string[] skkcodes)
        {
            try
            {
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;
                foreach (string idc in skkcodes)
                {
#if true
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable WHERE SKKコード='" + idc + "'";
                    dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                    dAdp.Fill(DataUnitDT);
#else //connectportも仕様する場合
                    if (idc.StartsWith("M") == false)
                    {
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable WHERE SKKコード='" + idc + "'";
                        dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                        DataTable tempdt = new DataTable();
                        dAdp.Fill(tempdt);
                        for (int i = 0; i < tempdt.Rows.Count; i++)
                        {
                            DataUnitDT.ImportRow(tempdt.Rows[i]);
                        }
                    }
                    else
                    {
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM r_15180_skk.MobileRouterTable WHERE SKKコード='" + idc + "'";
                        dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                        DataTable tempdt = new DataTable();
                        dAdp.Fill(tempdt);
                        for (int i = 0; i < tempdt.Rows.Count; i++)
                        {
                            DataUnitDT.ImportRow(tempdt.Rows[i]);
                        }
                    }
#endif
                }
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードで登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;
#if true
                sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable WHERE SKKコード='" + skkcode + "'";
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitDT);
#else //ConnectPortも使用する場合
　              if (skkcode.StartsWith("M") == false)
                {
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable WHERE SKKコード='" + skkcode + "'";
                        dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                        dAdp.Fill(DataUnitDT);
               }
               else
               {
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM r_15180_skk.MobileRouterTable WHERE SKKコード='" + skkcode + "'";
                        dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                        dAdp.Fill(DataUnitDT);
                }
#endif
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //SKKコードの列(GWU, モバイルルーターのワイルドカード指定 4桁:相光石油 M012* A060*)からテーブルを開く
        //public void OpenTableSkkcodesGwMbl(string gwc, string mblc, string uname)
        public void OpenTableSkkcodesMbl(string mblc)
        {
            try
            {
                //int strlen = mblc.Length;
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;

                sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM MobileRouterTable WHERE SKKコード LIKE '" + mblc + "%'";
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitDT);

#if false       //ConnectPortを使用する場合
                sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名 FROM ConnectPortTable WHERE SKKコード LIKE '" + gwc.Substring(0, 4) + "%'";
                DataTable tempdt = new DataTable();
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(tempdt);
                for (int i = 0; i < tempdt.Rows.Count; i++)
                {
                    DataUnitTempDT.ImportRow(tempdt.Rows[i]);
                }
#endif
                cn.Close();
#if false       //指定ユーザーのみ参照可能なSS情報のみ残す場合
                if (username == "*")
                    return;
                SiraPtlUserProfile sust = new SiraPtlUserProfile();
                sust.OpenTableByUser(username);
                List<string> sslistar = sust.GetReferrableSkkcodeList(0);
                string[] sslist = sslistar.ToArray();
                int rownum = DataUnitDT.Rows.Count;
                int ssnum = sslist.Length;
                string skkcode;
                bool bfound;

                //sslist中のSKKコードにないConnectPortTableDTの列を削除する。
                for (int i = rownum - 1; i >= 0; i--)
                {
                    bfound = false;
                    skkcode = DataUnitDT.Rows[i][0].ToString().TrimEnd();
                    for (int j = ssnum - 1; j >= 0; j--)
                    {
                        if (skkcode == sslist[j])
                        {
                            bfound = true;
                            break;
                        }
                    }
                    if (bfound == false)
                        DataUnitDT.Rows.RemoveAt(i);
                }
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return DataUnitDT.Rows.Count;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }

        //テーブル先頭の在庫文字列を取得
        public string GetZaikoString()
        {
            string zstr = "";
            try
            {
                if (DataUnitDT != null)
                {
                    if ((int)DataUnitDT.Rows[0][DataSw] == 0)
                    {
                        zstr = (string)DataUnitDT.Rows[0][DataBuf2].ToString().TrimEnd();
                    }
                    else
                    {
                        zstr = (string)DataUnitDT.Rows[0][DataBuf1].ToString().TrimEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }

        //テーブル指定行の在庫文字列を取得
        public string GetZaikoStringLine(int lineno)
        {
            string zstr = "";
            try
            {
                if (DataUnitDT != null)
                {
                    if ((int)DataUnitDT.Rows[lineno][DataSw] == 0)
                    {
                        zstr = (string)DataUnitDT.Rows[lineno][DataBuf2].ToString().TrimEnd();
                    }
                    else
                    {
                        zstr = (string)DataUnitDT.Rows[lineno][DataBuf1].ToString().TrimEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }

        //テーブル指定行のSKKコードを取得
        public string GetSSCode(int lineno)
        {
            try
            {
                if ((DataUnitDT != null) && (lineno < DataUnitDT.Rows.Count))
                {
                    return DataUnitDT.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //施設名より相当するSKKコード取得
        public string GetSSCodeBySitename(string sitename)
        {
            string skkcode = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            skkcode = DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return skkcode;
        }

        //施設名より相当する施設番号取得
        public int GetSSCodeBySitenum(string sitename)
        {
            int sitenum=0;
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++, sitenum++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return sitenum;
        }
        //指定した文字を含む施設を検索する。
        public string SearchSite(string sword)
        {
            string findname = "";
            string sitename = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        sitename = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        int idx = sitename.IndexOf(sword);
                        if (idx >= 0)
                        {
                            findname = sitename;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return findname;
        }

        //指定した施設番号のテーブル上の行数を返す
        public int GetSiteNo(string sitename)
        {
            string sname = "";
            int idx = 1;
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++, idx++)
                    {
                        sname = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        if (sname == sitename)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return idx;
        }

        //施設名より相当するSKKコード取得
        public string GetSSName(int lineno)
        {
            try
            {
                if ((DataUnitDT != null) && (lineno < DataUnitDT.Rows.Count))
                {
                    return DataUnitDT.Rows[lineno][SSNameColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //SKKコードより相当するSS名取得
        public string GetSSNameBySkkcode(string skkcode)
        {
            string ssname = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (skkcode == DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd())
                        {
                            ssname = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        }
                    }
                    return ssname;
                }
                else
                {
                    return ssname;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return ssname;
            }
            return ssname;
        }

        //SKK名リスト取得
        public List<string> GetSiteList()
        {
            List<string> list = new List<string>();
            try
            {
                int cnt = DataUnitDT.Rows.Count;
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        list.Add(DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }
        //指定した施設番号のテーブル上の行数を返す
        public int GetSiteNumber(string sitename)
        {
            int num = 0;
            int i;
            try
            {
                if (DataUnitDT != null)
                {
                    for (i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            num = i;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return num;
        }

        //テーブルよりSKKコードリスト取得
        public List<string> GetSkkcodeList()
        {
            List<string> list = new List<string>();
            try
            {
                int cnt = DataUnitDT.Rows.Count;
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        list.Add(DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

    }
}