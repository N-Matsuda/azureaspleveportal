﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class FilePath
    {
        private string LogDataPath; //ログデータへのパス
        private string stDataPath = "/App_Data/";
        private string stLogFilePath = "/App_Data/Log/";
        private string FileDataPath;
        private string LC4DataPath;
        private string stLC4FilePath = "/App_Data/Leak/";
        //private string exename = "DigiDataCollector.exe";
        public FilePath()
        {
            // Assembly myAssembly = Assembly.GetEntryAssembly();
            string ApplicationPath = HttpContext.Current.Server.MapPath("./");
            FileDataPath = ApplicationPath + stDataPath;
            LogDataPath = ApplicationPath + stLogFilePath;
            LC4DataPath = ApplicationPath + stLC4FilePath;
        }

        public string GetDataPath()
        {
            return FileDataPath;
        }

        public string GetLogDataPath()
        {
            return LogDataPath;
        }

        public void SetLogDataPath(string LPath)
        {
            LogDataPath = LPath;
        }

        public string GetTodayLogDataPath(int lno)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return LogDataPath + "/LG" + (lno - 1).ToString() + dt.ToString("yyMMdd") + ".csv";
        }

        //指定の日のLog履歴ファイル名の取り出し
        public string GetDayLogDataPath(DateTime dt, int lno)
        {
            return LogDataPath + "/LG" + (lno - 1).ToString() + dt.ToString("yyMMdd") + ".csv";
        }
        //LC4パスの取り出し
        public string GetLC4DataPath()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return LC4DataPath + dt.ToString("yyyy");
        }
        //今月の漏えい点検ファイル名の取り出し
        public string GetThisMonthLC4DataPath(string SkkCode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return LC4DataPath + dt.ToString("yyyy") + "/L" + SkkCode + dt.ToString("yyMM") + ".csv";
        }

        //指定月の点検ファイル名の取り出し
        public string GetMonthLC4DataPath(string SkkCode, DateTime dt)
        {
            return LC4DataPath + dt.ToString("yyyy") + "/L" + SkkCode + dt.ToString("yyMM") + ".csv";
        }
        //指定月の点検ファイル名の取り出し
        public string GetMonthLC4DataWDayStrPath(string SkkCode, string DayStr)
        {
            return LC4DataPath + "20" + DayStr.Substring(0, 2) + "/L" + SkkCode + DayStr + ".csv";
        }

    }
}