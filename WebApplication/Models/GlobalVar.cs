﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    /// Web版レべビジョン
    /// プログラム全体で共有する変数、ルーチンを集めています。
    /// 
    public class GlobalVar
    {
        public static int NUMTANK = 20;
        public static int TankStatColNo = 5;
        public static int MaxTankNo = 9;


        //    public static string DBCONNECTION = "Data Source = skk-atgs.jp;Initial Catalog = r_15180_skk;User ID=r_15180_skk;Password = Eq6GwE8u";
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";

        public static string ServerPath { get; set; }
        public static bool loginreq = true;             //trueならばAccount認証必要
        public static bool demomode = false;
        public static bool accdeltest = true;

        public static int SHOWDATE = 15;    //履歴表示日数
        public static string Adminusername = "n-matsuda@showa-kiki.co.jp";
        public static string Subadminusername = "n-matsuda@showa-kiki.co.jp";

        //public static string EmgAdminusername = "emgmarket";
        //public static string Subadminusername = "emgmarket2";
        public static string DemoUsername = "skkdemo";
        public static string SinanenUsername1 = "sinaleve";

        //------ 対象会社名 -------------------
        public const int CompanySKKNo = 0;
#if false
        public const int CompanyAikoNo = 1;
        public const int CompanyEneosNo = 2;
        public const int CompanyShowakosanNo = 3;
        public const int CompanyTokosyojiNo = 4;

        public static string GetCompanyName(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompanyName;
                case CompanyAikoNo:
                    return AikoCompanyName;
                case CompanyEneosNo:
                    return EneCompanyName;
                case CompanyShowakosanNo:
                    return ShowakosanCompanyName;
                case CompanyTokosyojiNo:
                    return TokosyojiCompanyName;
            }
        }

        public static string GetDefWcSkkcode(int compno )
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompMBUSkkcode;
                case CompanyAikoNo:
                    return AikoCompMBUSkkcode;
                case CompanyEneosNo:
                    return EneCompMBUSkkcode;
                case CompanyShowakosanNo:
                    return ShowakosanCompMBUSkkcode;
                case CompanyTokosyojiNo:
                    return TokosyojiCompMBUSkkcode;
            }
        }

        public static string GetDefSkkcode( int compno )
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSkkcode1;
                case CompanyAikoNo:
                    return AikoDefSkkcode1;
                case CompanyEneosNo:
                    return EneDefSkkcode1;
                case CompanyShowakosanNo:
                    return ShowakosanDefSkkcode1;
                case CompanyTokosyojiNo:
                    return TokosyojiDefSkkcode1;
            }
        }

        public static string GetDefSitename( int compno )
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSiteName1;
                case CompanyAikoNo:
                    return AikoDefSiteName1;
                case CompanyEneosNo:
                    return EneDefSiteName1;
                case CompanyShowakosanNo:
                    return ShowakosanDefSiteName1;
                case CompanyTokosyojiNo:
                    return TokosyojiDefSiteName1;
            }
        }
        public static string GetMlSysComapanyName(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKMlSysCompanyName;
                case CompanyAikoNo:
                    return AikoMlSysCompanyName;
                case CompanyEneosNo:
                    return EneMlSysCompanyName;
                case CompanyShowakosanNo:
                    return ShowakosanCompanyName;
                case CompanyTokosyojiNo:
                    return TokosyojiCompanyName;
            }
        }
#endif
        //------- SKK -------------------------------
        public static string SKKCompanyName = "SKK";
        public static string SKKCompMBUSkkcode = "M099";
        public static string SKKDefSkkcode1 = "M099000001";
        public static string SKKDefSiteName1 = "SKKテスト";
        public static string SKKCompGWUSkkcode = "A099";
        public static string SKKAdminName    = "SKKPrimeAdmin";
        public static string SKKSubAdminName = "SKKSubAdmin";
        public static int SkkMaxTankNo = 8;
        public static string SKKMlAdr1 = "n-matsuda@showa-kiki.co.jp";

        public static string SKKCompanyName3 = "SKK";
        public static string SKKMlSysCompanyName = "SKK";
        public static string SKKMlCompanyName = "Skk";
        public static string SKKCompanyFolder = "SkkKaihatsu";

        //-------- 相光石油 -----------------------------
        public static string AikoCompanyName = "相光石油";
        public static string AikoCompanyName2 = "AikoSekiyu";
        public static string AikoCompMBUSkkcode = "M012";
        public static string AikoDefSkkcode1 = "M012000001";
        public static string AikoDefSiteName1 = "大江SS";
        public static string AikoCompGWUSkkcode = "A060";
        public static string AikoAdminName = "AikoSekiyuPrimeAdmin";
        public static string AikoSubAdminName = "AikoSekiyuSubAdmin";
        public static int AikoMaxTankNo = 9;

        public static string AikoMlSysCompanyName = "相光石油";
        public static string AikoMlCompanyName = "相光石油";
        public static string AikoCompanyFolder = "Test1";


        //-------- エネオス株式会社 -----------------------------
        public static string EneCompanyName = "エネオス株式会社";
        public static string EneCompanyName2 = "EneosCorp";
        public static string EneCompMBUSkkcode = "M013";
        public static string EneDefSkkcode1 = "M013000001";
        public static string EneDefSiteName1 = "青森インターSS";
        public static string EneCompGWUSkkcode = "A099";
        public static string EneAdminName = "EneosCorpPrimeAdmin";
        public static string EneSubAdminName = "EneosCorpSubAdmin";
        public static int EneMaxTankNo = 11;

        public static string EneMlSysCompanyName = "エネオス株式会社";
        public static string EneMlCompanyName = "エネオス株式会社";
        public static string EneCompanyFolder = "Test1";

        //-------- 昭和興産株式会社 -----------------------------
        public static string ShowakosanAdminUname = "showakosan@skkatg.co.jp";

        public static string ShowakosanCompanyName = "昭和興産株式会社";
        public static string ShowakosanCompanyName2 = "Syowakosan";
        public static string ShowakosanCompMBUSkkcode = "M015";
        public static string ShowakosanDefSkkcode1 = "M015000001";
        public static string ShowakosanDefSiteName1 = "羊蹄トンネル工事作業所";
        public static string ShowakosanCompGWUSkkcode = "A099";
        public static string ShowakosanAdminName = "SyowakosanPrimeAdmin";
        public static string ShowakosanSubAdminName = "SyowakosanSubAdmin";
        public static int ShowakosanMaxTankNo = 11;

        public static string ShowakosanCompanyName3 = "昭和興産株式会社";
        public static string ShowakosanMlSysCompanyName = "昭和興産株式会社";
        public static string ShowakosanMlCompanyName = "昭和興産株式会社";
        public static string ShowakosanCompanyFolder = "Test1";

        //-------- 東鋼商事株式会社 -----------------------------
        public static string TokosyojiAdminUname = "calbeeshimotsuma@skkatg.co.jp";

        public static string TokosyojiCompanyName = "東鋼商事株式会社";
        public static string TokosyojiCompanyName2 = "Tokosyoji";
        public static string TokosyojiCompMBUSkkcode = "M016";
        public static string TokosyojiDefSkkcode1 = "M016000001";
        public static string TokosyojiDefSiteName1 = "カルビー下妻工場";
        public static string TokosyojiCompGWUSkkcode = "A099";
        public static string TokosyojiAdminName = "TokosyojiPrimeAdmin";
        public static string TokosyojiSubAdminName = "TokosyojiSubAdmin";
        public static int TokosyojiMaxTankNo = 11;

        public static string TokosyojiCompanyName3 = "東鋼商事株式会社";
        public static string TokosyojiMlSysCompanyName = "東鋼商事株式会社";
        public static string TokosyojiMlCompanyName = "東鋼商事株式会社";
        public static string TokosyojiCompanyFolder = "Test1";

        //Session用文字列
        public static string SCompanyNo = "CompanyNo";  //会社番号
        public static string SCompanyName = "Company"; //会社名
        public static string SDisplayNo = "Display"; //表示番号
        public static string SDispWrgOnly = "ShowWrgOnly"; //警報発生施設のみ表示
        public static string SSiteName = "SiteName"; //施設名
        public static string SSkkcode = "Skkcode"; //SKKコード
        public static string STankNo = "TankNo"; //タンク番号
        public static string SIPageNo = "InvPageNo"; //在庫表示用ページ番号
        public static string SOiltype = "OilType"; //油種
        public static string SSortby = "SortBy"; //タンク番号/液種順表示
        public static string SSiteNo = "SiteNo"; //施設番号
        public static string SDstname = "DstName"; //特約店名
        public static string SUsername = "UserName"; //ユーザー名(メール用)
        public static string SMlSitename = "Mlsitename"; //メール宛先施設名
        public static string SComStatus = "CommSts"; //指定集信ステータス
        public static string SAccName = "AccountName"; //設定用アカウント名
        public static string SAccNo = "AccountNo";  //アカウント削除用番号
        public static string SValSet = "ValSet";    //設定画面でのテキストボックス文字列設定確認
        public static int ValNoSet = 0;             //文字列未設定
        public static int ValSet = 1;               //文字列設定済
        public static string SComNum = "ComNum";    //指定集信施設数
#if false
        public static string SComNumSuccess = "ComNumSuccess";    //指定集信施設数
        public static string SComS1Code = "Com1Code"; //指定集信SKKコード1
        public static string SComS1Devadr = "Com1Adr"; //指定集信デバイスアドレス1
        public static string SComS2Code = "Com2Code"; //指定集信SKKコード2
        public static string SComS2Devadr = "Com2Adr"; //指定集信デバイスアドレス2
        public static string SComS3Code = "Com3Code"; //指定集信SKKコード2
        public static string SComS3Devadr = "Com3Adr"; //指定集信デバイスアドレス2
        public static string SComS4Code = "Com4Code"; //指定集信SKKコード2
        public static string SComS4Devadr = "Com4Adr"; //指定集信デバイスアドレス2
        public static string SComS5Code = "Com5Code"; //指定集信SKKコード2
        public static string SComS5Devadr = "Com5Adr"; //指定集信デバイスアドレス2
        public static string SComS6Code = "Com6Code"; //指定集信SKKコード2
        public static string SComS6Devadr = "Com6Adr"; //指定集信デバイスアドレス2
        public static string SComS7Code = "Com7Code"; //指定集信SKKコード2
        public static string SComS7Devadr = "Com7Adr"; //指定集信デバイスアｓドレス2
        public static string SComS8Code = "Com8Code"; //指定集信SKKコード2
        public static string SComS8Devadr = "Com8Adr"; //指定集信デバイスアドレス2
        public static string SComS9Code = "Com9Code"; //指定集信SKKコード2
        public static string SComS9Devadr = "Com9Adr"; //指定集信デバイスアドレス2
        public static string SComS10Code = "Com10Code"; //指定集信SKKコード2
        public static string SComS10Devadr = "Com10Adr"; //指定集信デバイスアドレス2
        public static string SComS1ErSite = "Com1ErSite"; //指定集信SKKコード1
        public static string SComS2ErSite = "Com2ErSite"; //指定集信SKKコード2
        public static string SComS3ErSite = "Com3ErSite"; //指定集信SKKコード3
        public static string SComS4ErSite = "Com4ErSite"; //指定集信SKKコード4
        public static string SComS5ErSite = "Com5ErSite"; //指定集信SKKコード5
        public static string SComS6ErSite = "Com6ErSite"; //指定集信SKKコード6
        public static string SComS7ErSite = "Com7ErSite"; //指定集信SKKコード7
        public static string SComS8ErSite = "Com8ErSite"; //指定集信SKKコード8
        public static string SComS9ErSite = "Com9ErSite"; //指定集信SKKコード9
        public static string SComS10ErSite = "Com10ErSite"; //指定集信SKKコード10
#endif

        //画面表示用文字列
        public static string ShowAllSites = "全ての施設を表示";
        public static string ShowWrgSites = "警報発生施設のみを表示";
        public static string ShowComSites = "指定集信を行う施設をチェックして、右の集信開始ボタンをクリック　　  ";
        public static string AllSites = "全て";
        public static string JavaHeader = "<script language=javascript>";
        public static string JavaEnd = "</script>";

        //タンクステータス文字列
        public static string TankNoError = "通常";

        //タブ名
        public static string TabTable = "在庫一覧";
        public static string TabComm = "指定集信";
        public static string TabSSTHistory = "在庫履歴（タンクごと）";
        public static string TabSSOHistory = "在庫履歴（液種ごと）";
        public static string TabSSWrg = "施設管理";
        public static string TablSiteInf = "登録施設";
        public static string TabRegAccSite = "表示施設";
        public static string TabRegLWMailSite = "減警報メール";
        public static string TabRegMailAdr = "メール送信先";
        public static string TabMailAdrList = "メール送信先一覧";
        public static string TabRegAccount = "アカウント作成";
        public static string TabAccountDel = "アカウント削除";

        //タブ番号
        public static int TabTableNo = 0;   //表表示
        public static int TabCommNo = 1;    //指定集信
        public static int TabSSTHisNo = 1;  //在庫履歴(タンク毎)
        public static int TabSSOHisNo = 2;  //在庫履歴(液種毎)
        public static int TabSSWrgNo = 3;   //施設管理
        public static int TabSiteInfNo = 0; //登録施設
        public static int TabRegAccSiteNo = 1;  //表示施設
        public static int TabRegLWMailSiteNo = 2; //減警報メール
        public static int TabRegMailAdrNo = 3;  //メール送信先登録
        public static int TabMailAdrListNo = 4; //メール送信先一覧
        public static int TabRegAccountNo = 5;  //アカウント作成
        public static int TabAccountDelNo = 6;  //アカウント削除

	    public static bool ShowHLOnly = true; //満、減警報のみ表示
        public static string SetDateStr(string zstr)
        {
            zstr = zstr.Insert(8, ":"); //ファイル名より抽出した日付を1207211410 → 2012/07/21 14:10 のようなフォーマットにする。
            zstr = zstr.Insert(6, " ");
            zstr = zstr.Insert(4, "/");
            zstr = zstr.Insert(2, "/");
            zstr = "20" + zstr;
            return zstr;
        }

        //DSV在庫情報
        public struct DsvInvData
        {
            public long FullVolume;       //申請容量
            public long OilLevel;         //液面高さ
        }

        public static DsvInvData tank1;

        //文字列"yyyy年MM月"よりDateTimeに変換
        public static DateTime GetDateTimeFromMonthString(string selmonth)
        {
            selmonth = selmonth.Remove(selmonth.Length - 1);//xxxx年yy
            int year = int.Parse(selmonth.Substring(0, 4));      //xxxx
            int month = int.Parse(selmonth.Substring(5));
            return new DateTime(year, month, 1);
        }
        //文字列"yyyy年MM月"よりDateTimeに変換  bfirst=true 毎月1日 false 15日
        public static DateTime GetDateTimeByHalfMonthFromMonthString(string datestr, bool bfsthalf)
        {
            //datestr = datestr.Substring(1);   //xxxx年yy月
            datestr = datestr.Remove(datestr.Length - 1);//xxxx年yy
            int year = int.Parse(datestr.Substring(0, 4)); //xxxx
            int month = int.Parse(datestr.Substring(5));
            if (bfsthalf == true)
                return (new DateTime(year, month, 1));
            else
                return (new DateTime(year, month, 15));
        }
    }

    public enum WrgType
    {
        LowWrg = 0, //減
        HighWrg = 1,  //満ｓ
        WtrWrg = 2,    //水
        SnsErrWrg = 3, //センサー異常
        LC4Wrg = 4,    //漏えい点検
        LeakWrg = 5,   //リーク
        NoDataWrg = 6  //データ無し
    }

    public class ErrDataDetail
    {
        public string SiteName;    //施設名
        public string Skkcode;     //SKKコード
        public string Companyname;  //会社名
        public int OilTypeno;       //液種名
        public int TankNo;          //タンク番号
        public string DateStr;      //日時
        public int WrgType;         //警報種類
        public int TrgOrRel;        //0:警報解除 1:発生
        public int WtrLvl;          //水位
        public int WtrVol;          //水量
        public string LC4Res;       //漏えい点検結果,判定値

        public ErrDataDetail(string SName, string Scode, string Cname, int Ono, int Tno, string DStr, int WType, int ToR, int Wl, int Wv, string res)
        {
            SiteName = SName;
            Skkcode = Scode;
            Companyname = Cname;
            OilTypeno = Ono;
            TankNo = Tno;
            DateStr = DStr;
            WrgType = WType;
            TrgOrRel = ToR;
            WtrLvl = Wl;
            WtrVol = Wv;
            LC4Res = res;
        }
    }

    public class OilName
    {
        public static string GetOilName(int oiltype)
        {
            string oilname = "";
            switch (oiltype)
            {
                case 1:
                    return "ハイオク";
                case 2:
                    return "レギュラー";
                case 3:
                    return "灯油";
                case 4:
                    return "軽油";
            }
            return oilname;
        }
    }

    public class ErrTypeName
    {
        public static string GetErrName(int errtype)
        {
            switch (errtype)
            {
                case (int)WrgType.LowWrg:
                    return "減警報";
                case (int)WrgType.HighWrg:
                    return "満警報";
                case (int)WrgType.WtrWrg:
                    return "水検知警報";
                case (int)WrgType.SnsErrWrg:
                    return "液面センサー異常警報";
                case (int)WrgType.LC4Wrg:
                    return "漏えい検知警報";
                case (int)WrgType.LeakWrg:
                    return "二重殻タンク間隙のリーク警報";
                case (int)WrgType.NoDataWrg:
                    return "データ更新なし警報";
            }
            return " ";
        }
    }

    public class WebResponseCtrl
    {
        //ファイルダウンロード用のHTTP Response作成 sendtext:ダウンロードする文字列 sDownloadFileName:ダウンロードファイル名
        static public void CreateResponse(System.Web.HttpResponse Response, string sendtext, string sDownloadFileName)
        {
            //Response情報クリア
            Response.ClearContent();
            //バッファリング
            Response.Buffer = true;
            //HTTPヘッダー情報設定
            // ヘッダとコンテンツのエンコードを「shift_jis」に設定。
            Response.HeaderEncoding = System.Text.Encoding.GetEncoding("shift_jis");
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("shift_jis");
            // HTTPのキャラセットを設定
            Response.Charset = "shift_jis";
            // ファイルのMIMEタイプを設定ですが…、とりあえず「その他」を設定(OS側で拡張子に従って開いてもらう為)
            Response.ContentType = "application/octet-stream";
            // コンテキスト(メッセージ)の扱いに関するヘッダ情報です。ここにファイル名を設定します。
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName));

            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", HttpUtility.UrlEncode(sDownloadFileName)));
            Response.ContentType = "text/plain";
            //ファイル書込
            Response.Write(sendtext);
            //フラッシュ
            Response.Flush();
            //レスポンス終了
            Response.End();
        }

        //Popupメッセージを表示する  sendtext:ポップアップに表示する文字列
        static public void WriteResponse(System.Web.HttpResponse Response, string sendtext)
        {
            string script = GlobalVar.JavaHeader + "window.alert('" + sendtext + "')" + GlobalVar.JavaEnd;
            Response.Write(script);
        }
    }

    //データテーブル初期化用クラス
    public class DataTableCtrl
    {
        //データテーブルの消去
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        //データセットの消去
        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }

    //データベースアクセスクラス
    public class DBCtrl
    {
        //sql分を実施して、指定されたデータテーブルにDB中のデータをコピーする
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        //sql分を実施する。
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.ExecuteNonQuery();
            cn.Close();
        }
    }

    //水履歴情報用クラス
    public class WtrHistory
    {
        //戻り値 true: 指定SKKコードの水履歴が存在する false:存在しない
        public static bool ExistWtrHistory(string skkcode)
        {
            bool bexist = false;
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            string sqlstr = "SELECT COUNT(*) FROM WtrHistory WHERE SKKコード='" + skkcode + "'"; ;
            System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            int count = (int)Com.ExecuteScalar();
            cn.Close();
            if (count > 0)
                bexist = true;
            return bexist;
        }

        //1か月前以上の古い水履歴を消去する
        public static void DeleteOldWtrHist()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddDays(-31); //31日前
                string sqlstr = "DELETE FROM WtrHistory WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //水履歴テーブルを作成する
        public static DataTable CreateWtrTable(ZaikoStr zs, SiteInfData sinf)
        {
            DataTable wtrtbl = new DataTable();
            wtrtbl.Columns.Add(new DataColumn("タンク番号", typeof(int)));
            wtrtbl.Columns.Add(new DataColumn("液種", typeof(string)));
            wtrtbl.Columns.Add(new DataColumn("水位", typeof(string)));
            wtrtbl.Columns.Add(new DataColumn("水量", typeof(string)));
            string[] wtrlvlar = zs.wtrlvllst.ToArray(); //水位取り出し
            string[] wtrvolar = zs.wtrvollst.ToArray(); //水量取り出し
            string[] wtrwrgar = zs.sslanstat.ToArray(); //SSLANステータス取り出し

            for (int i = 1; i <= zs.numtank; i++)
            {
                try
                {
                    DataRow drow = wtrtbl.NewRow();
                    drow["タンク番号"] = i;
                    drow["液種"] = sinf.GetOilTypeByTank(i);
                    string wrg = wtrwrgar[i - 1];
                    if ((wrg == "3") || (wrg == "4") || (wrg == "5"))
                    {
                        int wtrlvl = int.Parse(wtrlvlar[i - 1]);
                        wtrlvl = (wtrlvl + 50) / 100;
                        drow["水位"] = wtrlvl;
                        int wtrvol = int.Parse(wtrvolar[i - 1]);
                        wtrvol = (wtrvol + 50) / 100;
                        drow["水量"] = wtrvol;
                    }
                    else
                    {
                        drow["水位"] = "正常";
                        drow["水量"] = "正常";
                    }
                    wtrtbl.Rows.Add(drow);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return wtrtbl;
        }

    }
}