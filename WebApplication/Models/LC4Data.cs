﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.IO;
using System.Diagnostics;

namespace WebApplication.Models
{
    public class LC4Data
    {
        //通信データから新規作成した、またはCSVファイルから読み込んだ在庫データ
        public DataTable LC4DataTable;
        private DataTable[] LC4DtArray;
        //LC34データのCSVファイルに相当するクラスデータ
        const string strLC4Data = "LC34データ";

        //テーブルヘッダー文字列
        private const string stSSCode = "SSコード";
        private const string stTankNo = "タンク番号";
        private const string stTestMode = "点検モード";
        private const string stStTime = "開始時刻";
        private const string stStLvl = "開始液位(mm)";
        private const string stStVol = "開始液量(L)";
        private const string stStTherm = "開始液温(℃)";
        private const string stStAdjVol = "開始温度補正量(L)";
        private const string stEdTime = "終了時刻";
        private const string stEdVol = "終了液量(L)";
        private const string stEdTherm = "終了液温(℃)";
        private const string stEdAdjVol = "終了温度補正量(L)";
        private const string stRsltVal = "変化量(L/時)";
        private const string stRsltJdge = "判定結果";
        private const string stLnkInf = "連結情報";
        private const string stLnkRsVal = "連結変化量(L/時)";

        private const int Col_SSKCode = 0;            //SSKCode  *
        private const int Col_TankNo = 1;            //タンク番号 *
        private const int Col_TestMode = 2;            //点検モード *
        private const int Col_StTime = 3;            //開始日時 *
        private const int Col_StLvl = 4;            //開始時液面高さ *
        private const int Col_RsltVal = 5;            //結果値 *
        private const int Col_LnkRsVal = 6;            //連結結果値 *
        private const int Col_RsltJdge = 7;            //結果判定 *
        private const int Col_StVol = 8;            //開始時容量 *
        private const int Col_StTherm = 9;            //開始時温度 *
        private const int Col_StAdjVol = 10;            //開始時補正容量 *
        private const int Col_EdTime = 11;            //終了日時 *
        private const int Col_EdVol = 12;            //終了時容量 *
        private const int Col_EdTherm = 13;            //終了時温度 *
        private const int Col_EdAdjVol = 14;            //終了時補正容量 *
        private const int Col_LnkInf = 15;            //連結情報 *

        private const int Offs_TankNo = 0;            //タンク番号 *
        private const int Offs_TestMode = 2;            //点検モード *
        private const int Offs_StTime = 3;            //開始日時 *
        private const int Offs_StLvl = 15;            //開始時液面高さ *
        private const int Offs_StVol = 21;            //開始時容量 *
        private const int Offs_StTherm = 29;            //開始時温度 *
        private const int Offs_StAdjVol = 34;            //開始時補正容量 *
        private const int Offs_EdTime = 42;            //終了日時 *
        private const int Offs_EdVol = 54;            //終了時容量 *
        private const int Offs_EdTherm = 62;            //終了時温度 *
        private const int Offs_EdAdjVol = 67;            //終了時補正容量 *
        private const int Offs_RsltVal = 75;            //結果値 *
        private const int Offs_RsltJdge = 84;            //結果判定 *
        private const int Offs_LnkInf = 86;            //連結情報 *
        private const int Offs_LnkRsVal = 88;            //連結結果値 *

        private const int Offs_end = 106;

        //点検結果文字列
        private const string stLC4OK = "合格";
        private const string stLC5NTIMEOK = "LC5時間不足での仮合格";
        private const string stLC5NTIMEOKLNK = "LC5時間不足での仮合格(連結)";
        private const string stLC4LNKOK = "連結タンク合格";
        private const string stLC4NG = "不合格";
        private const string stLC4LNKNG = "連結タンク不合格";
        private const string stLC4FAIL = "不合格疑い";
        private const string stLC4LNKFAIL = "連結不合格疑い";
        private const string stLC4SENSERRTST = "点検中センサーエラーあり";
        private const string stLC4HGTERR = "液面高さ不足";
        private const string stLC4DLVTST = "点検中荷卸あり";
        private const string stLC4KEITST = "点検中給油あり";
        private const string stLC4THMERTST = "点検中温度液音不安定";
        private const string stLC4NOTTIMETST = "点検中時間不足";
        private const string stLC4SNSERR = "センサーエラーによる時間不足";
        private const string stLC4DLV = "荷卸による時間不足";
        private const string stLC4KEI = "給油による時間不足";
        private const string stLC4THMERR = "温度不安定による時間不足";
        private const string stLC4NOTTIME = "監視時間不足";
        private const string stLC4NOCHK = "未判定";
        //LC34 - RsltJudgeの定義
        //OK
        private const string LC4OK = "00";          //"合格"
        private const string LC5NTIMEOK = "01";     //"LC5時間不足での仮合格"
        private const string LC5NTIMEOKLNK = "02";  //"LC5時間不足での仮合格(連結)"
        private const string LC4LNKOK = "03";       //"連結タンク合格"
        private const string LC4NG = "04";          //"不合格"
        private const string LC4LNKNG = "05";       //"連結タンク不合格"
        private const string LC4FAIL = "06";        //"不合格疑い"
        private const string LC4LNKFAIL = "07";     //"連結不合格疑い"
        private const string LC4SENSERRTST = "08";  //"点検中センサーエラーあり"
        private const string LC4HGTERR = "09";       //"液面高さ不足"
        private const string LC4DLVTST = "10";      //"点検中荷卸あり"
        private const string LC4KEITST = "11";       //"点検中給油あり"
        private const string LC4THMERTST = "12";     //"点検中温度液音不安定"
        private const string LC4NOTTIMETST = "13";   //"点検中時間不足"
        private const string LC4SNSERR = "14";       //"センサーエラーによる時間不足"
        private const string LC4DLV = "15";          //"荷卸による時間不足"
        private const string LC4KEI = "16";         //"給油による時間不足"
        private const string LC4THMERR = "17";      //"温度不安定による時間不足"
        private const string LC4NOTTIME = "18";     //"監視時間不足"
        private const string LC4NOCHK = "19";       //"未判定"

        FilePath CurFilePathData;

        //コンストラクタ
        public LC4Data()
        {
            if (LC4DataTable != null)
            {
                LC4DataTable.Clear();
                LC4DataTable.Dispose();
                LC4DataTable = null;
            }
            CurFilePathData = new FilePath();
            LC4DtArray = new DataTable[20];
       }

    //LC34データの作成
    public void CreateLC4Table()
        {
            try
            {
                if (LC4DataTable != null)
                {
                    LC4DataTable.Clear();
                    LC4DataTable.Dispose();
                    LC4DataTable = null;
                }
                LC4DataTable = new DataTable();

                //LC34データを保存するディレクトリが存在しない場合は新たに作成する。
                string LC4DataPath = CurFilePathData.GetLC4DataPath();
                if (Directory.Exists(LC4DataPath) == false)
                {
                    Directory.CreateDirectory(LC4DataPath);
                }
                //レコードのフィールドを定義する
                LC4DataTable.Columns.Add(stSSCode);
                LC4DataTable.Columns.Add(stTankNo);
                LC4DataTable.Columns.Add(stTestMode);
                LC4DataTable.Columns.Add(stStTime);
                LC4DataTable.Columns.Add(stStLvl);
                LC4DataTable.Columns.Add(stRsltVal);
                LC4DataTable.Columns.Add(stLnkRsVal);
                LC4DataTable.Columns.Add(stRsltJdge);
                LC4DataTable.Columns.Add(stStVol);
                LC4DataTable.Columns.Add(stStTherm);
                LC4DataTable.Columns.Add(stStAdjVol);
                LC4DataTable.Columns.Add(stEdTime);
                LC4DataTable.Columns.Add(stEdVol);
                LC4DataTable.Columns.Add(stEdTherm);
                LC4DataTable.Columns.Add(stEdAdjVol);
                LC4DataTable.Columns.Add(stLnkInf);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //整数値で先頭の0を切り詰め
        private string TruncDigitString(string DigiStr, int iNum)
        {
            int j = 0;
            for (int i = 0; i <= iNum - 2; i++)
            {
                if (DigiStr.Substring(i, 1) != "0")
                {
                    break; // TODO: might not be correct. Was : Exit For
                }
                j = j + 1;
            }
            if (j > 0)
            {
                DigiStr = DigiStr.Substring(j, iNum - j);
            }
            //if (DigiStr.Length > 3)
            //{
            //    DigiStr = DigiStr.Insert(DigiStr.Length - 3, ",");
            //}
            return DigiStr;
        }

        //整数値+小数点２桁で先頭の0を切り詰め
        private string TruncDigitString2(string DigiStr, int iNum)
        {
            int j = 0;
            for (int i = 0; i <= iNum - 4; i++)
            {
                if (DigiStr.Substring(i, 1) != "0")
                {
                    break; // TODO: might not be correct. Was : Exit For
                }
                j = j + 1;
            }
            if (j > 0)
            {
                DigiStr = DigiStr.Substring(j, iNum - j);
            }
            return DigiStr;
        }

        //LC34データの更新
        public void UpdateLC4Table(string SkkCode, string RcvStr)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            DataRow dRow = default(DataRow);
            string lc4str = ""; 
            int i;
            try
            {
                int tno = RcvStr.Length / Offs_end;
                for( i=0; i< tno; i++ )
                {
                    lc4str = RcvStr.Substring(Offs_end * i);
                    //If LC34DataTable.Rows.Count = 0 Then
                    dRow = LC4DataTable.NewRow();
                    dRow[stSSCode] = SkkCode;
                    //タンク番号
                    string s1 = null;
                    s1 = lc4str.Substring(Offs_TankNo, 2);
                    dRow[stTankNo] = s1;

                    //点検モード
                    s1 = lc4str.Substring(Offs_TestMode, 1);
                    if (s1 == "4")
                    {
                        dRow[stTestMode] = "LC-4";
                    }
                    else if (s1 == "3")
                    {
                        dRow[stTestMode] = "LC-3";
                    }
                    else
                    {
                        dRow[stTestMode] = "LC-5";
                    }
                    //開始時間(ASCII yyyymmddhhmm)
                    s1 = lc4str.Substring(Offs_StTime, 12);
                    s1 = s1.Insert(s1.Length - 2, ":");
                    s1 = s1.Insert(s1.Length - 5, " ");
                    s1 = s1.Insert(s1.Length - 8, "/");
                    s1 = s1.Insert(s1.Length - 11, "/");
                    dRow[stStTime] = s1;

                    //開始時液面高
                    s1 = lc4str.Substring(Offs_StLvl, 6);
                    s1 = TruncDigitString(s1, 4);
                    dRow[stStLvl] = s1;

                    //結果符号			     
                    string s2 = null;
                    s2 = lc4str.Substring(Offs_RsltVal, 1);
                    if (s2 == "1")
                    {
                        s2 = "-";
                    }
                    else
                    {
                        s2 = "+";
                    }
                    //結果値
                    s1 = lc4str.Substring(Offs_RsltVal + 1, 8);
                    s1 = TruncDigitString2(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stRsltVal] = s2 + s1;

                    //連結符号
                    s2 = lc4str.Substring(Offs_LnkRsVal, 1);
                    if (s2 == "1")
                    {
                        s2 = "-";
                    }
                    else
                    {
                        s2 = "+";
                    }
                    //連結結果値
                    s1 = lc4str.Substring(Offs_LnkRsVal + 1, 8);
                    s1 = TruncDigitString2(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stLnkRsVal] = s2 + s1;

                    //判定
                    s1 = lc4str.Substring(Offs_RsltJdge, 2);
                    //結果判定0=OK/1=NG			 
                    switch (s1)
                    {
                        case LC4OK:
                            //OK
                            dRow[stRsltJdge] = stLC4OK;
                            break;
                        case LC5NTIMEOK:
                            dRow[stRsltJdge] = stLC5NTIMEOK;
                            break;
                        case LC5NTIMEOKLNK:
                            dRow[stRsltJdge] = stLC5NTIMEOKLNK;
                            break;
                        case LC4LNKOK:
                            dRow[stRsltJdge] = LC4LNKOK;
                            break;
                        case LC4NG:
                            dRow[stRsltJdge] = stLC4NG;
                            break;
                        case LC4LNKNG:
                            dRow[stRsltJdge] = LC4LNKNG;
                            break;
                        case LC4FAIL:
                            dRow[stRsltJdge] = stLC4FAIL;
                            break;
                        case LC4LNKFAIL:
                            dRow[stRsltJdge] = LC4LNKFAIL;
                            break;
                        case LC4SENSERRTST:
                            //センサーエラー(点検中）
                            dRow[stRsltJdge] = stLC4SENSERRTST;
                            //点検中センサーエラーあり"
                            break;
                        case LC4HGTERR:
                            //液面高さ不足
                            dRow[stRsltJdge] = stLC4HGTERR;
                            //液面高さ不足による時間不足"
                            break;
                        case LC4DLVTST:
                            //荷卸あり(点検中）
                            dRow[stRsltJdge] = stLC4DLVTST;
                            //点検中荷卸あり"
                            break;
                        case LC4KEITST:
                            //給油あり(点検中）
                            dRow[stRsltJdge] = stLC4KEITST;
                            //点検中給油あり"
                            break;
                        case LC4THMERTST:
                            //温度変化率規定値超え(点検中)
                            dRow[stRsltJdge] = stLC4THMERTST;
                            //点検中液温不安定"
                            break;
                        case LC4NOTTIMETST:
                            //監視時間不足(点検中)
                            dRow[stRsltJdge] = stLC4NOTTIMETST;
                            //点検中時間不足"
                            break;
                        case LC4SNSERR:
                            //センサーエラー(時間不足)
                            dRow[stRsltJdge] = stLC4SNSERR;
                            //センサーエラーによる時間不足"
                            break;
                        case LC4DLV:
                            //荷卸あり(時間不足)
                            dRow[stRsltJdge] = stLC4DLV;
                            //荷卸による時間不足"
                            break;
                        case LC4KEI:
                            //給油あり(時間不足)
                            dRow[stRsltJdge] = stLC4KEI;
                            //給油による時間不足"
                            break;
                        case LC4THMERR:
                            //温度変化率規定値超え(時間不足)
                            dRow[stRsltJdge] = stLC4THMERR;
                            //液温不安定による時間不足"
                            break;
                        case LC4NOTTIME:
                            //監視時間不足
                            dRow[stRsltJdge] = stLC4NOTTIME;
                            //点検時間不足"
                            break;
                        case LC4NOCHK:
                            dRow[stRsltJdge] = stLC4NOCHK;
                            break;
                    }

                    //開始時容量
                    s1 = lc4str.Substring(Offs_StVol, 8);
                    s1 = TruncDigitString(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stStVol] = s1;

                    //開始時温度
                    s1 = lc4str.Substring(Offs_StTherm, 5);
                    s1 = s1.Insert(2, ".");
                    dRow[stStTherm] = s1;

                    //開始時補正容量
                    s1 = lc4str.Substring(Offs_StAdjVol, 8);
                    s1 = TruncDigitString(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stStAdjVol] = s1;

                    //終了時間(ASCII yyyymmddhhmm)	 
                    s1 = lc4str.Substring(Offs_EdTime, 12);
                    s1 = s1.Insert(s1.Length - 2, ":");
                    s1 = s1.Insert(s1.Length - 5, " ");
                    s1 = s1.Insert(s1.Length - 8, "/");
                    s1 = s1.Insert(s1.Length - 11, "/");
                    dRow[stEdTime] = s1;

                    //終了時容量
                    s1 = lc4str.Substring(Offs_EdVol, 8);
                    s1 = TruncDigitString(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stEdVol] = s1;

                    //終了時時温度
                    s1 = lc4str.Substring(Offs_EdTherm, 5);
                    s1 = s1.Insert(2, ".");
                    dRow[stEdTherm] = s1;

                    //終了時補正容量
                    s1 = lc4str.Substring(Offs_EdAdjVol, 8);
                    s1 = TruncDigitString(s1, 8);
                    s1 = s1.Insert(s1.Length - 2, ".");
                    dRow[stEdAdjVol] = s1;

                    //連結情報
                    s1 = lc4str.Substring(Offs_LnkInf, 1);
                    dRow[stLnkInf] = s1;
                    if (s1 == "0")
                    {
                        dRow[stLnkRsVal] = "0.00";
                    }

                    //If LC34DataTable.Rows.Count = 0 Then
                    LC4DataTable.Rows.Add(dRow);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //指定LC34ファイル読み込み、ない場合は新規作成
        public bool OpenLC4Data(string SkkCode, DateTime dt)
        {
            string LC4FileName = null;
            LC4FileName = CurFilePathData.GetMonthLC4DataPath(SkkCode, dt);
            try
            {
                if (File.Exists(LC4FileName) == true)
                {
                    ReadFromCSVFile(LC4FileName);
                    if (LC4DataTable.Rows.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    CreateLC4Table();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        //指定LC34ファイル書き込み、ない場合は新規作成
        public bool WriteLC4Data(string SkkCode, DateTime dt)
        {
            string LC4FileName = null;
            bool ret = false;
            LC4FileName = CurFilePathData.GetMonthLC4DataPath(SkkCode, dt);
            try
            {
                CsvData cd = new CsvData();
                ret = cd.WriteCSV(LC4DataTable, LC4FileName, false);
                return ret;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public bool OpenLC4DataBySKKCode(string SkkCode)
        {
            string LC4FileName = null;
            LC4FileName = CurFilePathData.GetThisMonthLC4DataPath(SkkCode);
            try
            {
                if (File.Exists(LC4FileName) == true)
                {
                    ReadFromCSVFile(LC4FileName);
                    if (LC4DataTable.Rows.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    CreateLC4Table();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public bool OpenLC4DataBySKKCodeAndDate(string SkkCode, string DateStr)
        {
            string LC4FileName = null;
            LC4FileName = CurFilePathData.GetMonthLC4DataWDayStrPath(SkkCode, DateStr);
            try
            {
                if (File.Exists(LC4FileName) == true)
                {
                    ReadFromCSVFile(LC4FileName);
                    if (LC4DataTable.Rows.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    CreateLC4Table();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        //指定のLC34データCSVファイルからデータテーブルに読み出す
        private bool ReadFromCSVFile(string CSVFile)
        {
            try
            {
                if (LC4DataTable != null)
                {
                    LC4DataTable.Clear();
                    LC4DataTable.Dispose();
                    LC4DataTable = null;
                }
                LC4DataTable = new DataTable();
                //LC4DataTable.Columns.Add(stSSCode);
                //LC4DataTable.Columns.Add(stTankNo);
                //LC4DataTable.Columns.Add(stTestMode);
                //LC4DataTable.Columns.Add(stStTime);
                //LC4DataTable.Columns.Add(stStLvl);
                //LC4DataTable.Columns.Add(stRsltVal);
                //LC4DataTable.Columns.Add(stLnkRsVal);
                //LC4DataTable.Columns.Add(stRsltJdge);
                //LC4DataTable.Columns.Add(stStVol);
                //LC4DataTable.Columns.Add(stStTherm);
                //LC4DataTable.Columns.Add(stStAdjVol);
                //LC4DataTable.Columns.Add(stEdTime);
                //LC4DataTable.Columns.Add(stEdVol);
                //LC4DataTable.Columns.Add(stEdTherm);
                //LC4DataTable.Columns.Add(stEdAdjVol);
                //LC4DataTable.Columns.Add(stLnkInf);

                CsvData cd = new CsvData();
                LC4DataTable = cd.ReadCSV(CSVFile, ',', true);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("在庫データの読み込みに失敗しました");
                return false;
            }
        }

        public int GetNumLC4Data()
        {
            try
            {
                 return LC4DataTable.Rows.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        public double GetRsltValue(int i)
        {
            double dValue = 0;
            try
            {
                dValue = Convert.ToDouble(LC4DataTable.Rows[i][Col_RsltVal].ToString());
                return dValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        //変化量が有効かどうか返す
        public bool CheckRsltValid(int i)
        {
            bool rvalid = false;
            try
            {
                string res = LC4DataTable.Rows[i][Col_RsltJdge].ToString();
                if ((res == stLC4OK) || (res == stLC4NG) || (res == stLC4FAIL) || (res == stLC4LNKOK) || (res == stLC4LNKNG) || (res == stLC4LNKFAIL))
                {
                    rvalid = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                rvalid = false;
            }
            return rvalid;
        }


        //指定番号レコードのSKKコードを返す
        public string GetSKKCode(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_SSKCode].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号レコードのタンク番号を返す
        public string GetTankNo(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_TankNo].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号レコードの点検モードを返す
        public string GetTestMode(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_TestMode].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の開始日時を返す
        public string GetStTime(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_StTime].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        public int GetZaikoTstDate(int i)
        {
            try
            {
                string DateStr = null;
                DateTime dt = default(DateTime);
                //DateStr = LC4DataTable.Rows.Item(i).Item(LC4Col.Col_EdTime)
                DateStr = LC4DataTable.Rows[i][Col_StTime].ToString();
                dt = DateTime.Parse(DateStr);
                return Convert.ToInt32(dt.Day);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return 0;
            }
        }

        //指定番号の開始時液位を返す
        public string GetStLvl(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_StLvl].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の結果値を返す(文字列)
        public string GetRsltVal(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_RsltVal].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の連結結果値を返す
        public string GetLnkRsVal(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_LnkRsVal].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の結果判定を返す
        public string GetRsltJdge(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_RsltJdge].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の開始時容量を返す
        public string GetStVol(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_StVol].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の開始時温度を返す
        public string GetStTherm(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_StTherm].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の開始時補正容量を返す
        public string GetStAdjVol(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_StAdjVol].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の終了日時を返す
        public string GetEdTime(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_EdTime].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }
        //指定番号の終了日時(日付)を返す
        public DateTime GetEdDate(int i)
        {
            try
            {
                string dstr = LC4DataTable.Rows[i][Col_EdTime].ToString();
                return DateTime.Parse(dstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return DateTime.Now.ToLocalTime();
            }
        }

        //指定番号の終了時容量を返す
        public string GetEdVol(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_EdVol].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の終了時温度を返す
        public string GetEdTherm(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_EdTherm].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号の終了時補正容量を返す
        public string GetEdAdjVol(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_EdAdjVol].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }

        //指定番号のリンク情報を返す
        public string GetLnkInf(int i)
        {
            try
            {
                return LC4DataTable.Rows[i][Col_LnkInf].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "0";
            }
        }


        //指定番号の行をコピーする
        public void CopyRow(int i, DataRow dRow, ref LC4Data dtable)
        {
            try
            {
                dRow[Col_SSKCode] = dtable.GetSKKCode(i);
                //点検モード
                dRow[Col_TestMode] = dtable.GetTestMode(i);
                //点検モード
                dRow[Col_StTime] = dtable.GetStTime(i);
                //開始日時
                dRow[Col_StLvl] = dtable.GetStLvl(i);
                //開始時液面高さ
                dRow[Col_RsltVal] = dtable.GetRsltVal(i);
                //結果値
                dRow[Col_LnkRsVal] = dtable.GetLnkRsVal(i);
                //連結結果値
                dRow[Col_RsltJdge] = dtable.GetRsltJdge(i);
                //結果判定
                dRow[Col_StVol] = dtable.GetStVol(i);
                //開始時容量
                dRow[Col_StTherm] = dtable.GetStTherm(i);
                //開始時温度
                dRow[Col_StAdjVol] = dtable.GetStAdjVol(i);
                //開始時補正容量
                dRow[Col_EdTime] = dtable.GetEdTime(i);
                //終了日時
                dRow[Col_EdVol] = dtable.GetEdVol(i);
                //終了時容量
                dRow[Col_EdTherm] = dtable.GetEdTherm(i);
                //終了時温度
                dRow[Col_EdAdjVol] = dtable.GetEdAdjVol(i);
                //終了時補正容量
                dRow[Col_LnkInf] = dtable.GetLnkInf(i);
                //連結情報
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        //指定のタンクのテーブルを返す
        public void SortByTank(int TNum)
        {
            try
            {
                string TNo = TNum.ToString("00");
                DataTable dtbl = new DataTable();

                //レコードのフィールドを定義する
                dtbl.Columns.Add(stSSCode);
                dtbl.Columns.Add(stTankNo);
                dtbl.Columns.Add(stTestMode);
                dtbl.Columns.Add(stStTime);
                dtbl.Columns.Add(stStLvl);
                dtbl.Columns.Add(stRsltVal);
                dtbl.Columns.Add(stLnkRsVal);
                dtbl.Columns.Add(stRsltJdge);
                dtbl.Columns.Add(stStVol);
                dtbl.Columns.Add(stStTherm);
                dtbl.Columns.Add(stStAdjVol);
                dtbl.Columns.Add(stEdTime);
                dtbl.Columns.Add(stEdVol);
                dtbl.Columns.Add(stEdTherm);
                dtbl.Columns.Add(stEdAdjVol);
                dtbl.Columns.Add(stLnkInf);

                int Num = LC4DataTable.Rows.Count;
                int tnum;
                string tstr;
                DataRow dRow = default(DataRow);
                for (int i = 0; i <= Num - 1; i++)
                {
                    tstr = LC4DataTable.Rows[i][Col_TankNo].ToString();
                    tnum = int.Parse(tstr);
                    if (TNum == tnum)
                    {
                        dtbl.ImportRow(LC4DataTable.Rows[i]);
                    }
                }
                LC4DtArray[TNum-1] = dtbl;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        public bool IsLC4Data(int i)
        {
            try
            {
                if (LC4DataTable.Rows[i][Col_TestMode].ToString() == "LC-4")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public bool IsLC45Data(int i)
        {
            try
            {
                if ((LC4DataTable.Rows[i][Col_TestMode].ToString() == "LC-4") || (LC4DataTable.Rows[i][Col_TestMode].ToString() == "LC-5"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public void ShowLC4Data(System.Web.UI.WebControls.GridView pGridView)
        {
            try
            {
                DataTable lc4temp = LC4DataTable.Copy();
                lc4temp.Columns.RemoveAt(15); //連結情報
                lc4temp.Columns.RemoveAt(1); //タンク番号
                lc4temp.Columns.RemoveAt(0); //SKKコード
                pGridView.DataSource = lc4temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}