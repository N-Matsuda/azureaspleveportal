﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.Models
{
    public class MailAddress
    {
        DataTable cmptbl;
        DataTable usrtbl;
        DataTable sitetbl;
        protected string cmpname;

        const string COLCATEG = "分類";
        const string COLNAME = "名称";
        const string COLMLADR = "メールアドレス";
        const string COLMLNAME = "アドレス表示名";

        public MailAddress()
        {
            ;
        }

        //会社レベルのメールリスト一覧作成
        public void OpenCmpMailTable(string cpname)
        {
            cmpname = cpname;
            //会社メールアドレス表示
            cmptbl = new DataTable();
            cmptbl.Columns.Add(COLCATEG);
            cmptbl.Columns.Add(COLNAME);
            cmptbl.Columns.Add(COLMLADR);
            cmptbl.Columns.Add(COLMLNAME);
#if false
            WgMlSysCmpy wmcmp = new WgMlSysCmpy();
            wmcmp.OpenTable(cmpname);
            DataRow dEditRow = cmptbl.NewRow();
            string mladr = wmcmp.GetMlAddr1();
            if (mladr != "")
            {
                dEditRow[COLCATEG] = "会社";
                //dEditRow[COLNAME] = wmcmp.GetCompanyName();
                dEditRow[COLNAME] = cmpname;
                dEditRow[COLMLADR] = mladr;
                dEditRow[COLMLNAME] = wmcmp.GetMlAddrName1();
                cmptbl.Rows.Add(dEditRow);
                dEditRow = cmptbl.NewRow();
            }
            mladr = wmcmp.GetMlAddr2();
            if (mladr != "")
            {
                dEditRow[COLCATEG] = "会社";
                //dEditRow[COLNAME] = wmcmp.GetCompanyName();
                dEditRow[COLNAME] = cmpname;
                dEditRow[COLMLADR] = mladr;
                dEditRow[COLMLNAME] = wmcmp.GetMlAddrName2();
                cmptbl.Rows.Add(dEditRow);
            }
            mladr = wmcmp.GetMlAddr3();
            if (mladr != "")
            {
                dEditRow = cmptbl.NewRow();
                dEditRow[COLCATEG] = "会社";
                //dEditRow[COLNAME] = wmcmp.GetCompanyName();
                dEditRow[COLNAME] = cmpname;
                dEditRow[COLMLADR] = mladr;
                dEditRow[COLMLNAME] = wmcmp.GetMlAddrName3();
                cmptbl.Rows.Add(dEditRow);
            }
#endif
        }

        public DataTable GetCmpMailTable()
        {
            return cmptbl;
        }

        //ユーザーレベルのメールリスト作成
        public List<string> OpenUserMailTable(string cmpname, string usrname)
        {
            usrtbl = new DataTable();
            usrtbl.Columns.Add(COLCATEG);
            usrtbl.Columns.Add(COLNAME);
            usrtbl.Columns.Add(COLMLADR);
            usrtbl.Columns.Add(COLMLNAME);
            List<string> usrnames = new List<string>();
#if false
            WgMlSysUser wmusr = new WgMlSysUser();
            wmusr.OpenTable(cmpname, usrname);
            usrnames = wmusr.GetUserNames();
            DataRow dEditRow;
            string mladr;
            if (usrnames.Count > 0)
            {
                int cnt = 0;

                foreach (string uname in usrnames)
                {
                    mladr = wmusr.GetMlAddr1(cnt);
                    if (mladr != "")
                    {
                        dEditRow = usrtbl.NewRow();
                        dEditRow[COLCATEG] = "ユーザー";
                        dEditRow[COLNAME] = uname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmusr.GetMlAddrName1(cnt);
                        usrtbl.Rows.Add(dEditRow);
                    }
                    mladr = wmusr.GetMlAddr2(cnt);
                    if (mladr != "")
                    {
                        dEditRow = usrtbl.NewRow();
                        dEditRow[COLCATEG] = "ユーザー";
                        dEditRow[COLNAME] = uname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmusr.GetMlAddrName2(cnt);
                        usrtbl.Rows.Add(dEditRow);
                    }
                    mladr = wmusr.GetMlAddr3(cnt);
                    if (mladr != "")
                    {
                        dEditRow = usrtbl.NewRow();
                        dEditRow[COLCATEG] = "ユーザー";
                        dEditRow[COLNAME] = uname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmusr.GetMlAddrName3(cnt);
                        usrtbl.Rows.Add(dEditRow);
                    }
                    cnt++;
                }
            }
#endif
            return usrnames;
        }

        public DataTable GetUserMailTable()
        {
            return usrtbl;
        }

        //給油所レベルのメールリスト作成
        public List<string> OpenSiteMailTable(string cmpname, string sitename)
        {
            sitetbl = new DataTable();
            sitetbl.Columns.Add(COLCATEG);
            sitetbl.Columns.Add(COLNAME);
            sitetbl.Columns.Add(COLMLADR);
            sitetbl.Columns.Add(COLMLNAME);
            List<string> sitenames = new List<string>();
#if false
            WgMlSysSite wmsite = new WgMlSysSite();
            wmsite.OpenTable(cmpname, sitename);
            sitenames = wmsite.GetSiteNames();

            string mladr;
            DataRow dEditRow;

            if (sitenames.Count > 0)
            {
                int cnt = 0;
                foreach (string sname in sitenames)
                {
                    WgMlSysSiteMlAdr wmmladr = new WgMlSysSiteMlAdr();
                    string skkcode = wmsite.GetSKKCode(cnt);
                    wmmladr.OpenTable(cmpname, skkcode);
                    mladr = wmmladr.GetMlAddr1();
                    if (mladr != "")
                    {
                        dEditRow = sitetbl.NewRow();
                        dEditRow[COLCATEG] = "施設";
                        dEditRow[COLNAME] = sname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmmladr.GetMlAddrName1();
                        sitetbl.Rows.Add(dEditRow);
                    }
                    mladr = wmmladr.GetMlAddr2();
                    if (mladr != "")
                    {
                        dEditRow = sitetbl.NewRow();
                        dEditRow[COLCATEG] = "施設";
                        dEditRow[COLNAME] = sname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmmladr.GetMlAddrName2();
                        sitetbl.Rows.Add(dEditRow);
                    }
                    mladr = wmmladr.GetMlAddr3();
                    if (mladr != "")
                    {
                        dEditRow = sitetbl.NewRow();
                        dEditRow[COLCATEG] = "施設";
                        dEditRow[COLNAME] = sname;
                        dEditRow[COLMLADR] = mladr;
                        dEditRow[COLMLNAME] = wmmladr.GetMlAddrName3();
                        sitetbl.Rows.Add(dEditRow);
                    }
                    cnt++;
                }
            }
#endif
            return sitenames;
        }

        public DataTable GetSiteMailTable()
        {
            return sitetbl;
        }

        //メールアドレスの有効無効チェック
        protected bool mladrchk(string mladr)
        {
            bool bret = true;
            //途中に空白がある場合のチェック
            if (mladr.TrimEnd() == "")
                return bret;

            if ((mladr.IndexOf(" ") >= 0) || (mladr.IndexOf("　") >= 0))
            {
                bret = false;
                return bret;
            }
            //文字列長さによるチェック
            if (mladr.Length > 128)
            {
                bret = false;
                return bret;
            }
            //正規表現によるチェック
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            System.Text.RegularExpressions.Match m = regex.Match(mladr);
            if (!m.Success)
            {
                bret = false;
            }
            return bret;
        }

        public string RegisterMailAddressCmp(List<string> mladrlst, List<string> mlaliaslst)
        {
            string retstr = "";
            string[] mladrs = mladrlst.ToArray();
            string[] mlalias = mlaliaslst.ToArray();

            //string cmpname = GlobalVar.MlSysCompanyName;
#if false
            WgMlSysCmpy wmcmp = new WgMlSysCmpy();
            if (false == wmcmp.OpenTable(cmpname))
            {
                retstr = "登録できませんでした";
                return retstr;
            }
            string mladr = mladrs[0].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス1が無効です。再入力してください";
            }
            wmcmp.SetMlAddr1(mladr);
            wmcmp.SetMlAddrName1(mlalias[0]);

            mladr = mladrs[1].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス2が無効です。再入力してください";
                return retstr;
            }
            wmcmp.SetMlAddr2(mladr);
            wmcmp.SetMlAddrName2(mlalias[1]);

            mladr = mladrs[2].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス3が無効です。再入力してください";
                return retstr;
            }
            wmcmp.SetMlAddr3(mladr);
            wmcmp.SetMlAddrName3(mlalias[2]);

            if (true == wmcmp.UpdateMailAdr())
            {
                retstr = "登録できました";
                //GroupTable gt = new GroupTable();
                //LogData ComLog = new LogData(cmpname);
                //DateTime dt = DateTime.Now;        //処理開始ログ書き込み
                //ComLog.OpenLogfileOfDay(dt);
                //ComLog.WriteLog(dt.ToString("yyyy/MM/dd HH:mm"), "", cmpname + " メール設定変更");
                //ComLog.CloseLogTable();
            }
            else
            {
                retstr = "登録できませんでした";
            }
#endif
            return retstr;
        }

        public string RegisterMailAddressDst(List<string> mladrlst, List<string> mlaliaslst, string usrname)
        {
            string retstr = "";
            string[] mladrs = mladrlst.ToArray();
            string[] mlalias = mlaliaslst.ToArray();
#if false
            //string cmpname = GlobalVar.MlSysCompanyName;
            WgMlSysUser wmusr = new WgMlSysUser();
            if (false == wmusr.OpenTable(cmpname, usrname))
            {
                retstr = "登録できませんでした";
                return retstr;
            }
            string mladr = mladrs[0].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス1が無効です。再入力してください";
            }
            wmusr.SetMlAddr1(mladr);
            wmusr.SetMlAddrName1(mlalias[0]);

            mladr = mladrs[1].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス2が無効です。再入力してください";
                return retstr;
            }
            wmusr.SetMlAddr2(mladr);
            wmusr.SetMlAddrName2(mlalias[1]);

            mladr = mladrs[2].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス3が無効です。再入力してください";
                return retstr;
            }
            wmusr.SetMlAddr3(mladr);
            wmusr.SetMlAddrName3(mlalias[2]);

            if (true == wmusr.UpdateMailAdr())
            {
                retstr = "登録できました";
                //GroupTable gt = new GroupTable();
                //LogData ComLog = new LogData(cmpname);
                //DateTime dt = DateTime.Now;        //処理開始ログ書き込み
                //ComLog.OpenLogfileOfDay(dt);
                //ComLog.WriteLog(dt.ToString("yyyy/MM/dd HH:mm"), "", cmpname + " メール設定変更");
                //ComLog.CloseLogTable();
            }
            else
            {
                retstr = "登録できませんでした";
            }
#endif
            return retstr;
        }

        public string RegisterMailAddressSite(List<string> mladrlst, List<string> mlaliaslst, string sitename)
        {
            string retstr = "";
            string[] mladrs = mladrlst.ToArray();
            string[] mlalias = mlaliaslst.ToArray();

            //string cmpname = GlobalVar.MlSysCompanyName;
            WgMlSysSite wmsite = new WgMlSysSite();
            if (false == wmsite.OpenTableSite(cmpname, sitename))
            {
                retstr = "登録できませんでした";
                return retstr;
            }
            string skkcode = wmsite.GetSKKCode(0);
            WgMlSysSiteMlAdr wmmladr = new WgMlSysSiteMlAdr();
            wmmladr.OpenTable("*", skkcode);

            string mladr = mladrs[0].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス1が無効です。再入力してください";
            }
            wmmladr.SetMlAddr1(mladr);
            wmmladr.SetMlAddrName1(mlalias[0]);

            mladr = mladrs[1].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス2が無効です。再入力してください";
                return retstr;
            }
            wmmladr.SetMlAddr2(mladr);
            wmmladr.SetMlAddrName2(mlalias[1]);

            mladr = mladrs[2].TrimEnd();
            if (mladrchk(mladr) == false)
            {
                retstr = "登録できませんでした。 - アドレス3が無効です。再入力してください";
                return retstr;
            }
            wmmladr.SetMlAddr3(mladr);
            wmmladr.SetMlAddrName3(mlalias[2]);

            if (true == wmmladr.UpdateMailAdr(skkcode))
            {
                retstr = "登録できました";
                //GroupTable gt = new GroupTable();
                //LogData ComLog = new LogData(cmpname);
                //DateTime dt = DateTime.Now;        //処理開始ログ書き込み
                //ComLog.OpenLogfileOfDay(dt);
                //ComLog.WriteLog(dt.ToString("yyyy/MM/dd HH:mm"), "", cmpname + " メール設定変更");
                //ComLog.CloseLogTable();
            }
            else
            {
                retstr = "登録できませんでした";
            }
            return retstr;
        }

        public string TestMailAddressCmp(List<string> mladrls, List<string> mlnamels, string sitename)
        {
            string retstr = "";
            //string cmpname = GlobalVar.MlSysCompanyName;
            string[] mladrs = mladrls.ToArray();
            string[] mlnames = mlnamels.ToArray();
            List<string> tstmladr = new List<string>();
            List<string> tstmlname = new List<string>();

            WgMlSysCmpy wmcmp = new WgMlSysCmpy();
            if (false == wmcmp.OpenTable(cmpname))
            {
                retstr = "テストできませんでした";
                return retstr;
            }
            string mladr1 = mladrs[0];
            if (mladr1 != "")
            {
                if (mladrchk(mladr1) == false)
                {
                    retstr = "テストできませんでした。 - アドレス1が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr1);
                tstmlname.Add(mlnames[0]);
            }

            string mladr2 = mladrs[1];
            if (mladr2 != "")
            {
                if (mladrchk(mladr2) == false)
                {
                    retstr = "テストできませんでした。 - アドレス2が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr2);
                tstmlname.Add(mladrs[1]);
            }

            string mladr3 = mladrs[2];
            if (mladr3 != "")
            {
                if (mladrchk(mladr3) == false)
                {
                    retstr = "テストできませんでした。 - アドレス3が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr3);
                tstmlname.Add(mladrs[2]);
            }
            if (mladrls.Count > 0)
            {
                SendMail(sitename, tstmladr, tstmlname);
                retstr = "テストメールを送信しました";
                return retstr;
            }
            else
            {
                retstr = "テストできませんでした。 -　有効なメールアドレスが入力されていません。再入力してください";
                return retstr;
            }
            return retstr;
        }

        public string TestMailAddressDst(List<string> mladrls, List<string> mlnamels, string dstname, string sitename)
        {
            string retstr = "";
            //string cmpname = GlobalVar.MlSysCompanyName;
            string[] mladrs = mladrls.ToArray();
            string[] mlnames = mlnamels.ToArray();
            List<string> tstmladr = new List<string>();
            List<string> tstmlname = new List<string>();

            WgMlSysDstr wmdst = new WgMlSysDstr();
            if (false == wmdst.OpenTable(cmpname, dstname))
            {
                retstr = "テストできませんでした";
                return retstr;
            }
            string mladr1 = mladrs[0];
            if (mladr1 != "")
            {
                if (mladrchk(mladr1) == false)
                {
                    retstr = "テストできませんでした。 - アドレス1が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr1);
                tstmlname.Add(mlnames[0]);
            }

            string mladr2 = mladrs[1];
            if (mladr2 != "")
            {
                if (mladrchk(mladr2) == false)
                {
                    retstr = "テストできませんでした。 - アドレス2が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr2);
                tstmlname.Add(mladrs[1]);
            }

            string mladr3 = mladrs[2];
            if (mladr3 != "")
            {
                if (mladrchk(mladr3) == false)
                {
                    retstr = "テストできませんでした。 - アドレス3が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr3);
                tstmlname.Add(mladrs[2]);
            }
            if (mladrls.Count > 0)
            {
                SendMail(sitename, tstmladr, tstmlname);
                retstr = "テストメールを送信しました";
                return retstr;
            }
            else
            {
                retstr = "テストできませんでした。 -　有効なメールアドレスが入力されていません。再入力してください";
                return retstr;
            }
            return retstr;
        }

        public string TestMailAddressSite(List<string> mladrls, List<string> mlnamels, string sitename)
        {
            string retstr = "";
            //string cmpname = GlobalVar.MlSysCompanyName;
            string[] mladrs = mladrls.ToArray();
            string[] mlnames = mlnamels.ToArray();
            List<string> tstmladr = new List<string>();
            List<string> tstmlname = new List<string>();

            WgMlSysSite wmsite = new WgMlSysSite();
            if (false == wmsite.OpenTableSite(cmpname, sitename))
            {
                retstr = "テストできませんでした";
                return retstr;
            }
            string mladr1 = mladrs[0];
            if (mladr1 != "")
            {
                if (mladrchk(mladr1) == false)
                {
                    retstr = "テストできませんでした。 - アドレス1が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr1);
                tstmlname.Add(mlnames[0]);
            }

            string mladr2 = mladrs[1];
            if (mladr2 != "")
            {
                if (mladrchk(mladr2) == false)
                {
                    retstr = "テストできませんでした。 - アドレス2が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr2);
                tstmlname.Add(mladrs[1]);
            }

            string mladr3 = mladrs[2];
            if (mladr3 != "")
            {
                if (mladrchk(mladr3) == false)
                {
                    retstr = "テストできませんでした。 - アドレス3が無効です。再入力してください";
                    return retstr;
                }
                tstmladr.Add(mladr3);
                tstmlname.Add(mladrs[2]);
            }
            if (mladrls.Count > 0)
            {
                SendMail(sitename, tstmladr, tstmlname);
                retstr = "テストメールを送信しました";
                return retstr;
            }
            else
            {
                retstr = "テストできませんでした。 -　有効なメールアドレスが入力されていません。再入力してください";
                return retstr;
            }
            return retstr;
        }

        private void SendMail(string sitename, List<string> toadrs, List<string> names)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                //内容先頭
                string mlmsg = "このメールは送信テスト用です。\n\n昭和機器工業株式会社　警報自動通報メールシステム\n＊このメールは送信専用メールアドレスから配信していますので、返信などは受け付けておりません。\n";
                string subwr = sitename + " メール送信テスト"; //メール件名(Subject)
                string[] toadrarr = toadrs.ToArray();
                string[] namearray = names.ToArray();

                //メールメッセージにヘッダーフッターを追加する。
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(); //toadrarr[0], subwr, mlmsg);
                msg.From = new System.Net.Mail.MailAddress("n-matsuda@skk-atgs.jp", "昭和機器工業警報自動通報メールシステム");
                if (namearray[0] == "")
                    msg.To.Add(new System.Net.Mail.MailAddress(toadrarr[0], toadrarr[0]));
                else
                    msg.To.Add(new System.Net.Mail.MailAddress(toadrarr[0], namearray[0]));
                msg.Subject = subwr;
                msg.Body = mlmsg;

                for (int i = 1; i < toadrarr.Length; i++)
                {
                    if (namearray[i] == "")
                        msg.To.Add(new System.Net.Mail.MailAddress(toadrarr[i], toadrarr[i]));
                    else
                        msg.To.Add(new System.Net.Mail.MailAddress(toadrarr[i], namearray[i]));
                }
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                //SMTPサーバーなどを設定する
                sc.Host = "mail.skk-atgs.jp";
                sc.Port = 25;
                sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //ユーザー名とパスワードを設定する
                sc.Credentials = new System.Net.NetworkCredential("n-matsuda@skk-atgs.jp", "risudon");
                //メッセージを送信する
                sc.Send(msg);

                //後始末
                msg.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}