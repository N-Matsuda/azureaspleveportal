﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Drawing;

namespace WebApplication.Models
{
    //SIRA(日毎）の登録した履歴（在庫、販売、荷卸し)を管理するクラスです。
    public class SiraHistory
    {
        private DataTable SHistDT;
        private DataTable KurohonDT;
        private DataTable AveSalesDT;
        private DateTime lastday;
        public int maxval;
        private string spcskkcode; //指定のskkcode
                                   //SIRA(日毎）の登録した履歴（在庫、販売、荷卸し)を管理するクラスです。

        //コンストラクター
        public SiraHistory()
        {
            DataTableCtrl.InitializeTable(SHistDT);
        }

        //指定されたskkコードに相当する履歴をテーブルに読み込みます。
        public void OpenTableSkkcodeRecord(string skkcode)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                dt = dt.AddHours(-24 * GlobalVar.SHOWDATE);
                string sqlstr = "SELECT * FROM SiraHistory WHERE SKKコード= '" + skkcode + "' AND 日付 >= '" + dt.ToString("yyyyMMdd") + "' ORDER BY 日付";
                DataTableCtrl.InitializeTable(SHistDT);
                SHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SHistDT);

                bool bfound = false;
                dt = DateTime.Now.ToLocalTime();
                string nowstr = dt.ToString("yyyyMMdd");
                //本日の記録がないならば作成する。
                for (int i = 0; i < SHistDT.Rows.Count; i++)
                {
                    if (SHistDT.Rows[i][2].ToString().TrimEnd() == nowstr)
                    {
                        bfound = true;
                        break;
                    }
                }
                if (bfound == false)
                {
                    try
                    {
                        DataUnitTable dtbl = new DataUnitTable();
                        dtbl.OpenTableSkkcode(skkcode);
                        string zstr = dtbl.GetZaikoString();
                        ZaikoStr zs = new ZaikoStr(zstr);
                        zs.Analyze();
                        int numtank = zs.numtank;
                        DataRow brow = SHistDT.NewRow();
                        brow["SKKコード"] = skkcode;
                        brow["日付"] = nowstr;
                        string[] invstr = zs.vollst.ToArray();
                        string[] slstr = zs.salesvollst.ToArray();

                        for (int i = 0; i < numtank; i++)
                        {
                            brow["タンク" + (i + 1).ToString() + "在庫量"] = invstr[i];
                            brow["タンク" + (i + 1).ToString() + "販売量"] = slstr[i];
                            brow["タンク" + (i + 1).ToString() + "荷卸量"] = 0;
                        }
                        SHistDT.Rows.Add(brow);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードの施設の過去１月分の履歴をテーブルに読み込みます。
        public void OpenTableSkkcodeRecordPastMonth(string skkcode)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                lastday = dt;
                dt = dt.AddMonths(-1);
                string sqlstr = "SELECT * FROM SiraHistory WHERE SKKコード= '" + skkcode + "' AND 日付 >= '" + dt.ToString("yyyyMMdd") + "' ORDER BY 日付";
                DataTableCtrl.InitializeTable(SHistDT);
                SHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SHistDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //指定されたSKKコードの施設の指定された月のデータを取得
        public void OpenTableSkkcodeRecordMonth(string skkcode, DateTime spcdt)
        {
            try
            {
                DateTime nowdt = DateTime.Now.ToLocalTime();
                int nowyear = nowdt.Year;
                int nowmonth = nowdt.Month;
                int nowday = nowdt.Day;

                DateTime fromdt;        //開始日
                DateTime enddt;          //終了日
                bool bfirst = false; //true 指定された月の前半 false 指定された月の後半
                int day = spcdt.Day;
                int year = spcdt.Year;
                int month = spcdt.Month;

                spcskkcode = skkcode;
                if ((nowyear == year) && (nowmonth == month) && (nowday == day))
                {                                                               //指定された日が本日
                    OpenTableSkkcodeRecord(skkcode);
                    lastday = nowdt;
                    return;
                }
                bfirst = false;
                fromdt = new DateTime(year, month, 1);
                DateTime tmpdt = fromdt.AddMonths(1);
                year = tmpdt.Year;
                month = tmpdt.Month;
                enddt = new DateTime(year, month, 1);
                enddt = enddt.AddDays(-1);

                lastday = enddt;
                string sqlstr = "SELECT * FROM SiraHistory WHERE SKKコード= '" + skkcode + "' AND 日付 >= '" + fromdt.ToString("yyyyMMdd") + "' AND 日付 <= '" + enddt.ToString("yyyyMMdd") + "' ORDER BY 日付";

                DataTableCtrl.InitializeTable(SHistDT);
                SHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SHistDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたskkコードの指定された月の前半または後半のデータを取得
        public void OpenTableSkkcodeRecordHalfMonth(string skkcode, DateTime spcdt)
        {
            try
            {
                DateTime nowdt = DateTime.Now.ToLocalTime();
                int nowyear = nowdt.Year;
                int nowmonth = nowdt.Month;
                int nowday = nowdt.Day;

                DateTime fromdt;        //開始日
                DateTime enddt;          //終了日
                bool bfirst = false; //true 指定された月の前半 false 指定された月の後半
                int day = spcdt.Day;
                int year = spcdt.Year;
                int month = spcdt.Month;

                spcskkcode = skkcode;
                if ((nowyear == year) && (nowmonth == month) && (nowday == day))
                {                                                               //指定された日が本日
                    OpenTableSkkcodeRecord(skkcode);
                    lastday = nowdt;
                    return;
                }
                if (day < 15)       //月の前半
                {
                    bfirst = true;
                    fromdt = new DateTime(year, month, 1);
                    enddt = new DateTime(year, month, 15);
                }
                else              //月の後半
                {
                    bfirst = false;
                    fromdt = new DateTime(year, month, 16);
                    DateTime tmpdt = new DateTime(year, month, 1);
                    tmpdt = tmpdt.AddMonths(1);
                    year = tmpdt.Year;
                    month = tmpdt.Month;
                    enddt = new DateTime(year, month, 1);
                    enddt = enddt.AddDays(-1);
                }
                lastday = enddt;

                string sqlstr = "SELECT * FROM SiraHistory WHERE SKKコード= '" + skkcode + "' AND 日付 >= '" + fromdt.ToString("yyyyMMdd") + "' AND 日付 <= '" + enddt.ToString("yyyyMMdd") + "' ORDER BY 日付";
                DataTableCtrl.InitializeTable(SHistDT);
                SHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SHistDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //現在もしくはlastdayで指定された日から半月前までの各日の番号インデックス配列を取り出す
        private int[] GetDateIndex()
        {
            List<int> idxlist = new List<int>();
            string str;
            DateTime dtlim;
            if (lastday == null)            //今日からさかのぼるか、指定された日からさかのぼるか？
                dtlim = DateTime.Now.ToLocalTime();
            else
                dtlim = lastday;

	        if( lastday.Day == 31 )
    	        dtlim = dtlim.AddHours(-24 * (GlobalVar.SHOWDATE + 1));
        	else if(lastday.Day == 29 )
            	dtlim = dtlim.AddHours(-24 * (GlobalVar.SHOWDATE - 1));
	        else if(lastday.Day == 28 )
    	        dtlim = dtlim.AddHours(-24 * (GlobalVar.SHOWDATE - 2));
        	else
            	dtlim = dtlim.AddHours(-24 * (GlobalVar.SHOWDATE));
            
            for (int i = 0; i < SHistDT.Rows.Count; i++)
            {
                try
                {
                    str = (string)SHistDT.Rows[i][2];
                    str = str.Insert(6, "/").Insert(4, "/");
                    DateTime dt = DateTime.Parse(str);
                    if (dt > dtlim)
                        idxlist.Add(i);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            int[] idxarr = idxlist.ToArray();
            return idxarr;
        }


        //現在もしくはlastdayで指定された日から半月前までの各日の日付け配列を取り出す
        public string[] GetInvDateStr()
        {
            List<string> datelist = new List<string>();
            string str;
            int[] idxarr = GetDateIndex();
            var culture = System.Globalization.CultureInfo.GetCultureInfo("ja-jp");
            foreach (int i in idxarr)
            {
                try
                {
                    str = (string)SHistDT.Rows[i][2];
                    str = str.Insert(6, "/").Insert(4, "/");
                    DateTime dt = DateTime.Parse(str);
                    str = dt.ToString("M月d日") + "(" + dt.ToString("ddd", culture) + ")";
                    datelist.Add(str);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            string[] datearr = datelist.ToArray();
            return datearr;
        }

        //日付け配列中で指定された各日付の指定タンク(連結の場合は複数タンク)の販売量(int)配列を取り出す
        public int[] GetSalesByDayArr(int[] tnoar)
        {
            List<int> saleslist = new List<int>();
            int val = 0;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                val = 0;
                foreach (int tno in tnoar)
                {
                    try
                    {
                        val += (int)SHistDT.Rows[i][3 + (tno - 1) * 3 + 1];
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                }
                saleslist.Add(val);
            }
            int[] salesarr = saleslist.ToArray();
            return salesarr;
        }

        //日付け配列中で指定された各日付の指定タンク(連結の場合は複数タンク)の販売量文字列(string)配列を取り出す
        public string[] GetSalesByDayStrArr(int[] tnoar)
        {
            List<string> saleslist = new List<string>();
            int val = 0;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                val = 0;
                foreach (int tno in tnoar)
                {
                    try
                    {
                        val += (int)SHistDT.Rows[i][3 + (tno - 1) * 3 + 1];
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                }
                saleslist.Add(val.ToString());
            }
            string[] slarr = saleslist.ToArray();
            return slarr;
        }

        //日付け配列中で指定された各日付の指定タンク(連結の場合は複数タンク)の荷卸し量文字列(string)配列を取り出す
        public string[] GetDelivByDayStrArr(int[] tnoar)
        {
            List<string> delivlist = new List<string>();
            int val = 0;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                val = 0;
                foreach (int tno in tnoar)
                {
                    try
                    {
                        val += (int)SHistDT.Rows[i][3 + (tno - 1) * 3 + 2];
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                }
                delivlist.Add(val.ToString());
            }
            string[] slarr = delivlist.ToArray();
            return slarr;
        }

        //日付け配列中で指定された各日付の指定タンクの在庫量(int)配列を取り出す
        public int[] GetInvByDay(int tno)
        {
            List<int> invlist = new List<int>();
            int val;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                try
                {
                    val = (int)SHistDT.Rows[i][3 + (tno - 1) * 3];
                    invlist.Add(val);
                }
                catch (Exception ex)
                {
                    ;
                }
            }
            int[] invarr = invlist.ToArray();
            return invarr;
        }

        //日付け配列中で指定された各日付の指定タンク(連結の場合は複数タンク)の在庫量(int)配列を取り出す
        public int[] GetInvByDayArr(int[] tnoar)
        {
            List<int> invlist = new List<int>();
            int val = 0;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                val = 0;
                foreach (int tno in tnoar)
                {
                    try
                    {
                        val += (int)SHistDT.Rows[i][3 + (tno - 1) * 3];
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                }
                invlist.Add(val);
            }
            int[] invarr = invlist.ToArray();
            return invarr;
        }

        //日付け配列中で指定された各日付の指定タンクの在庫量文字列(string)配列を取り出す
        public string[] GetInvByDayStr(int tno)
        {
            List<string> invlist = new List<string>();
            int val;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                try
                {
                    val = (int)SHistDT.Rows[i][3 + (tno - 1) * 3];
                    invlist.Add(val.ToString());
                }
                catch (Exception ex)
                {
                    ;
                }
            }
            string[] invarr = invlist.ToArray();
            return invarr;
        }

        //日付け配列中で指定された各日付の指定タンク(連結の場合は複数タンク)の在庫量文字列(string)配列を取り出す
        public string[] GetInvByDayStrArr(int[] tnoar)
        {
            List<string> invlist = new List<string>();
            int val = 0;
            int[] idxarr = GetDateIndex();
            foreach (int i in idxarr)
            {
                val = 0;
                foreach (int tno in tnoar)
                {
                    try
                    {
                        val += (int)SHistDT.Rows[i][3 + (tno - 1) * 3];
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                }
                invlist.Add(val.ToString());
            }
            string[] invarr = invlist.ToArray();
            return invarr;
        }

        //指定されたSKKコード（予めテーブル読み取り時に指定:spcskkcode)の各油種の履歴(在庫、販売、荷卸し)をCSV形式の文字列で取り出す
        public string GetSiraHistoryString(string sitename)
        {
            string csvstr = "";
            SiteInfData SiteDat = new SiteInfData();
            SiteDat.OpenTableSkkcode(spcskkcode);

            int cnt = 0;
            string[] dayar = GetInvDateStr();
            int[] rtnoar = SiteDat.GetTankArrFromOilType("レギュラー");
            string[] rinvar = GetInvByDayStrArr(rtnoar);
            string[] rsalesar = GetSalesByDayStrArr(rtnoar);
            string[] rdelivar = GetDelivByDayStrArr(rtnoar);
            int[] htnoar = SiteDat.GetTankArrFromOilType("ハイオク");
            string[] hinvar = GetInvByDayStrArr(rtnoar);
            string[] hsalesar = GetSalesByDayStrArr(rtnoar);
            string[] hdelivar = GetDelivByDayStrArr(rtnoar);
            int[] dtnoar = SiteDat.GetTankArrFromOilType("軽油");
            string[] dinvar = GetInvByDayStrArr(rtnoar);
            string[] dsalesar = GetSalesByDayStrArr(rtnoar);
            string[] ddelivar = GetDelivByDayStrArr(rtnoar);
            int[] ktnoar = SiteDat.GetTankArrFromOilType("灯油");
            string[] kinvar = GetInvByDayStrArr(rtnoar);
            string[] ksalesar = GetSalesByDayStrArr(rtnoar);
            string[] kdelivar = GetDelivByDayStrArr(rtnoar);

            try
            {
                for (int i = 0; i < dayar.Length; i++)
                {
                    csvstr += sitename + " " + spcskkcode + ",";
                    csvstr += dayar[i] + ",";
                    csvstr += "レギュラー" + ",";
                    csvstr += rinvar[i] + ",";
                    csvstr += rsalesar[i] + ",";
                    csvstr += rdelivar[i] + ",";
                    csvstr += "ハイオク" + ",";
                    csvstr += hinvar[i] + ",";
                    csvstr += hsalesar[i] + ",";
                    csvstr += hdelivar[i] + ",";
                    csvstr += "軽油" + ",";
                    csvstr += dinvar[i] + ",";
                    csvstr += dsalesar[i] + ",";
                    csvstr += ddelivar[i] + ",";
                    csvstr += "灯油" + ",";
                    csvstr += kinvar[i] + ",";
                    csvstr += ksalesar[i] + ",";
                    csvstr += kdelivar[i] + ",";
                    csvstr += "\r\n";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvstr;

        }
        //テーブル上の各行の在庫履歴をCSV文字列で取り出す
        public string GetInventroyString()
        {
            string csvstr = "時間,タンク1在庫,タンク2在庫,タンク3在庫,タンク4在庫,タンク5在庫,タンク6在庫,タンク7在庫,タンク8在庫,タンク9在庫,タンク10在庫\r";
            string invstr;
            try
            {
                for (int i = 0; i < SHistDT.Rows.Count; i++)
                {
                    csvstr += SHistDT.Rows[i][2].ToString() + ",";
                    for (int j = 1; j <= 10; j++)
                    {
                        try
                        {
                            invstr = SHistDT.Rows[i][3 + (j - 1) * 3].ToString();
                        }
                        catch (Exception ex)
                        {
                            invstr = "0";
                        }
                        if (j != 10)
                            csvstr += invstr + ",";
                        else
                            csvstr += invstr + "\r";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvstr;
        }

        //テーブルより油種毎の黒本用テーブル(在庫量、販売量、荷受け量)作成
        public DateTime BuildLatestKurohonDT(string skkcode)
        {
            DateTime dt;
            try
            {
                SiteInfData SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);
                DataTableCtrl.InitializeTable(KurohonDT);
                KurohonDT = new DataTable();
                KurohonDT.Columns.Add(new DataColumn("液種", typeof(string)));
                KurohonDT.Columns.Add(new DataColumn("在庫量", typeof(int)));
                KurohonDT.Columns.Add(new DataColumn("販売量", typeof(int)));
                KurohonDT.Columns.Add(new DataColumn("荷受け量", typeof(int)));

                string[] oltypear = { "レギュラー", "ハイオク", "軽油", "灯油" };
                int[] tnoar;
                int rowno = SHistDT.Rows.Count - 1;
                string dtstr = (string)SHistDT.Rows[rowno][2];
                dtstr = dtstr.Insert(6, "/").Insert(4, "/");
                dt = DateTime.Parse(dtstr);
                int inv, sal, del;
                foreach (string oltype in oltypear)
                {
                    inv = sal = del = 0;
                    tnoar = SiteDat.GetTankArrFromOilType(oltype);
                    foreach (int tno in tnoar)
                    {
                        try
                        {
                            inv += (int)SHistDT.Rows[rowno][3 + (tno - 1) * 3]; //在庫量
                            sal += (int)SHistDT.Rows[rowno][3 + (tno - 1) * 3 + 1]; //販売量
                            del += (int)SHistDT.Rows[rowno][3 + (tno - 1) * 3 + 2]; //荷受け量
                        }
                        catch (Exception ex)
                        {
                            ;
                        }
                    }
                    DataRow drow = KurohonDT.NewRow();
                    drow[0] = oltype;
                    drow[1] = inv;
                    drow[2] = sal;
                    drow[3] = del;
                    KurohonDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return DateTime.Now.ToLocalTime();
            }
            return dt;
        }

        //黒本用テーブルを渡す
        public DataTable GetKurohonTable()
        {
            return KurohonDT;
        }

        //最新の油種毎の平均販売量、在庫日数(最新在庫量/平均販売量)テーブルを作成する。
        public void BuildAveSalesTableDT(string skkcode)
        {
            DateTime dt;
            try
            {
                SiteInfData SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);
                DataTableCtrl.InitializeTable(AveSalesDT);
                AveSalesDT = new DataTable();
                AveSalesDT.Columns.Add(new DataColumn("液種", typeof(string)));
                AveSalesDT.Columns.Add(new DataColumn("平均販売量", typeof(int)));
                AveSalesDT.Columns.Add(new DataColumn("在庫日数", typeof(string)));

                string[] oltypear = { "レギュラー", "ハイオク", "軽油", "灯油" };
                int[] tnoar;
                int rowno = SHistDT.Rows.Count - 1;
                int sal, inv;
                int[] sales = { 0 };
                foreach (string oltype in oltypear)
                {
                    inv = 0;
                    sal = 0;
                    tnoar = SiteDat.GetTankArrFromOilType(oltype);
                    foreach (int tno in tnoar)
                    {
                        try
                        {
                            inv += (int)SHistDT.Rows[rowno - 1][3 + (tno - 1) * 3]; //在庫量
                            int[] tnoar2 = { tno };
                            sales = GetSalesByDayArr(tnoar2);
                        }
                        catch (Exception ex)
                        {
                            ;
                        }
                    }
                    DataRow drow = AveSalesDT.NewRow();
                    drow[0] = oltype;
                    //平均販売量計算
                    for (int i = 0; i < sales.Length; i++)
                    {
                        sal += sales[i];
                    }
                    int avr = 0;
                    if (sales.Length > 0)
                        avr = (int)(sal / sales.Length);
                    else
                        avr = 0;
                    drow[1] = avr;

                    //平均在庫日数
                    float numday;
                    if (sales.Length > 0)
                        numday = (float)(inv) / (float)avr;
                    else
                        numday = 0;
                    drow[2] = numday.ToString("0.0");
                    AveSalesDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //最新の油種毎の平均販売量、在庫日数(最新在庫量/平均販売量)テーブルを渡す
        public DataTable GetAveSalesTable()
        {
            return AveSalesDT;
        }

    }
}