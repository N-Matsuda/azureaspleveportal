﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace WebApplication.Models
{
    //ASP.NETユーザーアカウント管理用クラス
    public class UserAccount
    {
        private DataTable UserAccDT;

        //コンストラクター
        public UserAccount()
        {
            DataTableCtrl.InitializeTable(UserAccDT);
        }

        //ASP.NETユーザーテーブル読み込み
        public string OpenTable(string username)
        {
            string cmpname = "";
            try
            {
                string sqlstr = "SELECT AspNetUsers.UserName, AspNetUsers.PhoneNumber FROM AspNetUsers WHERE UserName LIKE '" + username + "%'";
                DataTableCtrl.InitializeTable(UserAccDT);
                UserAccDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, UserAccDT);
                cmpname = GetCompanyName();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return cmpname;
        }

        //会社名取得
        public string GetCompanyName()
        {
            string cmpname = "";
            int idx = 0;
            try
            {
                cmpname = UserAccDT.Rows[0][1].ToString().TrimEnd();
                if ((idx = cmpname.IndexOf("PrimeAdmin")) > 0)
                    cmpname = cmpname.Substring(0, idx);
                else if ((idx = cmpname.IndexOf("SubAdmin")) > 0)
                    cmpname = cmpname.Substring(0, idx);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return cmpname;
        }

        //User accountリスト取得
        public List<string> GetUserAccList()
        {
            List<string> list = new List<string>();
            try
            {
                if (UserAccDT != null)
                {
                    for (int i = 0; i < UserAccDT.Rows.Count; i++)
                    {
                        list.Add(UserAccDT.Rows[i][0].ToString());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //管理者権限チェック
        public bool CheckAdminOrSubAdmin(string username)
        {
            bool badmin = false;
            if (CheckAdmin(username) == true)
                badmin = true;
            else if (CheckSubAdmin(username) == true)
                badmin = true;
            else if (username.StartsWith("n-matsuda") == true)
                badmin = true;
            return badmin;
        }

        //主管理者権限チェック
        public bool CheckAdmin(string username)
        {
            bool badmin = false;
            try
            {
                if (UserAccDT != null)
                {
                    for (int i = 0; i < UserAccDT.Rows.Count; i++)
                    {
                        if (username == UserAccDT.Rows[i][0].ToString().TrimEnd())
                        {
                            string cmpname = UserAccDT.Rows[i][1].ToString().TrimEnd();
                            if (cmpname.IndexOf("PrimeAdmin") > 0)
                            {
                                badmin = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return badmin;
        }
        //サブ管理者権限チェック
        public bool CheckSubAdmin(string username)
        {
            bool badmin = false;
            try
            {
                if (UserAccDT != null)
                {
                    for (int i = 0; i < UserAccDT.Rows.Count; i++)
                    {
                        if (username == UserAccDT.Rows[i][0].ToString().TrimEnd())
                        {
                            string cmpname = UserAccDT.Rows[i][1].ToString().TrimEnd();
                            if (cmpname.IndexOf("SubAdmin") > 0)
                            {
                                badmin = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return badmin;
        }

        //地域(サブ)管理者に昇格する
        public bool UpgradeToSubAdmin(string username, int companyno)
        {
            try
            {
                if (UserAccDT == null)
                {
                    return false;
                }
                for (int i = 0; i < UserAccDT.Rows.Count; i++) //DB　AspNetUserのPhoneNumberのところを地域管理者名の保存場所として使用する。
                {
                    if (UserAccDT.Rows[i][0].ToString().TrimEnd() == username)
                    {
                        LeveCompanyProfile lvcmp = new LeveCompanyProfile();
                        lvcmp.OpenTableById(companyno);
                        string subadname = lvcmp.subadminname;
                        UserAccDT.Rows[i][1] = subadname;
                        string sqlstr = "UPDATE AspNetUsers SET PhoneNumber= '" + subadname + "' WHERE UserName ='" + username + "'";
                        DBCtrl.ExecNonQuery(sqlstr);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }

        //一般利用者に降格する
        public bool DegradeToUser(string username, int companyno)
        {
            try
            {
                if (UserAccDT != null)
                {
                    for (int i = 0; i < UserAccDT.Rows.Count; i++)
                    {
                        if (UserAccDT.Rows[i][0].ToString().TrimEnd() == username)
                        {
                            LeveCompanyProfile lvcmp = new LeveCompanyProfile();
                            lvcmp.OpenTableById(companyno);
                            string accname = lvcmp.compname1;
                            UserAccDT.Rows[i][1] = accname;
                            string sqlstr = "UPDATE AspNetUsers SET PhoneNumber= '" + accname + "' WHERE UserName ='" + username + "'";
                            DBCtrl.ExecNonQuery(sqlstr);
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }

        //ユーザーアカウント削除
        public bool DelAccount(string username)
        {
            bool retv = false;
            try
            {
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                string sqlstr = "DELETE FROM AspNetUsers WHERE UserName='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                retv = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return retv;
            }
            return retv;
        }

    }
}