﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication.Models
{
    //各ユーザーが参照できるSkkコードを管理するクラスです
    public class UserSSTable
    {
        private DataTable UserSSTableDT;
        //private ConnectPortTable cpttbl;
        private DataTable CheckTableDT;
        private List<string> CheckedSSList;
        private const string stDisp = "表示";
        private const string stSkkcode = "SKKコード";
        private const string stSiteName = "施設名";

        //コンストラクター
        public UserSSTable()
        {
            DataTableCtrl.InitializeTable(UserSSTableDT);
        }

        //新規ユーザー作成
        public void CreateNewuser(string username)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                string sqlstr;
                con.ConnectionString = GlobalVar.DBCONNECTION;
                con.Open();
                cmd.CommandText = "SELECT UserName FROM UserReferrableSS WHERE UserName='" + username + "'";
                cmd.Connection = con;

                // SQLを実行します。
                SqlDataReader reader = cmd.ExecuteReader();

                string uname = "";
                // 結果を表示します。
                while (reader.Read())
                {
                    uname = (string)reader.GetValue(0);
                }
                reader.Close();
                if (uname == "")
                {
                    sqlstr = "INSERT INTO UserReferrableSS (UserName,SiteGroup1,SiteGroup2,SiteGroup3,SiteGroup4,SiteGroup5) VALUES ('" + username + "','','','','','')";
                    cmd = new SqlCommand(sqlstr, con);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //ユーザーごとのテーブル読み込み、各SS参照可能かどうかのチェックリスト作成
        public List<string> OpenTable(string username)
        {
            CheckedSSList = new List<string>();
            try
            {
                string sqlstr = "SELECT UserName,SiteGroup1 FROM UserReferrableSS WHERE UserName  ='" + username + "'";
                DataTableCtrl.InitializeTable(UserSSTableDT);
                UserSSTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, UserSSTableDT);

                string unamestr = UserSSTableDT.Rows[0][1].ToString();
                string[] chkdskkcode = new string[0];
                if (unamestr != "")
                {
                    chkdskkcode = unamestr.Split(',');
                }
                CheckedSSList.AddRange(chkdskkcode);

                //ConnectPortTable cpttbl = new ConnectPortTable();
                //cpttbl.OpenTable(GlobalVar.CompanyName);
                CheckTableDT = new DataTable();
                CheckTableDT.Columns.Add(new DataColumn(stDisp, typeof(bool)));
                CheckTableDT.Columns.Add(new DataColumn(stSiteName, typeof(string)));
                CheckTableDT.Columns.Add(new DataColumn(stSkkcode, typeof(string)));
                //string[] ssname = cpttbl.GetSiteNameList().ToArray();
                //string[] skkcode = cpttbl.GetSKKCodeList().ToArray();
#if false
            bool found = false;
                for( int i=0; i< ssname.Length; i++, found = false )
                {
                    DataRow drow = CheckTableDT.NewRow();
                    drow[stSkkcode] = skkcode[i];
                    drow[stSiteName] = ssname[i];
                    for (int j = 0; j < chkdskkcode.Length; j++)
                    {
                        if (skkcode[i] == chkdskkcode[j])
                        {
                            found = true;
                            break;
                        }
                    }
                    if( found == true )
                        drow[stDisp] = true;
                    else
                        drow[stDisp] = false;
                    CheckTableDT.Rows.Add(drow);
                }
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return CheckedSSList;
        }

        public DataTable GetCheckTable()
        {
            return CheckTableDT;
        }

        //指定ユーザーに参照可能なSKKコードのリストを登録
        public bool RegTable(List<string> chksites, string username)
        {
            bool ret = true;
            try
            {
                string sitelst = "";
                bool firstss = true;
                foreach (string siten in chksites)
                {
                    if (firstss == true)
                    {
                        sitelst += siten;
                        firstss = false;
                    }
                    else
                    {
                        sitelst += "," + siten;
                    }
                }
                string sqlstr = "UPDATE UserReferrableSS SET SiteGroup1 = '" + sitelst + "' WHERE UserName ='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

    }
}