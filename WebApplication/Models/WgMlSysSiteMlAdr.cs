﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    public class WgMlSysSiteMlAdr
    {
        private DataSet WgMlSysSiteMlAdrDS;
        private DataTable WgMlSysSiteMlAdrDT;
        private const string stWgMlSysSiteMlAdrDT = "メール警報システム施設メール情報";
        private const int COLCMPNAME = 1;   //会社名
        private const int COLSKKCODE = 2;   //SKKコード
        private const int COLMLADR1 = 3;    //メールアドレス1
        private const int COLMLNAME1 = 4;    //メールアドレス1表示名
        private const int COLMLADR2 = 5;    //メールアドレス2
        private const int COLMLNAME2 = 6;    //メールアドレス2表示名
        private const int COLMLADR3 = 7;    //メールアドレス3
        private const int COLMLNAME3 = 8;    //メールアドレス3表示名

        private string cmpnyname;

        public WgMlSysSiteMlAdr()
        {
            if (WgMlSysSiteMlAdrDS != null)
            {
                WgMlSysSiteMlAdrDS.Clear();
                WgMlSysSiteMlAdrDS.Tables.Clear();
                WgMlSysSiteMlAdrDS.Dispose();
                WgMlSysSiteMlAdrDS = null;
            }
        }

        //テーブル読み込み
        public void OpenTable(string cmpny, string skkcode)
        {
            try
            {
                //string dbpath;
                cmpnyname = cmpny;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;

                if ((cmpny == "*") && (skkcode == "*"))
                    sqlstr = "SELECT * FROM WgMlSysSiteMlAdr";
                else if (cmpny == "*")
                    sqlstr = "SELECT * FROM WgMlSysSiteMlAdr WHERE SKKコード= '" + skkcode + "'";
                else if (skkcode == "*")
                    sqlstr = "SELECT * FROM WgMlSysSiteMlAdr WHERE 会社名= '" + cmpny + "'";
                else
                    sqlstr = "SELECT * FROM WgMlSysSiteMlAdr WHERE 会社名= '" + cmpny + "' AND SKKコード= '" + skkcode + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysSiteMlAdrDS != null)
                {
                    WgMlSysSiteMlAdrDS.Clear();
                    WgMlSysSiteMlAdrDS.Tables.Clear();
                    WgMlSysSiteMlAdrDS.Dispose();
                    WgMlSysSiteMlAdrDS = null;
                }
                WgMlSysSiteMlAdrDS = new DataSet("WgMlSysSiteMlAdrDT");
                WgMlSysSiteMlAdrDS.Tables.Add(stWgMlSysSiteMlAdrDT);
                WgMlSysSiteMlAdrDT = WgMlSysSiteMlAdrDS.Tables[stWgMlSysSiteMlAdrDT];

                dAdp.Fill(WgMlSysSiteMlAdrDS, stWgMlSysSiteMlAdrDT);
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return WgMlSysSiteMlAdrDT.Rows.Count;
        }

        //メールアドレス1取得
        public string GetMlAddr1()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1表示名取得
        public string GetMlAddrName1()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2取得
        public string GetMlAddr2()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2表示名取得
        public string GetMlAddrName2()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3取得
        public string GetMlAddr3()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3表示名取得
        public string GetMlAddrName3()
        {
            string mladr = "";
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    mladr = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1設定
        public void SetMlAddr1(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLADR1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス1表示名設定
        public void SetMlAddrName1(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2設定
        public void SetMlAddr2(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLADR2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2表示名設定
        public void SetMlAddrName2(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3設定
        public void SetMlAddr3(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLADR3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2表示名設定
        public void SetMlAddrName3(string mladr)
        {
            try
            {
                if (WgMlSysSiteMlAdrDT != null)
                {
                    WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス、警報設定のアップデート
        public bool UpdateMailAdr(string skkcode)
        {
            bool bret = true;
            try
            {
                string mladr1 = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR1].ToString();
                string mladr2 = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR2].ToString();
                string mladr3 = WgMlSysSiteMlAdrDT.Rows[0][COLMLADR3].ToString();
                string mlname1 = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME1].ToString();
                string mlname2 = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME2].ToString();
                string mlname3 = WgMlSysSiteMlAdrDT.Rows[0][COLMLNAME3].ToString();

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysSiteMlAdr SET メールアドレス1= '" + mladr1 + "', アドレス表示名1= '" + mlname1 + "', メールアドレス2= '" + mladr2 + "', アドレス表示名2= '" + mlname2 + "', メールアドレス3= '" + mladr3 + "', アドレス表示名3= '" + mlname3 +
                    "' WHERE SKKコード = '" + skkcode + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

    }
}