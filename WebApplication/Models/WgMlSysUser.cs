﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    public class WgMlSysUser
    {
        private DataSet WgMlSysUserDS;
        private DataTable WgMlSysUserDT;
        private const string stWgMlSysUser = "メール警報システムユーザー情報";
        private const int COLUSERNAME = 1;   //ユーザー名
        private const int COLCMPNAME = 2;   //会社名
        private const int COLMLADR1 = 3;    //メールアドレス1
        private const int COLMLNAME1 = 4;    //メールアドレス1表示名
        private const int COLMLADR2 = 5;    //メールアドレス2
        private const int COLMLNAME2 = 6;    //メールアドレス2表示名
        private const int COLMLADR3 = 7;    //メールアドレス3
        private const int COLMLNAME3 = 8;    //メールアドレス3表示名

        private string cmpnyname;
        private string Username; //ユーザー名
        //コンストラクター
        public WgMlSysUser()
        {
            if (WgMlSysUserDS != null)
            {
                WgMlSysUserDS.Clear();
                WgMlSysUserDS.Tables.Clear();
                WgMlSysUserDS.Dispose();
                WgMlSysUserDS = null;
            }
        }

        //テーブル読み込み
        public bool OpenTable(string cmpny, string User)
        {
            bool bret = true;
            if (User == GlobalVar.AllSites) //全て
                User = "*";
            try
            {
                //string dbpath;
                cmpnyname = cmpny;
                Username = User;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;

                if (((cmpny == "*") || (cmpny == "")) && ((User == "*") || (User == "")))
                    sqlstr = "SELECT * FROM WgMlSysUser";
                else if ((User == "*") || (User == ""))
                    sqlstr = "SELECT * FROM WgMlSysUser WHERE 会社名= '" + cmpny + "'";
                else
                    sqlstr = "SELECT * FROM WgMlSysUser WHERE 会社名= '" + cmpny + "' AND ユーザー名= '" + User + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysUserDS != null)
                {
                    WgMlSysUserDS.Clear();
                    WgMlSysUserDS.Tables.Clear();
                    WgMlSysUserDS.Dispose();
                    WgMlSysUserDS = null;
                }
                WgMlSysUserDS = new DataSet("WgMlSysUser");
                WgMlSysUserDS.Tables.Add(stWgMlSysUser);
                WgMlSysUserDT = WgMlSysUserDS.Tables[stWgMlSysUser];

                dAdp.Fill(WgMlSysUserDS, stWgMlSysUser);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return WgMlSysUserDT.Rows.Count;
        }

        //メールアドレス1取得
        public string GetMlAddr1(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLADR1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1表示名取得
        public string GetMlAddrName1(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLNAME1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2取得
        public string GetMlAddr2(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLADR2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2表示名取得
        public string GetMlAddrName2(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLNAME2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3取得
        public string GetMlAddr3(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLADR3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3表示名取得
        public string GetMlAddrName3(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysUserDT != null) && (lineno < WgMlSysUserDT.Rows.Count))
                {
                    mladr = WgMlSysUserDT.Rows[lineno][COLMLNAME3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //ユーザーリスト取得
        //会社名一覧取得
        public List<string> GetUserNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < WgMlSysUserDT.Rows.Count; i++)
            {
                list.Add(WgMlSysUserDT.Rows[i][COLUSERNAME].ToString().TrimEnd());
            }
            return list;
        }

        //メールアドレス1設定
        public void SetMlAddr1(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLADR1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス1表示名設定
        public void SetMlAddrName1(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLNAME1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2設定
        public void SetMlAddr2(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLADR2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2表示名設定
        public void SetMlAddrName2(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLNAME2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3設定
        public void SetMlAddr3(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLADR3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3表示名設定
        public void SetMlAddrName3(string mladr)
        {
            try
            {
                if (WgMlSysUserDT != null)
                {
                    WgMlSysUserDT.Rows[0][COLMLNAME3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス、警報設定のアップデート
        public bool UpdateMailAdr()
        {
            bool bret = true;
            try
            {
                string mladr1 = WgMlSysUserDT.Rows[0][COLMLADR1].ToString();
                string mladr2 = WgMlSysUserDT.Rows[0][COLMLADR2].ToString();
                string mladr3 = WgMlSysUserDT.Rows[0][COLMLADR3].ToString();
                string mlname1 = WgMlSysUserDT.Rows[0][COLMLNAME1].ToString();
                string mlname2 = WgMlSysUserDT.Rows[0][COLMLNAME2].ToString();
                string mlname3 = WgMlSysUserDT.Rows[0][COLMLNAME3].ToString();

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysUser SET メールアドレス1= '" + mladr1 + "', アドレス表示名1= '" + mlname1 + "', メールアドレス2= '" + mladr2 + "', アドレス表示名2= '" + mlname2 + "', メールアドレス3= '" + mladr3 + "', アドレス表示名3= '" + mlname3 +
                    "' WHERE ユーザー名 = '" + Username + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

    }
}