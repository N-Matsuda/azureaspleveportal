﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    public class WrgHistory
    {
        private DataTable WrgHisDT;
        public DateTime zdate { get; set; }

        //コンストラクタ
        public WrgHistory()
        {
            DataTableCtrl.InitializeTable(WrgHisDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM WrgHistory WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(WrgHisDT);
                WrgHisDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, WrgHisDT);

                string str;
                for (int i = 0; i < WrgHisDT.Rows.Count; i++)
                {
                    //時間を時間列形式に変換1310181035 -> 2013年10月18日10時35分
                    str = (string)WrgHisDT.Rows[i][2];
                    str = "20" + str.Insert(10, "分").Insert(8, "時").Insert(6, "日").Insert(4, "月").Insert(2, "年");
                    WrgHisDT.Rows[i][2] = str;

                    //警報文字列がある場合先頭10文字はSKKコードなので削除する。
                    str = (string)WrgHisDT.Rows[i][3];
                    if (str.Length > 10)
                    {
                        WrgHisDT.Rows[i][3] = str.Substring(10);
                    }
                }
                WrgHisDT.Columns.Remove("ID");
                WrgHisDT.Columns.Remove("SKKコード");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //文字列表示
        public void ShowWrgList(System.Web.UI.WebControls.GridView pGridView)
        {
            try
            {
                pGridView.DataSource = WrgHisDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //行数取得
        public int NumWrgList()
        {
            if (WrgHisDT != null)
            {
                return WrgHisDT.Rows.Count;
            }
            else
            {
                return 0;
            }
        }

    }
}