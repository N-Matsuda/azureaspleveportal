﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Drawing;
using System.Data;

namespace WebApplication.Models
{
    /// 在庫履歴を管理するクラスです
    public class ZHistData
    {
        private DataTable ZHistDT;
        private DataTable WtrHistDT;
        private DataTable KurohonDT;
        private DataTable AveSalesDT;
        public int maxval;
        private const int TANKNO = 10;
        private string skkcode;
        //コンストラクター
        public ZHistData()
        {
            DataTableCtrl.InitializeTable(ZHistDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcodeByMonth(string idcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM InvHistory WHERE SKKコード= '" + idcode + "' ORDER BY 日付";
                DataTableCtrl.InitializeTable(ZHistDT);
                ZHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, ZHistDT);
                skkcode = idcode;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcodeRecord(string skkcode)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                dt = dt.AddHours(-24 * GlobalVar.SHOWDATE);
                string sqlstr = "SELECT * FROM InvHistory WHERE SKKコード= '" + skkcode + "' AND 日付 >= '" + dt.ToString("yyyyMMddHHmm") + "' ORDER BY 日付";
                DataTableCtrl.InitializeTable(ZHistDT);
                ZHistDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, ZHistDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //過去15日分の文字列作る 170712 0711
        private DateTime[] GetPast15DaysDT()
        {
            List<DateTime> datelist = new List<DateTime>();
            DateTime dt = DateTime.Now.ToLocalTime();
            DateTime dtpast;

            for (int i = -14; i <= 0; i++)
            {
                dtpast = dt.AddDays(i);
                datelist.Add(dtpast);
            }
            DateTime[] datearr = datelist.ToArray();
            return datearr;
        }

        //過去15日分の文字列作る(曜日込)  7月12日(水)
        public string[] GetPast15DaysOfWeek()
        {
            List<string> datestrlist = new List<string>();
            DateTime[] dtarr = GetPast15DaysDT();
            var culture = System.Globalization.CultureInfo.GetCultureInfo("ja-jp");
            foreach (DateTime dt in dtarr)
            {
                datestrlist.Add(dt.ToString("M月d日") + "(" + dt.ToString("ddd",culture) + ")");
            }
            string[] datearr = datestrlist.ToArray();
            return datearr;
        }

        //過去15日分の在庫数列作る
        public int[] GetPast15DayValue(int[] tnoar)
        {
            List<int> invlist = new List<int>();
            DateTime[] dtarr = GetPast15DaysDT();
            DateTime dtnow = DateTime.Now.ToLocalTime();
            int[] invarr = { 0 };
            int val = 0;
            maxval = 0;
            bool bfound = false;
            string daystr = "";
            if (ZHistDT == null)
            {
                return invarr;
            }
            DateTime pastdays, recday;
            TimeSpan ts;
            foreach (DateTime dt in dtarr)
            {
                pastdays = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
                bfound = false;
                val = 0;
                for (int i = 0; i < ZHistDT.Rows.Count; i++)
                {
                    try
                    {
                        //daystr = "20" + ZHistDT.Rows[i][2].ToString().TrimEnd();
                        daystr = ZHistDT.Rows[i][2].ToString().TrimEnd();
                        daystr = daystr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                        recday = DateTime.Parse(daystr);
                        ts = recday.Subtract(pastdays);
                        if( ( ts.TotalMinutes > -10 ) && (ts.TotalMinutes < 10 ) )
                        {
                            foreach (int tno in tnoar)
                            {
                                val += (int)ZHistDT.Rows[i][3 + tno - 1] / 100;
                            }
                            bfound = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                if (bfound == false)
                    val = 0;
                invlist.Add(val);
            }
            invarr = invlist.ToArray();
            return invarr;
        }

        //過去15日分の在庫数文字列作る
        public string[] GetPast15DayValueStr(int[] tnoarr)
        {
            List<string> invlist = new List<string>();
            int[] invstrar = GetPast15DayValue(tnoarr);
            foreach (int val in invstrar)
            {
                invlist.Add(val.ToString());
            }
            string[] invarr = invlist.ToArray();
            return invarr;
        }

        //ダウンロードCSVファイルの作成 水データ無し
        //"SS名, ID, 集信日時,タンク番号,液種,全容量,在庫\r\n"
        public string GetInventroyHistoryStringWOWtr(string ssname, string skkcode)
        {
            string csvstr = "";
            string invstr;
            try
            {
                SiteInfData sinf = new SiteInfData();
                sinf.OpenTableSkkcode(skkcode);
                for (int i = 0; i < ZHistDT.Rows.Count; i++)
                {
                    string datestr = ZHistDT.Rows[i][2].ToString();
                    datestr = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                    csvstr += datestr + ",";
                    for (int j = 1; j <= TANKNO; j++)
                    {
                        int capa = sinf.GetCapacityByTank(j);
                        try
                        {
                            if (capa == 0)
                            {
                                csvstr += ",,,,";
                            }
                            else
                            {
                                csvstr += j.ToString() + "," + sinf.GetOilTypeByTank(j) + "," + capa + ",";
                                int vol = int.Parse(ZHistDT.Rows[i][2 + j].ToString()) / 100;
                                csvstr += vol.ToString() + ",";
                            }
                        }
                        catch (Exception ex)
                        {
                            ;
                        }
                        if (j == TANKNO)
                            csvstr += "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvstr;
        }

        //ダウンロードCSVファイルの作成 水データ有り
        //"SS名, ID, 集信日時,タンク番号,液種,全容量,在庫,水位,水量\r\n"
        public string GetInventroyHistoryStringWWtr(string ssname, string skkcode)
        {
            string csvstr = "";
            string invstr, wtrlvlstr, wtrvolstr;
            int wtrlvl, wtrvol;
            try
            {
                SiteInfData sinf = new SiteInfData();
                sinf.OpenTableSkkcode(skkcode);
                int widx;
                for (int i = 0; i < ZHistDT.Rows.Count; i++)
                {
                    string datestr = "20" + ZHistDT.Rows[i][2].ToString().TrimEnd();
                    datestr = datestr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                    csvstr += datestr + ",";

                    //在庫履歴の時間にとられた水情報があるかチェックする。
                    bool bwtrfound = false;
                    for (widx = 0; widx < WtrHistDT.Rows.Count; widx++)
                    {
                        if (WtrHistDT.Rows[widx][2].ToString().TrimEnd() == datestr)
                        {
                            bwtrfound = true;
                            break;
                        }
                    }
                    for (int j = 1; j <= TANKNO; j++)
                    {
                        try
                        {
                            int capa = sinf.GetCapacityByTank(j);
                            if (capa == 0)
                            {
                                csvstr += ",,,,,,";
                            }
                            else
                            {
                                csvstr += j.ToString() + "," + sinf.GetOilTypeByTank(j) + "," + capa + ",";
                                invstr = ZHistDT.Rows[i][2 + j].ToString();
                                if (bwtrfound == true)
                                {
                                    wtrlvl = int.Parse(WtrHistDT.Rows[widx][3 + (j - 1) * 2].ToString());
                                    wtrvol = int.Parse(WtrHistDT.Rows[widx][3 + (j - 1) * 2 + 1].ToString());
                                    wtrlvl = (wtrlvl + 50) / 100;
                                    wtrvol = (wtrvol + 50) / 100;
                                    wtrlvlstr = wtrlvl.ToString();
                                    wtrvolstr = wtrvol.ToString();
                                }
                                else
                                {
                                    wtrlvlstr = "0";
                                    wtrvolstr = "0";
                                }
                                csvstr += invstr + "," + wtrlvlstr + "," + wtrvolstr + ",";
                            }
                        }
                        catch (Exception ex)
                        {
                            ;
                        }
                        if (j == TANKNO)
                            csvstr += "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvstr;
        }

        //ダウンロードCSVファイルの作成
        public string GetInventroyString()
        {
            string csvstr = "時間,タンク1在庫,タンク2在庫,タンク3在庫,タンク4在庫,タンク5在庫,タンク6在庫,タンク7在庫,タンク8在庫,タンク9在庫,タンク10在庫\r";
            string invstr;
            try
            {
                for (int i = 0; i < ZHistDT.Rows.Count; i++)
                {
                    csvstr += ZHistDT.Rows[i][2].ToString() + ",";
                    for (int j = 1; j <= TANKNO; j++)
                    {
                        try
                        {
                            invstr = ZHistDT.Rows[i][2 + j].ToString();
                        }
                        catch (Exception ex)
                        {
                            invstr = "0";
                        }
                        if (j != TANKNO)
                            csvstr += invstr + ",";
                        else
                            csvstr += invstr + "\r";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvstr;
        }

        //最新の油種毎の黒本用データ作成
        public DateTime BuildLatestKurohonDT(string skkcode)
        {
            DateTime dt;
            try
            {
                SiteInfData SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);

                DataTableCtrl.InitializeTable(KurohonDT);
                KurohonDT = new DataTable();
                KurohonDT.Columns.Add(new DataColumn("液種", typeof(string)));
                KurohonDT.Columns.Add(new DataColumn("在庫量", typeof(string)));
                KurohonDT.Columns.Add(new DataColumn("販売量", typeof(string)));
                KurohonDT.Columns.Add(new DataColumn("荷受け量", typeof(string)));

                string[] oltypear = { "レギュラー", "ハイオク", "軽油", "灯油" };
                int rowno = ZHistDT.Rows.Count - 1;
                string dtstr = "20" + (string)ZHistDT.Rows[rowno][2];
                dtstr = dtstr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                dt = DateTime.Parse(dtstr);
                int inv;
                int[] tnoar;
                int[] invar;

                foreach (string oltype in oltypear)
                {
                    tnoar = SiteDat.GetTankArrFromOilType(oltype);
                    invar = GetPast15DayValue(tnoar);
                    inv = invar[invar.Length - 1];
                    DataRow drow = KurohonDT.NewRow();
                    drow[0] = oltype;
                    drow[1] = inv.ToString("#,0");
                    drow[2] = "データなし";
                    drow[3] = "データなし";
                    KurohonDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return DateTime.Now.ToLocalTime();
            }
            return dt;
        }

        public DataTable GetKurohonTable()
        {
            try
            {
                return KurohonDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }

        //最新の油種毎の平均販売量、在庫日数(最新在庫量/平均販売量)テーブルを作成する。
        public void BuildAveSalesTableDT(string skkcode)
        {
            DateTime dt;
            try
            {
                SiteInfData SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);

                DataTableCtrl.InitializeTable(AveSalesDT);
                AveSalesDT = new DataTable();
                AveSalesDT.Columns.Add(new DataColumn("液種", typeof(string)));
                AveSalesDT.Columns.Add(new DataColumn("平均販売量", typeof(string)));
                AveSalesDT.Columns.Add(new DataColumn("在庫日数", typeof(string)));

                string[] oltypear = { "レギュラー", "ハイオク", "軽油", "灯油" };
                foreach (string oltype in oltypear)
                {
                    DataRow drow = AveSalesDT.NewRow();
                    drow[0] = oltype;
                    drow[1] = "データなし";
                    drow[2] = "データなし";
                    AveSalesDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public DataTable GetAveSalesTable()
        {
            try
            {
                return AveSalesDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }
    }
}