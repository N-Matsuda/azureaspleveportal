﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace WebApplication.Models
{
    //各種在庫テーブルを管理、作成するクラス
    public class ZaikoDataTable
    {
        const int NUMTANK = 20; //最大タンク数
        private string skkcode;
        //private ConnectPortTable cptable;
        private MRouterTable mrtable;
        private DataUnitTable dttable;
        private DataTable ZaikoTable;
        private DataTable TankZaikoTable;
        private DataTable OilZaikoTable;
        private DataTable GraphTankTable;
        private DataTable CheckBoxTable;
        private DataTable ResultBoxTable;
        private DataTable WrningSSTable;
        private struct RecIndex
        {
            public int ZaikoTblIdx { get; set; }
            public int StartIdx { get; set; }
            public int EndIdx { get; set; }
        };
        private RecIndex[] TankZaikoIndex;
        private RecIndex[] OilZaikoIndex;
        private int TankIdx;
        private int OilIdx;
        private int tanknum;
        SiteInfData siteinf;

        //ZaikoTable 列定義
        private const string stSSName = "配送先名"; // 配送先名   (1)
        private const string stDate = "日付";
        private const string stTime = "時刻";
        private const string stSSCode = "SSコード";
        private const string stZaiko = "在庫";
        private const string stOilCode = "液種番号";
        private const string stFullVol = "全容量"; //全容量     
        private const string stTankStatus = "タンク状況";
        private string companyname;
        private string gwskkcode; //GWU skkcode
        private string mbuskkcode; //mobile unit skkcode
        private string username;

        enum ZaikoCol
        {
            HaisouName = 0, // 配送先名
            Date = 1, //日付
            Time = 2, //時刻
            SSCode = 3, //SSｺｰﾄﾞ
            Zaiko = 4, //在庫 4～23
            OilCode = Zaiko + NUMTANK, //油種 24～33
            FullVol = OilCode + NUMTANK, //全容量 34～53
            TankStatus = FullVol + NUMTANK  //状況　54～73
        }

        //全施設一覧用
        //TankZaikoTable, OilZaikoTable 列定義
        private const string stHaisouName = "施設名";
        private const string stComTime = "集信時刻";
        private const string stTankNo = "タンク番号";
        //private const string stOilCode = "液種番号";
        private const string stOilType = "液種";
        //private const string stFullVol = "全容量";
        private const string stZaikoVol = "在庫量";
        private const string stRemVol = "空間容量";
        private const string stDelivVol = "荷受可能量";
        //private const string stTankStatus = "タンク状況";

        //警報発生なし時の文字列
        private const string stNoWrg = "通常";

        enum TankOilCol //タンク別油種別コラムのインデックス
        {
            HaisouName = 0,         //施設名
            ComTime,        //集信時刻
            TankNo,             //タンク番号
            OilCode,            //液種
            OilType,            //液種タイプ
            FullVol,            //全容量
            ZaikoVol,           //在庫量
            RemVol,             //空間容量
            DelivVol,           //荷受け可能量
            TankStatus,         //タンク状況
        }

        enum TankOilColPerSS //タンク別油種別コラムのインデックス
        {
            TankNo,             //タンク番号
            OilType,            //液種タイプ
            FullVol,            //全容量
            ZaikoVol,           //在庫量
            RemVol,             //空間容量
            TankStatus,         //タンク状況
        }

        enum CheckTblCol //タンク別油種別コラムのインデックス
        {
            Checked = 0,
            HaisouName,         //施設名
            Description,        //説明
            Tank1,              //集信時刻
            Tank1Img,           //タンク番号
            Tank2,              //集信時刻
            Tank2Img,           //タンク番号
            Tank3,              //集信時刻
            Tank3Img,           //タンク番号
            Tank4,              //集信時刻
            Tank4Img,           //タンク番号
            Tank5,              //集信時刻
            Tank5Img,           //タンク番号
            Tank6,              //集信時刻
            Tank6Img,           //タンク番号
            Tank7,              //集信時刻
            Tank7Img,           //タンク番号
            Tank8,              //集信時刻
            Tank8Img,           //タンク番号
            Tank9,              //集信時刻
            Tank9Img,           //タンク番号
            Tank10,              //集信時刻
            Tank10Img           //タンク番号
        }

        enum ResultTblCol //タンク別油種別コラムのインデックス
        {
            HaisouName = 0,         //施設名
            Description,        //説明
            Tank1,              //集信時刻
            Tank1Img,           //タンク番号
            Tank2,              //集信時刻
            Tank2Img,           //タンク番号
            Tank3,              //集信時刻
            Tank3Img,           //タンク番号
            Tank4,              //集信時刻
            Tank4Img,           //タンク番号
            Tank5,              //集信時刻
            Tank5Img,           //タンク番号
            Tank6,              //集信時刻
            Tank6Img,           //タンク番号
            Tank7,              //集信時刻
            Tank7Img,           //タンク番号
            Tank8,              //集信時刻
            Tank8Img,           //タンク番号
            Tank9,              //集信時刻
            Tank9Img,           //タンク番号
            Tank10,              //集信時刻
            Tank10Img           //タンク番号
        }

        //コンストラクタ1
        public ZaikoDataTable(string cmpname, string[] idcodes)
        {
            dttable = new DataUnitTable();
            dttable.OpenTableSkkcodes(idcodes);
            skkcode = idcodes[0];
#if false
            skkcode = idcode;
            if (skkcode.StartsWith("M") == true)
            {
                mrtable = new MRouterTable();
                mrtable.OpenTableSkkcode(skkcode);
            }
            else
            {
                cptable = new ConnectPortTable();
                //cptable.OpenTableUsername(cmpname, username);
                cptable.OpenTableSkkcode(idcode);
            }
#endif
            companyname = cmpname;
        }

        //コンストラクタ2
        public ZaikoDataTable(string cmpname, string scode)
        {
            dttable = new DataUnitTable();
            dttable.OpenTableSkkcode(scode);
            skkcode = scode;
            companyname = cmpname;
        }

        //在庫テーブル(ZaikoTable)の作成 SS名、日付、時間、SSコード、各タンク在庫量、各タンク油種、各タンク容量
        public string CreateZaikoTable(bool wrnonly)
        {
            string zstr, dstring = "";
            string strwk, strwk2;
            int inv, capa;

            //int numcnt =  (skkcode.StartsWith("M") == true)? mrtable.GetNumOfRecord(): cptable.GetNumOfRecord();
            int numcnt = dttable.GetNumOfRecord();
            if (numcnt == 0)
                return dstring;
            DataTableCtrl.InitializeTable(ZaikoTable);
            //テーブルのフィールドを定義する
            ZaikoTable = new DataTable();
            string[] columnlst = { stSSName, stDate, stTime, stSSCode }; 
            ZaikoTable.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stZaiko + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stOilCode + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stFullVol + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stTankStatus + i.ToString(), typeof(string)));
            }
            bool foundwrg = false;
            for (int i = 0; i < numcnt; i++)
            {
                zstr = "";
                siteinf = new SiteInfData();
                siteinf.OpenTableSkkcode(skkcode);  //該SKKコードの施設情報

                DataRow drow = ZaikoTable.NewRow();

                foundwrg = false;
                string sts = "";

                drow[(int)ZaikoCol.HaisouName] = dttable.GetSSName(i);  //SS名取り出し
                drow[(int)ZaikoCol.SSCode] = skkcode;
                zstr = dttable.GetZaikoStringLine(i);           //在庫文字列取り出し
                if (zstr.Length > 10)
                {
                    drow[(int)ZaikoCol.Date] = "20" + zstr.Substring(0, 6); //在庫文字列より日付、時間取得
                    drow[(int)ZaikoCol.Time] = zstr.Substring(6, 4);
                    dstring = (string)drow[(int)ZaikoCol.Date] + (string)drow[(int)ZaikoCol.Time];
                }
                else
                {
                    continue;
                }
                ZaikoStr zs = new ZaikoStr(zstr);   //在庫文字列解析
                zs.Analyze();

                for (int j = 0; j < zs.numtank; j++)
                {
                    try
                    {
                        //タンク番号
                        int tno;
                        if( true == int.TryParse(zs.tanknolst[j], out tno) )
                        {
                            if (tno == 0)
                                continue;
                            tno -= 1;
                        } else
                        {
                            tno = j;
                        }
                        //液種
                        strwk = zs.lqtypelst[j];
                        drow[(int)ZaikoCol.OilCode + tno] = int.Parse(strwk);

                        //在庫量, タンク容量取得
                        inv = int.Parse(zs.vollst[j]);
                        drow[(int)ZaikoCol.Zaiko + tno] = inv;
                        //capa = int.Parse(zstr.Substring(hdoffset + OFFSTANK * j + OFFSCAPA, 6));
                        capa = siteinf.GetCapacityByTank(tno + 1);
                        //全容量
                        drow[(int)ZaikoCol.FullVol + tno] = capa;
                        if( capa > 0 )
                        {
	                        //警報
    	                    strwk = zs.sslan2stat[j];
        	                strwk2 = zs.sslanstat[j];
            	            int lowvol = siteinf.GetLowWarningByTank(tno+1);
                	        sts = GetWrngStr(strwk, strwk2, skkcode, j, GlobalVar.ShowHLOnly, inv, lowvol);
                    	    drow[(int)ZaikoCol.TankStatus + tno] += sts;
                        	if ((wrnonly == true) && (sts != GlobalVar.TankNoError))   
	                        {       //警報のみ表示で、警報発生時
    	                        foundwrg = true;
        	                }
        	            }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                //新規行を追加
                if ((wrnonly == false) || (foundwrg == true))   //全て表示か警報のみ表示で、警報発生時
                    ZaikoTable.Rows.Add(drow);
                else
                    drow.Delete();
            }
            Console.WriteLine("Done"); ;
            return dstring;
        }

        //ダウンロード文字列ヘッダー (SS名あり)
        public static string GetInventoryStringHeader()
        {
            string retstr = "";
            //ヘッダー
            retstr += "SS名, ID, 集信日時,";
            for (int i = 1; i <= GlobalVar.NUMTANK; i++)
            {
                if (i == GlobalVar.NUMTANK)
                    retstr += "タンクNo.,液種,全容量,在庫\r\n";
                else
                    retstr += "タンクNo.,液種,全容量,在庫量,";
            }
            return retstr;
        }

        //ダウンロード文字列ヘッダー (SS名なし)
        public static string GetInventoryStringHeaderWOSSName()
        {
            string retstr = "";
            //ヘッダー
            retstr += "集信日時,";
            for (int i = 1; i <= GlobalVar.NUMTANK; i++)
            {
                if (i == GlobalVar.NUMTANK)
                    retstr += "タンクNo.,液種,全容量,在庫,水位,水量\r\n";
                else
                    retstr += "タンクNo.,液種,全容量,在庫,水位,水量,";
            }
            return retstr;
        }

        //在庫テーブル用文字列の作成 SS名、SSコード, 時間、タンク番号、各タンク在庫量、各タンク油種、各タンク容量 x 20
        public string GetInventoryString()
        {
            string retstr = "";
            string zstr, dstring = "";
            string strwk, strwk2;
            int inv, capa;

            int numcnt = dttable.GetNumOfRecord();
            if (numcnt == 0)
                return retstr;

            for (int i = 0; i < numcnt; i++)
            {
                zstr = "";
                siteinf = new SiteInfData();
                siteinf.OpenTableSkkcode(skkcode);  //該SKKコードの施設情報


                zstr = dttable.GetZaikoStringLine(i);   //データテーブルより在庫文字列取得
                if (zstr.Length < 10)
                {
                    continue;
                }
                retstr += dttable.GetSSName(i) + ",";
                retstr += skkcode + ",";

                ZaikoStr zs = new ZaikoStr(zstr);   //在庫文字列解析
                zs.Analyze();

                retstr += zs.timestmp.ToString("yyyy/MM/dd hh:mm") + ",";
                for (int j = 0; j < zs.numtank; j++)
                {
                    try
                    {
                        retstr += (j + 1).ToString() + ",";
                        //液種
                        strwk = zs.lqtypelst[j];
                        int oilno = int.Parse(strwk);
                        string olname = OilName.GetOilName(oilno);
                        if (olname == "")
                            olname = siteinf.GetOilTypeByTank(j+1);
                        retstr += olname + ",";
                        //申請容量
                        capa = siteinf.GetCapacityByTank(j + 1);
                        retstr += capa.ToString() + ",";

                        //在庫量
                        inv = int.Parse(zs.vollst[j]);
                        retstr += inv.ToString() + ",";
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                for (int k = zs.numtank; k <= GlobalVar.NUMTANK; k++)
                {
                    retstr += ",,,,,";
                }
                retstr += "\r\n";
            }
            Console.WriteLine("Done"); ;
            return retstr;
        }

        public int GetNumTank()
        {
            try
            {
               return ZaikoTable.Rows.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        //警報テーブル(WarningTable)の作成 SS名、タンク、液種、警報
        public void CreateWarningSSTable()
        {
            DataTableCtrl.InitializeTable(WrningSSTable);
            WrningSSTable = new DataTable();
            string[] columnlst = { "施設名", "タンク", "液種", "警報種類"};
            WrningSSTable.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            int numcnt = dttable.GetNumOfRecord();
            string skkcode;
            for (int i = 1; i <= numcnt; i++)
            {
                string zstr = "";
                skkcode = dttable.GetSSCode(i);
                siteinf = new SiteInfData();
                siteinf.OpenTableSkkcode(skkcode);  //該SKKコードの施設情報

                string sts = "";
                string strwk, strwk2;
                int capa;
                zstr = dttable.GetZaikoStringLine(i);   //在庫文字列取得
                ZaikoStr zs = new ZaikoStr(zstr);

                for (int j = 0; j < zs.numtank; j++)
                {
                    try
                    {
                        capa = siteinf.GetCapacityByTank(j + 1);
                        if (capa > 0)   //施設情報に全容量が記録されている場合
                        {
                            int lowvol = siteinf.GetLowWarningByTank(j+1);
                            int inv = int.Parse(zs.vollst[j]);
                            strwk = zs.sslan2stat[j];
                            strwk2 = zs.sslanstat[j];
                            //警報文字列取得
                            sts = GetWrngStr(strwk, strwk2, skkcode, j, false, inv, lowvol);
                            if (sts != GlobalVar.TankNoError)
                            {
                                //液種
                                strwk = zs.lqtypelst[j];
                                DataRow drow = WrningSSTable.NewRow();
                                drow[0] = dttable.GetSSName(i) + " " + skkcode;
                                drow[1] = (j + 1).ToString();
                                string olname = GetOilNameFromNo(int.Parse(strwk));
                                if (olname == "") //オイル名が決まらなかったときはDBを参照
                                {
                                    olname = siteinf.GetOilTypeByTank(j + 1);
                                }
                                drow[2] = olname;
                                drow[3] = sts;
                                WrningSSTable.Rows.Add(drow);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        }

        //レコード数取得
        public int GetNumOfRecordOfWarningTable()
        {
            try
            { 
                return WrningSSTable.Rows.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }
        //警報テーブル取得
        public DataTable GetWrningTable()
        {
            try
            {
                return WrningSSTable;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }

        //油種コードから油種名への変換
        private string GetOilName(string oilstr)
        {
            string oilname = "";
            int oilnum = 0;
            if (oilstr.Substring(1) != "00")
            {
                oilstr = oilstr.Substring(1, 2);
                oilnum = int.Parse(oilstr);
                switch (oilnum)
                {
                    case 45:
                    default:
                        oilname = "レギュラー";
                        break;
                    case 53:
                        oilname = "ハイオク";
                        break;
                    case 57:
                        oilname = "軽油";
                        break;
                    case 58:
                        oilname = "灯油";
                        break;
                    case 59:
                        oilname = "廃油";
                        break;
                    case 60:
                        oilname = "A重油";
                        break;
                    case 61:
                        oilname = "重油";
                        break;
                    case 68:
                        oilname = "プレミアム軽油";
                        break;
                }
            }
            else
            {
                oilstr = oilstr.Substring(0, 1);
                oilnum = int.Parse(oilstr);
                switch (oilnum)
                {
                    case 1:
                        oilname = "ハイオク";
                        break;
                    case 2:
                    default:
                        oilname = "レギュラー";
                        break;
                    case 3:
                        oilname = "灯油";
                        break;
                    case 4:
                        oilname = "軽油";
                        break;
                    case 6:
                        oilname = "プレミアム軽油";
                        break;
                    case 9:
                        oilname = "廃油";
                        break;
                }
            }
            return oilname;
        }

        //警報文字列取得
        //str --- SSLAN2ステータス 3byte str2 -- SSLANステータス
        private string GetWrngStr(string str, string str2, string skkcode, int tno, bool HLonly, int inv,int lowvol)
        {
            string wrgstr = "";
            byte[] WrgArray = Encoding.ASCII.GetBytes(str);
            byte sts1 = WrgArray[0];
            byte sts2 = WrgArray[1];
            if ((sts1 & 0x10) == 0x10)
            {
                wrgstr += "減,";
            }
            else if (inv < lowvol)
            {
                wrgstr += "減,";
            }
            //else
            //{
            //    if( true == wgsite.ChkLowWrgStsBySite(skkcode, tno) )
            //        wrgstr += "減,";
            //}
            if ((sts1 & 0x08) == 0x08)
                wrgstr += "満,";
            if (HLonly == false)
            {
                if ((sts1 & 0x04) == 0x04)
                    wrgstr += "水,";
                if ((sts1 & 0x02) == 0x02)
                    wrgstr += "センサ,";
                if ((sts1 & 0x01) == 0x01)
                    wrgstr += "リーク,";
                if ((sts2 & 0x20) == 0x20)
                    wrgstr += "下限,";
                if ((sts2 & 0x10) == 0x20)
                    wrgstr += "LC-1,";
                if ((sts2 & 0x08) == 0x08)
                    wrgstr += "LC-4,";
                if ((sts2 & 0x04) == 0x04)
                    wrgstr += "LC-5,";
                if ((sts2 & 0x02) == 0x02)
                    wrgstr += "LC-7,";
                if ((sts2 & 0x01) == 0x01)
                    wrgstr += "LC-8,";
            }
            if (wrgstr.Length > 1)
                wrgstr = wrgstr.Substring(0, wrgstr.Length - 1); //最後の","をとる
            if ((wrgstr.Length == 0) && (str2 != "0"))
            {
                switch (str2)
                {
                    case "1":
                        wrgstr = "満";
                        break;
                    case "2":
                        wrgstr = "減";
                        break;
                    case "3":
                        if (HLonly == false)
                            wrgstr = "水";
                        break;
                    case "4":
                        if (HLonly == false)
                            wrgstr = "満, 水";
                        break;
                    case "5":
                        if (HLonly == false)
                            wrgstr = "減, 水";
                        break;
                }
            }
            if (wrgstr == "")
                wrgstr = GlobalVar.TankNoError;
            return wrgstr;
        }

        //全施設の一覧表示のためのタンクテーブル初期化
        private void TableInit()
        {
            try
            {
                DataTableCtrl.InitializeTable(TankZaikoTable);
                TankZaikoTable = new DataTable();
                DataTableCtrl.InitializeTable(OilZaikoTable);
                OilZaikoTable = new DataTable();

                if (ZaikoTable.Rows.Count > 0)
                {
                    TankZaikoIndex = new RecIndex[ZaikoTable.Rows.Count];
                    OilZaikoIndex = new RecIndex[ZaikoTable.Rows.Count];
                    TankZaikoIndex[0].ZaikoTblIdx = -1;
                    TankZaikoIndex[0].StartIdx = -1;
                    TankZaikoIndex[0].EndIdx = -1;
                    OilZaikoIndex[0].ZaikoTblIdx = -1;
                    OilZaikoIndex[0].StartIdx = -1;
                    OilZaikoIndex[0].EndIdx = -1;
                }
                //タンク別在庫テーブルのフィールドセット'
                TankZaikoTable.Columns.Add(new DataColumn(stHaisouName, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stComTime, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stTankNo, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stOilCode, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stOilType, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stFullVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stZaikoVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stRemVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stDelivVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stTankStatus, typeof(string)));
                //油種別在庫テーブルのフィールドセット
                OilZaikoTable.Columns.Add(new DataColumn(stHaisouName, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stComTime, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stTankNo, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stOilCode, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stOilType, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stFullVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stZaikoVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stRemVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stDelivVol, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stTankStatus, typeof(string)));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //SS毎のタンク表示のための初期化
        private void TableInitPerSS()
        {
            try
            {
                DataTableCtrl.InitializeTable(TankZaikoTable);
                TankZaikoTable = new DataTable();
                DataTableCtrl.InitializeTable(OilZaikoTable);
                OilZaikoTable = new DataTable();

                //タンク別在庫テーブルのフィールドセット'
                TankZaikoTable.Columns.Add(new DataColumn(stTankNo, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stOilType, typeof(string)));
                TankZaikoTable.Columns.Add(new DataColumn(stFullVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stZaikoVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stRemVol, typeof(int)));
                TankZaikoTable.Columns.Add(new DataColumn(stTankStatus, typeof(string)));
                //油種別在庫テーブルのフィールドセット
                OilZaikoTable.Columns.Add(new DataColumn(stTankNo, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stOilType, typeof(string)));
                OilZaikoTable.Columns.Add(new DataColumn(stFullVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stZaikoVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stRemVol, typeof(int)));
                OilZaikoTable.Columns.Add(new DataColumn(stTankStatus, typeof(string)));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //在庫テーブル(ZaikoTable)より表グラフ表示用の油種別(OilZaikoTable)、タンク別テーブル(TankZaikoTable)の作成
        public void CreateTankOilZaikoTable()
        {
            bool bShowOilOrder = false; //false タンク番号順、true 油種順
            TableInit();
            int iVol, iVol2;
            int iNumOilType = 30;
            int iOilCode = 0;
            bool[] bOilExist = new bool[iNumOilType + 1]; //油種別テーブル作成のための配列
            int iTankRecNo = 0;  //タンクレコード用のインデックス番号
            int iOilRecNo = 0;   //オイルレコード用のインデックス番号
            int iIndxTblNo = 0;  //タンク/オイル別在庫データと在庫データの関連を記録しておくインデックステーブルのアイテム数
            bool FoundRow = false;

            try
            {
                for (int i = 0; i < ZaikoTable.Rows.Count; i++)
                {
                    DataRow dZaikoRow = ZaikoTable.Rows[i];
                    //新しい行を確保
                    DataRow dTankRow = TankZaikoTable.NewRow();
                    DataRow dOilRow = OilZaikoTable.NewRow();

                    //レコード番号管理用のテーブル
                    TankZaikoIndex[iIndxTblNo].ZaikoTblIdx = i;
                    TankZaikoIndex[iIndxTblNo].StartIdx = iTankRecNo;
                    TankZaikoIndex[iIndxTblNo].EndIdx = iTankRecNo;
                    OilZaikoIndex[iIndxTblNo].ZaikoTblIdx = i;
                    OilZaikoIndex[iIndxTblNo].StartIdx = iOilRecNo;
                    OilZaikoIndex[iIndxTblNo].EndIdx = iOilRecNo;

                    iTankRecNo += 1;
                    iOilRecNo += 1;
                    //配送先
                    dTankRow[(int)TankOilCol.HaisouName] = dZaikoRow[(int)ZaikoCol.HaisouName];
                    dOilRow[(int)TankOilCol.HaisouName] = dZaikoRow[(int)ZaikoCol.HaisouName];
                    //時間フォーマット変換後、時間セット
                    string timestr = dZaikoRow[(int)ZaikoCol.Time].ToString();
                    string datestr = dZaikoRow[(int)ZaikoCol.Date].ToString();
                    dTankRow[(int)TankOilCol.ComTime] = ConvertDateStr(datestr) + ConvertTimeStr(timestr);
                    dOilRow[(int)TankOilCol.ComTime] = dTankRow[(int)TankOilCol.ComTime];
                    string skkcode = dZaikoRow[(int)ZaikoCol.SSCode].ToString().TrimEnd();

                    //油種テーブル作成のための配列　油種があった場合にTrueをセット、初期化としてすべてFalseに
                    for (int k = 0; k <= iNumOilType - 1; k++)
                    {
                        bOilExist[k] = false;
                    }

                    if (bShowOilOrder == false) //タンク順
                    {
                        for (int j = 0; j <= NUMTANK - 1; j++)
                        {
                            iVol = (int)dZaikoRow[(int)ZaikoCol.FullVol + j]; //全容量
                            iVol2 = (int)dZaikoRow[(int)ZaikoCol.Zaiko + j]; //在庫
                            //if ((int)dZaikoRow[(int)ZaikoCol.Zaiko + j] != 0)
                            if ((iVol != 0) || (iVol2 != 0))
                            {
                                iOilCode = (int)dZaikoRow[(int)ZaikoCol.OilCode + j];

                                //油種処理
                                if (iOilCode == 9) //廃油をはずす
                                    continue;
                                bOilExist[iOilCode] = true;

                                if (j != 0)
                                {
                                    dTankRow = TankZaikoTable.NewRow();
                                    TankZaikoIndex[iIndxTblNo].EndIdx = iTankRecNo;
                                    iTankRecNo += 1;
                                }
                                //タンク番号、全容量、在庫量
                                dTankRow[(int)TankOilCol.TankNo] = (j + 1).ToString();
                                //iVol = (int)dZaikoRow[(int)ZaikoCol.FullVol + j];
                                dTankRow[(int)TankOilCol.FullVol] = iVol;
                                //iVol2 = (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                                dTankRow[(int)TankOilCol.ZaikoVol] = iVol2;
                                dTankRow[(int)TankOilCol.OilCode] = iOilCode;
                                string olname = GetOilNameFromNo(iOilCode);
                                if (olname == "") //オイル名が決まらなかったときはDBを参照
                                {
                                    SiteInfData sitinf = new SiteInfData();
                                    sitinf.OpenTableSkkcode(skkcode);
                                    olname = sitinf.GetOilTypeByTank(j + 1);
                                }
                                dTankRow[(int)TankOilCol.OilType] = olname;
                                //空き容量
                                if (iVol > iVol2)
                                {
                                    dTankRow[(int)TankOilCol.RemVol] = iVol - iVol2;
                                    //荷受け可能量
                                    iVol = iVol - iVol2;
                                    iVol = iVol / 1000;
                                    if (iVol % 2 != 0)
                                        iVol -= 1;
                                    iVol *= 1000;
                                }
                                else
                                {
                                    dTankRow[(int)TankOilCol.RemVol] = 0;
                                    iVol = 0;
                                }
                                dTankRow[(int)TankOilCol.DelivVol] = iVol;

                                //警報
                                dTankRow[(int)TankOilCol.TankStatus] = dZaikoRow[(int)ZaikoCol.TankStatus + j].ToString();
                                TankZaikoTable.Rows.Add(dTankRow);

                            }
                        }
                    }
                    else //油種順
                    {
                        FoundRow = false;
                        for (int k = 1; k <= iNumOilType; k++)
                        {
                            for (int j = 0; j <= NUMTANK - 1; j++)
                            {
                                iOilCode = (int)dZaikoRow[(int)ZaikoCol.OilCode + j];
                                if ((iOilCode == 0) || (iOilCode == 9)) //廃油をはずす。
                                    continue;
                                if (iOilCode != k)
                                    continue;
                                if (FoundRow == true)
                                {
                                    dTankRow = TankZaikoTable.NewRow();
                                    TankZaikoIndex[iIndxTblNo].EndIdx = iTankRecNo;
                                    iTankRecNo += 1;
                                }
                                FoundRow = true;
                                dTankRow[(int)TankOilCol.TankNo] = (j + 1).ToString();
                                iVol = (int)dZaikoRow[(int)ZaikoCol.FullVol + j];       //全容量
                                dTankRow[(int)TankOilCol.FullVol] = iVol;
                                iVol2 = (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];        //在庫量
                                dTankRow[(int)TankOilCol.ZaikoVol] = iVol2;

                                //油種、空間容量、荷受け可能量は単位に合わせてストリングに変換する必要があるが、これは
                                //表示時に行う。
                                dTankRow[(int)TankOilCol.OilCode] = iOilCode;
                                string olname = GetOilNameFromNo(iOilCode);
                                if (olname == "") //オイル名が決まらなかったときはDBを参照
                                {
                                    SiteInfData sitinf = new SiteInfData();
                                    sitinf.OpenTableSkkcode(skkcode);
                                    olname = sitinf.GetOilTypeByTank(j + 1);
                                }
                                dTankRow[(int)TankOilCol.OilType] = olname;
                                bOilExist[iOilCode] = true;
                                if (iVol > iVol2)
                                {
                                    dTankRow[(int)TankOilCol.RemVol] = iVol - iVol2;
                                    //荷受け可能量
                                    iVol = iVol - iVol2;
                                    iVol /= 1000;
                                    if (iVol % 2 != 0)
                                        iVol -= 1;
                                    iVol *= 1000;
                                }
                                else
                                {
                                    dTankRow[(int)TankOilCol.RemVol] = 0;
                                    iVol = 0;
                                }
                                dTankRow[(int)TankOilCol.DelivVol] = iVol;
                                dTankRow[(int)TankOilCol.TankStatus] = dZaikoRow[(int)ZaikoCol.TankStatus + j];

                                TankZaikoTable.Rows.Add(dTankRow);
                            }
                        }
                    }

                    bool bNewTable = false;
                    bool bFirstData = true;
                    int iTotalVol = 0;  //全容量格納ワーク 
                    int iZaikoVol = 0;  //在庫量格納ワーク
                    int iRemVol = 0;  //空間容量格納ワーク
                    int iDelivVol = 0;  //荷受け量格納ワーク

                    for (int k = 1; k <= iNumOilType; k++)
                    {
                        if (bOilExist[k] == false)  //当オイルに該当するデータなし
                            continue;  //次のオイル種へスキップ
                        if (bNewTable == true) //新しい行を作る
                        {
                            dOilRow = OilZaikoTable.NewRow();
                            OilZaikoIndex[iIndxTblNo].EndIdx = iOilRecNo;
                            iOilRecNo += 1;
                        }
                        bFirstData = true;
                        iTotalVol = 0;
                        iZaikoVol = 0;
                        iRemVol = 0;
                        iDelivVol = 0;

                        //当油種に対するすべてのタンクに対しての合計を求める
                        for (int j = 0; j <= NUMTANK - 1; j++)
                        {
                            if ((int)dZaikoRow[(int)ZaikoCol.OilCode + j] == k)
                            {
                                iTotalVol += (int)dZaikoRow[(int)ZaikoCol.FullVol + j];
                                iZaikoVol += (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                                if ((int)dZaikoRow[(int)ZaikoCol.FullVol + j] > (int)dZaikoRow[(int)ZaikoCol.Zaiko + j])
                                    iRemVol += (int)dZaikoRow[(int)ZaikoCol.FullVol + j] - (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                            }
                        }
                        //合計値をフィールドにセットする

                        dOilRow[(int)TankOilCol.OilCode] = k;
                        dOilRow[(int)TankOilCol.OilType] = GetOilNameFromNo(k);
                        dOilRow[(int)TankOilCol.FullVol] = iTotalVol;
                        dOilRow[(int)TankOilCol.ZaikoVol] = iZaikoVol;
                        dOilRow[(int)TankOilCol.RemVol] = iRemVol;
                        iVol = iRemVol;
                        iVol /= 1000;
                        if (iVol % 2 != 0)
                            iVol -= 1;
                        iVol *= 1000;
                        dOilRow[(int)TankOilCol.DelivVol] = iVol;
                        OilZaikoTable.Rows.Add(dOilRow);
                        bNewTable = true;
                    }
                    iIndxTblNo += 1;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine("Done");
        }

        //在庫テーブル(ZaikoTable)より施設毎の表グラフ表示用の油種別(OilZaikoTable)、タンク別テーブル(TankZaikoTable)の作成
        public void CreateTankOilZaikoTablePerSS()
        {
            bool bShowOilOrder = false; //false タンク番号順、true 油種順
            TableInitPerSS();
            int iVol, iVol2;
            int iNumOilType = 30;
            int numtank = NUMTANK;
            int iOilCode = 0;
            bool[] bOilExist = new bool[iNumOilType + 1]; //油種別テーブル作成のための配列
            int iTankRecNo = 0;  //タンクレコード用のインデックス番号
            int iOilRecNo = 0;   //オイルレコード用のインデックス番号
            int iIndxTblNo = 0;  //タンク/オイル別在庫データと在庫データの関連を記録しておくインデックステーブルのアイテム数
            bool FoundRow = false;

            try
            {
                DataRow dZaikoRow = ZaikoTable.Rows[0];
                //新しい行を確保
                DataRow dTankRow = TankZaikoTable.NewRow();
                DataRow dOilRow = OilZaikoTable.NewRow();

                //油種テーブル作成のための配列　油種があった場合にTrueをセット、初期化としてすべてFalseに
                for (int k = 0; k <= iNumOilType - 1; k++)
                {
                    bOilExist[k] = false;
                }

                if (bShowOilOrder == false) //タンク順
                {
                    string skkcode = dZaikoRow[(int)ZaikoCol.SSCode].ToString().TrimEnd();
                    for (int j = 0; j <= NUMTANK - 1; j++)
                    {
                        try
                        {
                            iVol = (int)dZaikoRow[(int)ZaikoCol.FullVol + j]; //全容量
                            iVol2 = (int)dZaikoRow[(int)ZaikoCol.Zaiko + j]; //在庫
                        }
                        catch(Exception ex1)
                        {
                            continue;
                        }
                        //if ((int)dZaikoRow[(int)ZaikoCol.Zaiko + j] != 0)
                        if ((iVol != 0) || (iVol2 != 0))
                        {
                            iOilCode = (int)dZaikoRow[(int)ZaikoCol.OilCode + j];

                            //油種処理
                            if (iOilCode == 9) //廃油をはずす
                                continue;
                            bOilExist[iOilCode] = true;

                            if (j != 0)
                            {
                                dTankRow = TankZaikoTable.NewRow();
                            }
                            //タンク番号、全容量、在庫量
                            dTankRow[(int)TankOilColPerSS.TankNo] = (j + 1).ToString();
                            dTankRow[(int)TankOilColPerSS.FullVol] = iVol;
                            dTankRow[(int)TankOilColPerSS.ZaikoVol] = iVol2;
                            string olname = GetOilNameFromNo(iOilCode);
                            //特別処理
                            if ((skkcode == "M399000001") && (j == 6))
                                olname = "LSA";     //狭山灯油センター　7番タンクはLSA
                            if (olname == "") //オイル名が決まらなかったときはDBを参照
                            {
                                SiteInfData sitinf = new SiteInfData();
                                sitinf.OpenTableSkkcode(skkcode);
                                olname = sitinf.GetOilTypeByTank(j + 1);
                            }
                            dTankRow[(int)TankOilColPerSS.OilType] = olname;
                            //油種、空間容量、荷受け可能量は単位に合わせてストリングに変換する必要があるが、これは
                            //表示時に行う。
                            if (iVol > iVol2)
                            {
                                dTankRow[(int)TankOilColPerSS.RemVol] = iVol - iVol2;
                            }
                            else
                            {
                                dTankRow[(int)TankOilColPerSS.RemVol] = 0;
                            }
                            //警報
                            dTankRow[(int)TankOilColPerSS.TankStatus] = dZaikoRow[(int)ZaikoCol.TankStatus + j].ToString();
                            TankZaikoTable.Rows.Add(dTankRow);
                            numtank = j;
                        }
                    }
                }
                else //油種順
                {
                    FoundRow = false;
                    for (int k = 1; k <= iNumOilType; k++)
                    {
                        for (int j = 0; j <= NUMTANK - 1; j++)
                        {
                            iOilCode = (int)dZaikoRow[(int)ZaikoCol.OilCode + j];
                            if ((iOilCode == 0) || (iOilCode == 9)) //廃油をはずす。
                                continue;
                            if (iOilCode != k)
                                continue;
                            if (FoundRow == true)
                            {
                                dTankRow = TankZaikoTable.NewRow();
                                TankZaikoIndex[iIndxTblNo].EndIdx = iTankRecNo;
                                iTankRecNo += 1;
                            }
                            FoundRow = true;
                            dTankRow[(int)TankOilColPerSS.TankNo] = (j + 1).ToString();
                            iVol = (int)dZaikoRow[(int)ZaikoCol.FullVol + j];
                            dTankRow[(int)TankOilColPerSS.FullVol] = iVol;
                            iVol2 = (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                            dTankRow[(int)TankOilColPerSS.ZaikoVol] = iVol2;

                            //油種、空間容量、荷受け可能量は単位に合わせてストリングに変換する必要があるが、これは
                            //表示時に行う。
                            if (iVol > iVol2)
                            {
                                dTankRow[(int)TankOilColPerSS.RemVol] = iVol - iVol2;
                            }
                            else
                            {
                                dTankRow[(int)TankOilColPerSS.RemVol] = 0;
                            }
                            string olname = GetOilNameFromNo(iOilCode);
                            if ((skkcode == "M399000001") && (j == 6))
                                olname = "LSA";     //狭山灯油センター　7番タンクはLSA
                            if (olname == "") //オイル名が決まらなかったときはDBを参照
                            {
                                SiteInfData sitinf = new SiteInfData();
                                sitinf.OpenTableSkkcode(skkcode);
                                olname = sitinf.GetOilTypeByTank(j + 1);
                            }
                            dTankRow[(int)TankOilColPerSS.OilType] = olname;
                            bOilExist[iOilCode] = true;
                            dTankRow[(int)TankOilColPerSS.TankStatus] = dZaikoRow[(int)ZaikoCol.TankStatus + j];
                            TankZaikoTable.Rows.Add(dTankRow);
                        }
                    }
                }

                bool bNewTable = false;
                bool bFirstData = true;
                int iTotalVol = 0;  //全容量格納ワーク 
                int iZaikoVol = 0;  //在庫量格納ワーク
                int iRemVol = 0;  //空間容量格納ワーク
                int iDelivVol = 0;  //荷受け量格納ワーク

                for (int k = 1; k <= iNumOilType; k++)
                {
                    if (bOilExist[k] == false)  //当オイルに該当するデータなし
                        continue;  //次のオイル種へスキップ
                    if (bNewTable == true) //新しい行を作る
                    {
                        dOilRow = OilZaikoTable.NewRow();
                    }
                    bFirstData = true;
                    iTotalVol = 0;
                    iZaikoVol = 0;
                    iRemVol = 0;
                    iDelivVol = 0;

                    //当油種に対するすべてのタンクに対しての合計を求める
                    for (int j = 0; j <= numtank - 1; j++)
                    {
                        if ((int)dZaikoRow[(int)ZaikoCol.OilCode + j] == k)
                        {
                            iTotalVol += (int)dZaikoRow[(int)ZaikoCol.FullVol + j];
                            iZaikoVol += (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                            if ((int)dZaikoRow[(int)ZaikoCol.FullVol + j] > (int)dZaikoRow[(int)ZaikoCol.Zaiko + j])
                                iRemVol += (int)dZaikoRow[(int)ZaikoCol.FullVol + j] - (int)dZaikoRow[(int)ZaikoCol.Zaiko + j];
                        }
                    }
                    dOilRow[(int)TankOilColPerSS.RemVol] = iRemVol;
                    //合計値をフィールドにセットする
                    dOilRow[(int)TankOilColPerSS.OilType] = GetOilNameFromNo(k);
                    dOilRow[(int)TankOilColPerSS.FullVol] = iTotalVol;
                    dOilRow[(int)TankOilColPerSS.ZaikoVol] = iZaikoVol;
                    dOilRow[(int)TankOilColPerSS.RemVol] = iRemVol;
                    OilZaikoTable.Rows.Add(dOilRow);
                    bNewTable = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine("Done");
        }

        //"タンク番号(油種名)"文字列配列取得
        public string[] GetTankOilLabel()
        {
            List<string> labellst = new List<string>();
            string[] labelar = new string[] { "" };
            string lblstr;
            try
            {
                for (int i = 0; i < TankZaikoTable.Rows.Count; i++)
                {
                    lblstr = TankZaikoTable.Rows[i][(int)TankOilColPerSS.TankNo] + " (";
                    lblstr += TankZaikoTable.Rows[i][(int)TankOilColPerSS.OilType] + ") ";
                    labellst.Add(lblstr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (labellst.Count > 0)
                labelar = labellst.ToArray();
            return labelar;
        }
        //在庫率文字列配列作成
        public string[] GetTankPerLabel()
        {
            List<string> labellst = new List<string>();
            string[] labelar = new string[] { "" };
            string lblstr;
            int fvol, zvol;
            double perval;
            try
            {
                for (int i = 0; i < TankZaikoTable.Rows.Count; i++)
                {
                    fvol = (int)TankZaikoTable.Rows[i][(int)TankOilColPerSS.FullVol];
                    zvol = (int)TankZaikoTable.Rows[i][(int)TankOilColPerSS.ZaikoVol];
                    perval = (double)zvol / (double)fvol;
                    lblstr = perval.ToString("P1");
                    labellst.Add(lblstr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (labellst.Count > 0)
                labelar = labellst.ToArray();
            return labelar;
        }
        //在庫率数値配列作成
        public int[] GetTankPerVal()
        {
            List<int> vollst = new List<int>();
            int[] volarr = new int[] { 0 };
            int fvol, zvol, pvol;
            double perval;
            try
            {
                for (int i = 0; i < TankZaikoTable.Rows.Count; i++)
                {
                    fvol = (int)TankZaikoTable.Rows[i][(int)TankOilColPerSS.FullVol];
                    zvol = (int)TankZaikoTable.Rows[i][(int)TankOilColPerSS.ZaikoVol];
                    pvol = (int)(100 * zvol / fvol);
                    vollst.Add(pvol);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (vollst.Count > 0)
                volarr = vollst.ToArray();
            return volarr;
        }

        //タンク別テーブル(TankZaikoTable)よりグラフ表示用のテーブル(GraphTankTable)作成
        public void CreateTankGraphTable()
        {
            DataTableCtrl.InitializeTable(GraphTankTable);
            GraphTankTable = new DataTable();
            GraphTankTable.Columns.Add(new DataColumn("施設名", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("説明", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク1", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像1", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク2", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像2", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク3", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像3", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク4", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像4", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク5", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像5", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク6", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像6", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク7", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像7", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク8", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像8", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク9", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像9", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("タンク10", typeof(string)));
            GraphTankTable.Columns.Add(new DataColumn("画像10", typeof(string)));
            try
            {
                if (ZaikoTable.Rows.Count == 0)
                {
                    return;
                }
                int numsite = TankZaikoIndex.Count();
                string str, olname;
                int idx, capa, inv;
                for (int i = 0; i < numsite; i++)
                {
                        DataRow dTankRow = GraphTankTable.NewRow();
                        idx = TankZaikoIndex[i].StartIdx;
                        dTankRow["施設名"] = TankZaikoTable.Rows[idx][(int)TankOilCol.HaisouName].ToString() + "<br />";
                        string dtstr = TankZaikoTable.Rows[idx][(int)TankOilCol.ComTime].ToString();
                        int tpos = dtstr.IndexOf("日");
                        dTankRow["施設名"] += dtstr.Substring(0,tpos+1) + "<br />";
                        dTankRow["施設名"] += dtstr.Substring(tpos+1) + "<br />";
                        dTankRow["施設名"] += ZaikoTable.Rows[i][(int)ZaikoCol.SSCode].ToString();
                        dTankRow["説明"] = "液種<br />" + "空間容量<br />" + "在庫量";
                        int k = 1;
                        for (int j = TankZaikoIndex[i].StartIdx; j <= TankZaikoIndex[i].EndIdx; j++, k++, idx++)
                        {
                            //油種
                            olname = TankZaikoTable.Rows[idx][(int)TankOilCol.OilType].ToString();
                            dTankRow["タンク" + k.ToString()] = olname + "<br />";
                            //フル容量
                            capa = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.FullVol];
                            //残容量
                            inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.RemVol];
                            str = inv.ToString("#,0") + "<br />";
                            dTankRow["タンク" + k.ToString()] += str;
                            //在庫量
                            inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.ZaikoVol];
                            dTankRow["タンク" + k.ToString()] += inv.ToString("#,0") + "<br />";
                            //画像
                            if ((string)TankZaikoTable.Rows[idx][(int)TankOilCol.TankStatus] == stNoWrg)
                            {
                                if (capa == 0)
                                    dTankRow["画像" + k.ToString()] = GetBitmapURL(1000, olname);
                                else
                                    dTankRow["画像" + k.ToString()] = GetBitmapURL(inv * 1000 / capa, olname);
                            }
                            else
                            {
                                if (capa == 0)
                                    dTankRow["画像" + k.ToString()] = GetBitmapURL2(1000);
                                else
                                    dTankRow["画像" + k.ToString()] = GetBitmapURL2(inv * 1000 / capa);
                            }
                        }
                        GraphTankTable.Rows.Add(dTankRow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("Done");
        }


        //--------- 共通関数 020310(2時3分10秒)形式のストリングを表示用の文字列( 2時 3分)に変更する関数
        private string ConvertTimeStr(string DateStr)
        {
            string CnvDateStr = "";

            if (DateStr.Substring(0, 1) == "0")
                CnvDateStr += " " + DateStr.Substring(1, 1) + "時";
            else
                CnvDateStr += DateStr.Substring(0, 2) + "時";

            if (DateStr.Substring(2, 1) == "0")
                CnvDateStr += " " + DateStr.Substring(3, 1) + "分";
            else
                CnvDateStr += DateStr.Substring(2, 2) + "分";

            return CnvDateStr;
        }

        //--------- 共通関数 100310(2010年3月10日)形式のストリングを表示用の文字列に変更する関数
        private string ConvertDateStr(string DateStr)
        {
            string CnvDateStr = "";
            //CnvDateStr = "20" + DateStr.Substring(2, 2) + "年";
            if (DateStr.Substring(4, 1) == "0")
                CnvDateStr += " " + DateStr.Substring(5, 1) + "月";
            else
                CnvDateStr += DateStr.Substring(4, 2) + "月";

            if (DateStr.Substring(6, 1) == "0")
                CnvDateStr += " " + DateStr.Substring(7, 1) + "日";
            else
                CnvDateStr += DateStr.Substring(6, 2) + "日";

            return CnvDateStr;
        }

        //----------- 油種番号より油種名取得
        private string GetOilNameFromNo(int oilno)
        {
            string oilname = "";
            switch (oilno)
            {
                case 1:
                    oilname = "ハイオク";
                    break;
                case 2:
                    oilname = "レギュラー";
                    break;
                case 3:
                    oilname = "灯油";
                    break;
                case 4:
                    oilname = "軽油";
                    break;
                case 6:
                    oilname = "プレミアム軽油";
                    break;
                case 9:
                    oilname = "廃油";
                    break;
                case 45:
                    oilname = "レギュラー";
                    break;
                case 53:
                    oilname = "ハイオク";
                    break;
                case 57:
                    oilname = "軽油";
                    break;
                case 58:
                    oilname = "灯油";
                    break;
                case 59:
                    oilname = "廃油";
                    break;
                case 60:
                    oilname = "A重油";
                    break;
                case 61:
                    oilname = "重油";
                    break;
                case 68:
                    oilname = "プレミアム軽油";
                    break;
            }
            return oilname;
        }

        public DataTable GetZaikoTable()
        {
            try
            {
                return ZaikoTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public DataTable GetTankTable()
        {
            try
            {
                return TankZaikoTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public DataTable GetOilTable()
        {
            try
            {
                return OilZaikoTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }
        public DataTable GetGraphTankTable()
        {
            try
            {
                return GraphTankTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }
        public DataTable GetCheckBoxTable()
        {
            try
            {
                return CheckBoxTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }
        public DataTable GetResultBoxTable()
        {
            try
            {
                return ResultBoxTable;
            }
            catch (Exception e)
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public int GetNumCheckBoxTable()
        {
            try
            {
                return CheckBoxTable.Rows.Count;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //画像URLの取得
        private string GetBitmapURL(int PerVal, string olname)
        {
            string bmurl;
            if (PerVal > 975)
            {
                bmurl = "tank100per.png"; //100%
            }
            else if (PerVal > 925)
            {
                bmurl = "tank95per.png"; //95%
            }
            else if (PerVal > 875)
            {
                bmurl = "tank90per.png"; //90%
            }
            else if (PerVal > 825)
            {
                bmurl = "tank85per.png"; //85%
            }
            else if (PerVal > 775)
            {
                bmurl = "tank80per.png"; //80%
            }
            else if (PerVal > 725)
            {
                bmurl = "tank75per.png"; //75%
            }
            else if (PerVal > 675)
            {
                bmurl = "tank70per.png"; //70%
            }
            else if (PerVal > 625)
            {
                bmurl = "tank65per.png"; //65%
            }
            else if (PerVal > 575)
            {
                bmurl = "tank60per.png"; //60%
            }
            else if (PerVal > 525)
            {
                bmurl = "tank55per.png"; //55%
            }
            else if (PerVal > 475)
            {
                bmurl = "tank50per.png"; //50%
            }
            else if (PerVal > 425)
            {
                bmurl = "tank45per.png"; //45%
            }
            else if (PerVal > 375)
            {
                bmurl = "tank40per.png"; //40%
            }
            else if (PerVal > 325)
            {
                bmurl = "tank35per.png"; //35%
            }
            else if (PerVal > 275)
            {
                bmurl = "tank30per.png"; //30%
            }
            else if (PerVal > 225)
            {
                bmurl = "tank25per.png"; //25%
            }
            else if (PerVal > 175)
            {
                bmurl = "tank20per.png"; //20%
            }
            else if (PerVal > 125)
            {
                bmurl = "tank15per.png"; //15%
            }
            else if (PerVal > 75)
            {
                bmurl = "tank10per.png"; //10%
            }
            else if (PerVal > 25)
            {
                bmurl = "tank5per.png"; //5%
            }
            else
            {
                bmurl = "tank0per.png"; //00%
            }
            if (olname == "レギュラー")
                bmurl = "images/Blue" + bmurl;
            else if (olname == "ハイオク")
                bmurl = "images/Green" + bmurl;
            else if (olname == "軽油")
                bmurl = "images/Orange" + bmurl;
            else if (olname == "灯油")
                bmurl = "images/Purple" + bmurl;
            else
                bmurl = "images/Gray" + bmurl;
            return bmurl;
        }

        private string GetBitmapURL2(int PerVal)
        {
            if (PerVal > 975)
            {
                return "images/Redtank100per.png"; //100%
            }
            else if (PerVal > 925)
            {
                return "images/Redtank95per.png"; //95%
            }
            else if (PerVal > 875)
            {
                return "images/Redtank90per.png"; //90%
            }
            else if (PerVal > 825)
            {
                return "images/Redtank85per.png"; //85%
            }
            else if (PerVal > 775)
            {
                return "images/Redtank80per.png"; //80%
            }
            else if (PerVal > 725)
            {
                return "images/Redtank75per.png"; //75%
            }
            else if (PerVal > 675)
            {
                return "images/Redtank70per.png"; //70%
            }
            else if (PerVal > 625)
            {
                return "images/Redtank65per.png"; //65%
            }
            else if (PerVal > 575)
            {
                return "images/Redtank60per.png"; //60%
            }
            else if (PerVal > 525)
            {
                return "images/Redtank55per.png"; //55%
            }
            else if (PerVal > 475)
            {
                return "images/Redtank50per.png"; //50%
            }
            else if (PerVal > 425)
            {
                return "images/Redtank45per.png"; //45%
            }
            else if (PerVal > 375)
            {
                return "images/Redtank40per.png"; //40%
            }
            else if (PerVal > 325)
            {
                return "images/Redtank35per.png"; //35%
            }
            else if (PerVal > 275)
            {
                return "images/Redtank30per.png"; //30%
            }
            else if (PerVal > 225)
            {
                return "images/Redtank25per.png"; //25%
            }
            else if (PerVal > 175)
            {
                return "images/Redtank20per.png"; //20%
            }
            else if (PerVal > 125)
            {
                return "images/Redtank15per.png"; //15%
            }
            else if (PerVal > 75)
            {
                return "images/Redtank10per.png"; //10%
            }
            else if (PerVal > 25)
            {
                return "images/Redtank5per.png"; //5%
            }
            else
            {
                return "images/Redtank0per.png"; //00%
            }
        }

        //タンク別テーブル(TankZaikoTable)よりcheckbox付グラフ表示用のテーブル(CheckBoxTable)作成
        public void CreateCheckBoxTable(bool errspcd, string[] errsitearr)
        {
            //if (cptable.GetNumOfRecord() == 0)
            //    return;
            DataTableCtrl.InitializeTable(CheckBoxTable);
            //テーブルのフィールドを定義する
            CheckBoxTable = new DataTable();

            CheckBoxTable.Columns.Add(new DataColumn("実行", typeof(bool)));
            CheckBoxTable.Columns.Add(new DataColumn(stSSName, typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("説明", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク1", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像1", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク2", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像2", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク3", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像3", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク4", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像4", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク5", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像5", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク6", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像6", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク7", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像7", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク8", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像8", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク9", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像9", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("タンク10", typeof(string)));
            CheckBoxTable.Columns.Add(new DataColumn("画像10", typeof(string)));

            int numsite = TankZaikoIndex.Count();
            string str, olname;
            int idx, capa, inv;
            for (int i = 0; i < numsite; i++)
            {
                try
                {
                    DataRow dTankRow = CheckBoxTable.NewRow();
                    dTankRow[(int)CheckTblCol.Checked] = false;
                    idx = TankZaikoIndex[i].StartIdx;
                    dTankRow[(int)CheckTblCol.HaisouName] = TankZaikoTable.Rows[idx][(int)TankOilCol.HaisouName].ToString() + "<br />";
                    string skkcode = ZaikoTable.Rows[i][(int)ZaikoCol.SSCode].ToString();
                    if (errspcd == true) //集信失敗施設に登録されている場合は集信失敗を表示
                    {
                        bool found = false;
                        for (int j = 0; j < errsitearr.Count(); j++)
                        {
                            if (errsitearr[j] == skkcode)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == true)
                            dTankRow[(int)CheckTblCol.HaisouName] += "集信失敗 <br />";
                        else
                            dTankRow[(int)CheckTblCol.HaisouName] += TankZaikoTable.Rows[idx][(int)TankOilCol.ComTime].ToString() + "<br />";

                    }
                    else
                    {
                        dTankRow[(int)CheckTblCol.HaisouName] += TankZaikoTable.Rows[idx][(int)TankOilCol.ComTime].ToString() + "<br />";
                    }
                    dTankRow[(int)CheckTblCol.HaisouName] += skkcode;
                    dTankRow[(int)CheckTblCol.Description] = "液種<br />" + "空間容量<br />" + "在庫量";
                    int k = 0;
                    for (int j = TankZaikoIndex[i].StartIdx; j <= TankZaikoIndex[i].EndIdx; j++, k++, idx++)
                    {
                        //油種
                        olname = TankZaikoTable.Rows[idx][(int)TankOilCol.OilType].ToString();
                        dTankRow[(int)CheckTblCol.Tank1 + k * 2] = olname + "<br />";
                        //フル容量
                        capa = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.FullVol];
                        //残容量
                        inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.RemVol];
                        str = inv.ToString("#,0") + "<br />";
                        dTankRow[(int)CheckTblCol.Tank1 + k * 2] += str;
                        //在庫量
                        inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.ZaikoVol];
                        dTankRow[(int)CheckTblCol.Tank1 + k * 2] += inv.ToString("#,0") + "<br />";
                        //画像
                        if ((string)TankZaikoTable.Rows[idx][(int)TankOilCol.TankStatus] == stNoWrg)
                        {
                            if (capa == 0)
                                dTankRow[(int)CheckTblCol.Tank1 + k * 2 + 1] = GetBitmapURL(1000, olname);
                            else
                                dTankRow[(int)CheckTblCol.Tank1 + k * 2 + 1] = GetBitmapURL(inv * 1000 / capa, olname);
                        }
                        else
                        {
                            if (capa == 0)
                                dTankRow[(int)CheckTblCol.Tank1 + k * 2 + 1] = GetBitmapURL2(1000);
                            else
                                dTankRow[(int)CheckTblCol.Tank1 + k * 2 + 1] = GetBitmapURL2(inv * 1000 / capa);
                        }
                    }
                    CheckBoxTable.Rows.Add(dTankRow);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            Console.WriteLine("Done");

        }

        //タンク別テーブル(TankZaikoTable)より指定集信結果取得
        public void CreateResultBoxTable(bool errspcd, string[] errsitearr)
        {
            DataTableCtrl.InitializeTable(ResultBoxTable);
            //テーブルのフィールドを定義する
            ResultBoxTable = new DataTable();

            ResultBoxTable.Columns.Add(new DataColumn(stSSName, typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("説明", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク1", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像1", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク2", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像2", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク3", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像3", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク4", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像4", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク5", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像5", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク6", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像6", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク7", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像7", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク8", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像8", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク9", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像9", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("タンク10", typeof(string)));
            ResultBoxTable.Columns.Add(new DataColumn("画像10", typeof(string)));

            int numsite = TankZaikoIndex.Count();
            string str, olname;
            int idx, capa, inv;
            for (int i = 0; i < numsite; i++)
            {
                try
                {
                    DataRow dTankRow = ResultBoxTable.NewRow();
                    idx = TankZaikoIndex[i].StartIdx;
                    dTankRow[(int)ResultTblCol.HaisouName] = TankZaikoTable.Rows[idx][(int)TankOilCol.HaisouName].ToString() + "<br />";
                    string skkcode = ZaikoTable.Rows[i][(int)ZaikoCol.SSCode].ToString();
                    if (errspcd == true) //集信失敗施設に登録されている場合は集信失敗を表示
                    {
                        bool found = false;
                        for (int j = 0; j < errsitearr.Count(); j++)
                        {
                            if (errsitearr[j] == skkcode)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == true)
                            dTankRow[(int)ResultTblCol.HaisouName] += "集信失敗 <br />";
                        else
                            dTankRow[(int)ResultTblCol.HaisouName] += TankZaikoTable.Rows[idx][(int)TankOilCol.ComTime].ToString() + "<br />";

                    }
                    else
                    {
                        dTankRow[(int)ResultTblCol.HaisouName] += TankZaikoTable.Rows[idx][(int)TankOilCol.ComTime].ToString() + "<br />";
                    }
                    dTankRow[(int)ResultTblCol.HaisouName] += skkcode;
                    dTankRow[(int)ResultTblCol.Description] = "液種<br />" + "空間容量<br />" + "在庫量";
                    int k = 0;
                    for (int j = TankZaikoIndex[i].StartIdx; j <= TankZaikoIndex[i].EndIdx; j++, k++, idx++)
                    {
                        //油種
                        olname = TankZaikoTable.Rows[idx][(int)TankOilCol.OilType].ToString();
                        dTankRow[(int)ResultTblCol.Tank1 + k * 2] = olname + "<br />";
                        //フル容量
                        capa = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.FullVol];
                        //残容量
                        inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.RemVol];
                        str = inv.ToString("#,0") + "<br />";
                        dTankRow[(int)ResultTblCol.Tank1 + k * 2] += str;
                        //在庫量
                        inv = (int)TankZaikoTable.Rows[idx][(int)TankOilCol.ZaikoVol];
                        dTankRow[(int)ResultTblCol.Tank1 + k * 2] += inv.ToString("#,0") + "<br />";
                        //画像
                        if ((string)TankZaikoTable.Rows[idx][(int)TankOilCol.TankStatus] == stNoWrg)
                        {
                            if (capa == 0)
                                dTankRow[(int)ResultTblCol.Tank1 + k * 2 + 1] = GetBitmapURL(1000, olname);
                            else
                                dTankRow[(int)ResultTblCol.Tank1 + k * 2 + 1] = GetBitmapURL(inv * 1000 / capa, olname);
                        }
                        else
                        {
                            if (capa == 0)
                                dTankRow[(int)ResultTblCol.Tank1 + k * 2 + 1] = GetBitmapURL2(1000);
                            else
                                dTankRow[(int)ResultTblCol.Tank1 + k * 2 + 1] = GetBitmapURL2(inv * 1000 / capa);
                        }

                    }
                    ResultBoxTable.Rows.Add(dTankRow);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            Console.WriteLine("Done");

        }

        public string GetSkkcodeFromChkTbl(int lineno)
        {
            //return cptable.GetSKKCode(lineno+1);
            return "";
        }
        public string GetDevidFromChkTbl(int lineno)
        {
            //return cptable.GetDevid(lineno+1);
            return "";
        }

        
    }
}