﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkkMaster.Master" AutoEventWireup="true" CodeBehind="SkkDefault.aspx.cs" Inherits="WebApplication.SkkDefault" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AppView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Splitter" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.C1WebChart.4" namespace="C1.Web.C1WebChart" tagprefix="C1WebChart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel1" runat="server" BackColor="#102C44" ForeColor="#99CCFF" Height="30px">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 　<asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="ログアウト" />
        <br />
        <br />
    </asp:Panel>
    <br />
    <wijmo:C1Tabs ID="C1Tabs1" runat="server" AutoPostBack="True" Height="4200px" OnSelectedChanged="C1Tab1_SelectedChanged">
        <Pages>
            <wijmo:C1TabPage runat="server" Text="在庫一覧" StaticKey="" ID="C1Tabs1_Tab1">
                <asp:Label ID="Label15" runat="server" Font-Size="Large" Text="施設を選択し表示をスクロールします。　"></asp:Label>
                <asp:ListBox ID="ZaikoSelectLB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ZaikoSelectLB_SelectedIndexChanged" Rows="1" Width="200px"></asp:ListBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="NxtPageBtn" runat="server" OnClick="NxtPageBtn_Click" Text="次ページ" />
                　<asp:Button ID="PrvPageBtn" runat="server" OnClick="PrvPageBtn_Click" Text="前ページ" />
                <br />
                <br />
                <asp:Button ID="ZDownloadBtn" runat="server" OnClick="ZDownloadBtn_Click1" Text="ダウンロード" />
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ1" runat="server" Font-Size="X-Large" Text="昭和機器工業SS  A000100001"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ1" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ1" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="100px">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="150px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ1" runat="server" Height="289px" LastDesignUpdate="636664672190066563" Width="499px" ImageRenderMethod="File">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ2" runat="server" Font-Size="X-Large" Text="昭和機器工業SS2  A000100002"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ2" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ2" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ2" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706206700401" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ3" runat="server" Font-Size="X-Large" Text="昭和機器工業SS3  A000100003"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ3" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ3" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ3" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ4" runat="server" Font-Size="X-Large" Text="昭和機器工業SS4  A000100004"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ4" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ4" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ4" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ5" runat="server" Font-Size="X-Large" Text="昭和機器工業SS5  A000100005"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ5" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ5" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ5" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ6" runat="server" Font-Size="X-Large" Text="昭和機器工業SS6  A000100006"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ6" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ6" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ6" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ7" runat="server" Font-Size="X-Large" Text="昭和機器工業SS7  A000100007"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ7" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ7" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ7" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ8" runat="server" Font-Size="X-Large" Text="昭和機器工業SS8  A000100008"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ8" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ8" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ8" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664709285661271" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ9" runat="server" Font-Size="X-Large" Text="昭和機器工業SS9  A000100009"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ9" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ9" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ9" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664709285661271" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ10" runat="server" Font-Size="X-Large" Text="昭和機器工業SS9  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ10" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ10" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ10" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664709285661271" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs1_Tab2" runat="server" StaticKey="" Text="在庫履歴（タンクごと）" TabIndex="1">
                <asp:Label ID="SSNameLabelTH" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="昭和機器工業 A000100001"></asp:Label>
                &nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" Text="施設"></asp:Label>
                &nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="SelectSSDDL" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectSSDDL_SelectIndexChanged">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" Text="タンク"></asp:Label>
                　　<asp:DropDownList ID="SelectTankDDL" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectTankDDL_SelectIndexChanged">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="在庫履歴ダウンロード" />
                <br />
                <br />
                <br />
                <asp:Label ID="SelectTankLabel0" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="半月間の推移（タンク毎)     タンク1 ハイオク(20kL)"></asp:Label>
                <br />
                <br />
                <asp:Label ID="THHisDateLabel" runat="server" Font-Size="X-Large" Text="2018/7/5 15:30"></asp:Label>
                <br />
                <br />
                <br />
                <C1WebChart:C1WebChart ID="C1WebChartTH1" runat="server" Height="221px" ImageRenderMethod="File" LastDesignUpdate="637236846066464667" Width="564px">
                    <Serializer Value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Far;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate90;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5&quot; Min=&quot;1&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;25&quot; Min=&quot;5&quot; UnitMajor=&quot;5&quot; UnitMinor=&quot;2.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;5&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;East&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                </C1WebChart:C1WebChart>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs1_Tab3" runat="server" StaticKey="" Text="在庫履歴（液種ごと）" TabIndex="2">
                <asp:Label ID="Label3" runat="server" Text="施設"></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="SelectOHSSList" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="SSNameLabelOH" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="昭和機器工業SS M000100001"></asp:Label>
                <br />
                <br />
                &nbsp;<asp:Button ID="SelectRegularButton" runat="server" BackColor="#FFFFCC" OnClick="SelectRegularButton_Click" Text="レギュラー" />
                　<asp:Button ID="SelectPremiumButton" runat="server" BackColor="#FFFFCC" OnClick="SelectPremiumButton_Click" Text="ハイオク" />
                &nbsp;&nbsp;
                <asp:Button ID="SelectDieselButton" runat="server" BackColor="#FFFFCC" OnClick="SelectDieselButton_Click" Text="軽油" />
                　<asp:Button ID="SelectKeroseneButton" runat="server" BackColor="#FFFFCC" OnClick="SelectKeroseneButton_Click" Text="灯油" />
                <br />
                <br />
                <asp:Label ID="OilTypeLabel" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="半月間の推移（液種毎）レギュラー (30kL)"></asp:Label>
                <br />
                <br />
                <br />
                <C1WebChart:C1WebChart ID="C1WebChartOH1" runat="server" Height="341px" ImageRenderMethod="File" LastDesignUpdate="637236846066554179" Width="1099px">
                    <Serializer Value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Far;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate90;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5&quot; Min=&quot;1&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;8&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;East&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                </C1WebChart:C1WebChart>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterOH1" runat="server" Height="200px" SplitterDistance="523" Width="948px">
                    <Panel1>
                        <ContentTemplate>
                            <asp:Label ID="Label13" runat="server" Font-Size="Large" Text="本日の在庫管理データ"></asp:Label>
                            <br />
                            <wijmo:C1GridView ID="C1GridViewOH1" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: 1px; left: 5px; margin-top: 0px" Width="520px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="販売量" HeaderText="販売量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="荷受け量" HeaderText="荷受け量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <asp:Label ID="Label14" runat="server" Font-Size="Large" Text="平均販売量(過去2週間)と在庫日数"></asp:Label>
                            <br />
                            <wijmo:C1GridView ID="C1GridViewOH2" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" Width="420px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="平均販売量" HeaderText="平均販売量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫日数" HeaderText="在庫日数(日)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" />
</asp:Content>
