﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Models;
using System.IO;
using System.Drawing;
using System.Web.Security;
using System.Data;
using System.Net;
using System.Diagnostics;
namespace WebApplication
{
    public partial class SkkDefault : System.Web.UI.Page
    {
        SiraHistory SHistDat;
        SiteInfData SiteDat;
        C1.Web.Wijmo.Controls.C1GridView.C1GridView[] C1GVArr;
        C1.Web.C1WebChart.C1WebChart[] C1WCArr;
        C1.Web.Wijmo.Controls.C1Splitter.C1Splitter[] C1SPArr;
        Label[] SSNLArr;

        string skkcode;
        string sitename;
        int sitenum;
        int companyno;
        int tankno;
        const int NUMSSPERPAGE = 10;
        int invpageno;
        bool showwrgonly;
        LeveCompanyProfile lvcprof;

        protected void Page_Load(object sender, EventArgs e)
        {
            showwrgonly = false;
            if ((GlobalVar.loginreq == true) && (User.Identity.IsAuthenticated == false))
            {
                Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
                Session[GlobalVar.SDispWrgOnly] = false;
                Response.Redirect("Account\\Login.aspx");
            }
            try
            {
                C1.Web.Wijmo.Controls.C1Tabs.C1TabPageCollection c1tabs = C1Tabs1.Pages;
                c1tabs.RemoveAt(2);
            }
            catch (Exception ex)
            {
                ;
            }
            UserAccount ust = new UserAccount();                //ログインアカウントに対する会社名取り出し
            string compname = ust.OpenTable(User.Identity.Name);
            lvcprof = new LeveCompanyProfile();                 //会社名に対応する会社情報を開き会社番号を取り出す
            lvcprof.OpenTable(compname);

            Session[GlobalVar.SCompanyNo] = lvcprof.compno;
            int dspno;
            try
            {                                                   //セッション情報の取り出し or 初期設定
                dspno = (int)Session[GlobalVar.SDisplayNo];
                showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
                invpageno = (int)Session[GlobalVar.SIPageNo];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                showwrgonly = false;
                Session[GlobalVar.SDispWrgOnly] = false;
                dspno = GlobalVar.TabTableNo;
                C1Tabs1.Selected = 0;
            }
            DataUnitTable dtu = new DataUnitTable();
            dtu.OpenTableSkkcodesMbl(lvcprof.mrdefcode);                   //会社名に対応するSSデータ一覧を取り出す

            //string text = C1Tabs2.SelectedPage.Text;
            if (dspno == GlobalVar.TabTableNo) //表＆グラフ
            {
                if (Page.IsPostBack == false)
                {
	                SetTableComponents();   //表示コントロール設定
    	            GetSiteSkkcode();       //SKKコード取得
                    SetZaikoSelectLB();     //在庫表、グラフ表示
        	        ShowTableZ(sitenum);    //表グラフ表示
        	    }
                Page.DataBind();
                AdjustTableLayoutZ();
                RedrawTimerSet();       //再描画タイマーセット
                SetDispSession(GlobalVar.TabTableNo);//表&グラフタブセット
            }
            else if (dspno == GlobalVar.TabSSTHisNo) //タンク別履歴
            {
                if (Page.IsPostBack == false)
                {
                    GetSiteSkkcode();   //SKKコード取得
                    SetDropDownList(SelectSSDDL, sitename);     //drop down list 追加
                    SelectTankDDL.Items.Clear();
                    SiteDat = new SiteInfData();
                    int tno = SiteDat.OpenTableSkkcode(skkcode);
                    for (int i = 1; i <= tno; i++)          //タンクドロップダウンリスト作成
                    {
                        SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
                    }
                }
                else
                {
                    GetSiteSkkcode();
                }
                ShowSSTankHistory(sitename, skkcode, tankno);   //タンク毎の在庫履歴表示

                DateTime dt = DateTime.Now.ToLocalTime();
                THHisDateLabel.Text = dt.ToString("yyyy/M/d HH") + ":00";   //時刻ラベル表示
                Timer1.Enabled = false;
                Page.DataBind();
                SetDispSession(GlobalVar.TabSSTHisNo);
            }
            else if (dspno == GlobalVar.TabSSOHisNo)//液種別履歴
            {
                if (Page.IsPostBack == false)
                {
                    GetSiteSkkcode();                               //SKKコード取得
                    SetDropDownList(SelectOHSSList, sitename);      //施設リストボックス
                    string oltype = "レギュラー";
                    Session[GlobalVar.SOiltype] = oltype;

                    ShowSSOilHistory(sitename, skkcode, oltype);    //液種ごとの履歴表示
                }
                else
                {
                    GetSiteSkkcode();
                }
                Timer1.Enabled = false;
                Page.DataBind();
                SetDispSession(GlobalVar.TabSSOHisNo);
            }
            AddMetaHead();
        }
        //自動ログアウト設定ヘッダー追加
        protected void AddMetaHead()
        {
            System.Web.UI.HtmlControls.HtmlHead head = Page.Header;
            System.Web.UI.HtmlControls.HtmlMeta meta = new System.Web.UI.HtmlControls.HtmlMeta();

            meta.HttpEquiv = "Refresh";
            meta.Content = "900;URL= Account/Login.aspx"; //10分後にログアウト
            //meta.Content = "60;URL= Account/Login.aspx"; //10分後にログアウト
            head.Controls.Add(meta);
        }

        //----------------------- タブ変更 ------------------------------------------------
        protected void C1Tab1_SelectedChanged(object sender, EventArgs e)
        {
            string accname = User.Identity.Name;
            string pagestr = ((C1.Web.Wijmo.Controls.C1Tabs.C1Tabs)sender).SelectedPage.Text;
            GetCompanyProfile();
            if (pagestr == GlobalVar.TabTable) //表＆グラフ
            {
                SetTableComponents();           //表示コンポーネントセット
                Session[GlobalVar.SDispWrgOnly] = false;
                SetDefSiteSkkcode();            //SKKコード取り出し

                ShowTableZ(sitenum);        //在庫画面表グラフ表示
                Page.DataBind();
                AdjustTableLayoutZ();
                RedrawTimerSet();           //画面更新タイマーセット
                SetDispSession(GlobalVar.TabTableNo);
            }
            else if (pagestr == GlobalVar.TabSSTHistory) //タンク別履歴
            {
                SetDefSiteSkkcode();
                SetDropDownList(SelectSSDDL, sitename);     //施設名リストセット
                SelectTankDDL.Items.Clear();
                SiteDat = new SiteInfData();
                int totaltno = SiteDat.OpenTableSkkcode(skkcode);
                SelectTankDDL.Items.Clear();                //タンク番号ドロップダウンリストセット
                for (int i = 1; i <= totaltno; i++)
                {
                    SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
                }
                SSNameLabelTH.Text = sitename + " " + skkcode;  //施設名表示
                DateTime dt = DateTime.Now.ToLocalTime();
                THHisDateLabel.Text = dt.ToString("yyyy/M/d HH") + ":00";　//時刻表示
                ShowSSTankHistory(sitename, skkcode, tankno);   //タンク履歴表示
                Page.DataBind();
                Timer1.Enabled = false;
                SetDispSession(GlobalVar.TabSSTHisNo);
            }
            else if (pagestr == GlobalVar.TabSSOHistory) //液種別履歴
            {
                SetDefSiteSkkcode();            //SKKコード取り出し
                SetDropDownList(SelectOHSSList, sitename);     //施設選択リストボックスセット

                string oltype = "レギュラー";
                Session[GlobalVar.SOiltype] = oltype;

                Timer1.Enabled = false;
                ShowSSOilHistory(sitename, skkcode, oltype);    //液種別履歴表示
                SSNameLabelOH.Text = sitename + " " + skkcode;  //施設名表示

                Page.DataBind();
                SetDispSession(GlobalVar.TabSSOHisNo);
            }
            AddMetaHead();
        }

        //----------------------- 表表示画面処理 ------------------------------------------------
        protected void ShowTableZ(int sitenum)　//表表示処理
        {
            showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
            Session[GlobalVar.SCompanyName] = lvcprof.compname2;
            ZaikoDataTable dtbl;
            foreach (string file in Directory.GetFiles(HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\", "*.png"))
            {               //Component One temporaryファイル削除
                File.Delete(file);
            }
            DataUnitTable dttable = new DataUnitTable();        //施設一覧を指定会社に対してオープン
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);

            int numsite = dttable.GetNumOfRecord(); //施設数
            int i, j=0;
            string tmstr = "";

            for (i = sitenum, j=0; i < numsite; i++)     //各SSに対して処理実行
            {
                try
                {
                    string skkcode = dttable.GetSSCode(i);      //SKKコード取り出し
                    dtbl = new ZaikoDataTable(lvcprof.compname2, skkcode);
                    tmstr = dtbl.CreateZaikoTable(showwrgonly);
                    dtbl.CreateTankOilZaikoTablePerSS();        //各施設に対する在庫表作成
                    ShowTableComponent(j);
                    DataTable dtb = dtbl.GetTankTable();
                    C1GVArr[j].DataSource = dtb;                //在庫表表示
                    DrawGraph.ShowSSGraph(C1WCArr[j], dtbl, skkcode);   //在庫グラフ作成、表示
                    if( ( tmstr != "" ) && ( tmstr.Length >= 10 ) )
	                    tmstr = tmstr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                    SSNLArr[j].Text = dttable.GetSSName(i) + " " + tmstr;   //SS名、時間表示
                    SSNLArr[j].Visible = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                j++;
                if (j >= NUMSSPERPAGE)
                    break;
            }
            if (j == 0)
            {
                if (showwrgonly == true)
                    WebResponseCtrl.WriteResponse(Response, "警報発生中の施設はありません。");
            }
            if (j < NUMSSPERPAGE)
            {
                for (; j < NUMSSPERPAGE; j++)
                    HideTableComponent(j);
            }
            Session[GlobalVar.SSortby] = "タンク";
        }

        protected void AdjustTableLayoutZ()
        {
            string str;
            try
            {       //Arrayの実態がないときに終了するため
                bool bchk = C1GVArr[0].DisplayVisible;
            }
            catch (Exception ex)
            {
                return;
            }

            for (int j = 0; j < NUMSSPERPAGE; j++)
            {
                if (C1GVArr[j].DisplayVisible == true)
                {
                    for (int i = 0; i < C1GVArr[j].Rows.Count; i++)
                    {
                        C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow drow = C1GVArr[j].Rows[i];

                        str = drow.Cells[5].Text;
                        if (str != GlobalVar.TankNoError)
                        {
                            C1GVArr[j].Rows[i].Cells[GlobalVar.TankStatColNo].ForeColor = Color.Red;
                        }
                    }
                }
            }
        }

        //施設選択用リストボックス設定
        protected void SetZaikoSelectLB()
        {
            try
            {
                ZaikoSelectLB.Items.Clear();       
                DataUnitTable dttable = new DataUnitTable();
                //現在の会社のログインアカウントに対するデータテーブルをオープン
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);
                List<string> ssnames = dttable.GetSiteList();   //SS名一覧を取り出す
                ZaikoSelectLB.DataSource = ssnames; //リストボックスに設定
                string[] ssar = ssnames.ToArray();
                ZaikoSelectLB.Text = ssar[0];
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //再描画用タイマー設定
        protected void RedrawTimerSet()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            bool btmset = false;
            int min = dt.Minute;
            int wtmin = 0;
#if false
            if (min < 5)  //毎時 0-4分
            {
                wtmin = 5 - min;
                btmset = true;
            }
            else if ((min > 20) && (min < 35)) //毎時21-34分
            {
                wtmin = 35 - min;
                btmset = true;
            }
            else if (min > 50) //毎時51-59分
            {
                wtmin = 5 + (60 - min);
                btmset = true;
            }
            if (btmset == true)
            {
                wtmin = wtmin * 1000 * 60;
                Timer1.Enabled = true;
                Timer1.Interval = wtmin;
            }
            else
            {
                Timer1.Enabled = false;
            }
#else   //for debug
            Timer1.Enabled = true;
            //Timer1.Interval = 60000;
            Timer1.Interval = 600000;
#endif
        }

        //画面更新タイマー満了処理
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            string sessionstr = (string)Session[GlobalVar.SComStatus];
            int dspno = (int)Session[GlobalVar.SDisplayNo];
            Timer1.Enabled = false;
            GetCompanyProfile();
            if (dspno == GlobalVar.TabTableNo)  //在庫表示画面表示中なら画面更新
            {
                SetTableComponents();
                GetSiteSkkcode();
                SetZaikoSelectLB();               //施設リスト表示
                int siteno = (int)Session[GlobalVar.SSiteNo];
                ShowTableZ(siteno);             //在庫画面表グラフ表示
                Page.DataBind();
                AdjustTableLayoutZ();
                RedrawTimerSet();               //再度画面更新タイマーセット
                Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
                return;
            }
        }

        //在庫ダウンロードボタン
        protected void ZDownloadBtn_Click1(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            try
            {
                string sitename = Session[GlobalVar.SSiteName].ToString().TrimEnd();
                GetCompanyProfile();

                string sDownloadFileName = "在庫" + dt.ToString("yyyyMMdd") + ".csv";
                //現在の会社の指定アカウントに対するすべての施設のデータテーブルをオープン
                DataUnitTable dttable = new DataUnitTable();
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);
                int numsite = dttable.GetNumOfRecord();     //施設数
                DateTime dtnow = DateTime.Now.ToLocalTime();
                if (numsite > 0)
                {
                    string sendtext = "";
                    sendtext += ZaikoDataTable.GetInventoryStringHeader();
                    for (int i = 0; i < numsite; i++)   //各施設ごとにSKKコードを取り出し、在庫表を作成
                    {
                        string skkcode = dttable.GetSSCode(i);
                        ZaikoDataTable dtbl = new ZaikoDataTable(lvcprof.mrdefcode, skkcode);
                        sendtext += dtbl.GetInventoryString();  //在庫文字列を取り出す
                    }
                    WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);  //取り出した文字列をダウンロード
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "データがありません");
                    return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //施設切り替え
        protected void ZaikoSelectLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            sitename = ZaikoSelectLB.Text;      //選択された施設名取り出し
            GetCompanyProfile();

            //選択された施設に対するデータテーブルを開く
            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);
            skkcode = dttable.GetSSCodeBySitename(sitename);
            sitenum = dttable.GetSSCodeBySitenum(sitename);
            Session[GlobalVar.SSiteName] = sitename;
            Session[GlobalVar.SSkkcode] = skkcode;
            Session[GlobalVar.SSiteNo] = sitenum;
            int numpage = (sitenum - 1) / NUMSSPERPAGE;
            Session[GlobalVar.SIPageNo] = numpage;
            SetTableComponents();

            ShowTableZ(sitenum);       //選択された施設の表、グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();
        }

        //次ページボタンクリック時
        protected void NxtPageBtn_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            invpageno = (int)Session[GlobalVar.SIPageNo];
            DataUnitTable dttable = new DataUnitTable();    //現在のアカウントで表示可能なSS一覧取得
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);
            int numsite = dttable.GetNumOfRecord();
            int numpage = (numsite - 1) / NUMSSPERPAGE;
            invpageno = invpageno + 1;
            if (invpageno > numpage)
            {
                WebResponseCtrl.WriteResponse(Response, "最後のページです");
                return;
            }
            Session[GlobalVar.SIPageNo] = invpageno;
            Session[GlobalVar.SSiteNo] = sitenum = NUMSSPERPAGE * invpageno;

            SetTableComponents();
            SetZaikoSelectLB();  //施設リスト表示
            ShowTableZ(sitenum); //表＆グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();           //再描画タイマーセット
            SetDispSession(GlobalVar.TabTableNo);
        }

        //前ページボタンクリック時
        protected void PrvPageBtn_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            invpageno = (int)Session[GlobalVar.SIPageNo];
            if (invpageno == 0)
            {
                WebResponseCtrl.WriteResponse(Response, "先頭のページです");
                return;
            }
            invpageno = invpageno - 1;
            Session[GlobalVar.SIPageNo] = invpageno;
            Session[GlobalVar.SSiteNo] = sitenum = NUMSSPERPAGE * invpageno;

            SetTableComponents();
            SetZaikoSelectLB();  //施設リスト表示
            ShowTableZ(sitenum); //表＆グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();           //再描画タイマーセット
            SetDispSession(GlobalVar.TabTableNo);
        }
        //----------------------- タンク毎施設履歴表示画面処理 ------------------------------------------------
        protected void ShowSSTankHistory(string sitename, string skkcode, int Tankno)
        {
            try
            {
                foreach (string file in Directory.GetFiles(HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\", "*.png"))
                {
                    File.Delete(file);
                }
                //タンク毎の履歴のグラフ表示
                DrawGraph.ShowChartTankHistory(C1WebChartTH1, THHisDateLabel, SelectTankLabel0, sitename, skkcode, Tankno, false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //TankLabel.Text = "在庫履歴表示: " + sitename;
                //TankLabel.Text += "在庫データが無効です";
            }
        }

        //タンク番号変更時の再描画
        protected void SelectTankDDL_SelectIndexChanged(object sender, EventArgs e)
        {
            GetCompanyProfile();
            DropDownList list = (DropDownList)sender;
            skkcode = (string)Session[GlobalVar.SSkkcode];
            string tankno = list.SelectedValue; 

            int tno = int.Parse(tankno);        //選択されたタンク番号
            Session[GlobalVar.STankNo] = tno;
            skkcode = (string)Session[GlobalVar.SSkkcode];
            sitename = (string)Session[GlobalVar.SSiteName]; //現在のSKKコード、施設名をSessionより取り出す
            SSNameLabelTH.Text = sitename + " " + skkcode;
            ShowSSTankHistory(sitename, skkcode, tno);  //タンク履歴表示
            Page.DataBind();
        }

        //施設一覧リストボックス表示
        protected void SetDropDownList(DropDownList ddlist, string ssname)
        {
            int idx = 0;
            string ssnmstr;
            ddlist.Items.Clear();
            DataUnitTable dttable = new DataUnitTable();
            
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode); //指定会社に対するデータテーブルを開く
            List<string> ssnames = dttable.GetSiteList();   //SS一覧を取得
            int i = 0;
            foreach (string ssnm in ssnames)        //各SS毎をドロップダウンリストに追加
            {
                ssnmstr = ssnm.TrimEnd();
                if (ssnmstr == ssname)
                    idx = i;
                ddlist.Items.Add(new ListItem(ssnmstr, i.ToString()));
                i++;
            }
            ddlist.SelectedIndex = idx;
        }

        //施設名変更時の再表示
        protected void SelectSSDDL_SelectIndexChanged(object sender, EventArgs e)
        {
            DropDownList list = (DropDownList)sender;
            int siteno = int.Parse(list.SelectedValue); //選択された施設の施設番号
            GetCompanyProfile();

            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode); //施設一覧を指定会社に対してオープン
            sitename = dttable.GetSSName(siteno);
            skkcode = dttable.GetSSCode(siteno);        //施設番号より施設名、SSコードを取り出す

            SiteDat = new SiteInfData();
            int tno = SiteDat.OpenTableSkkcode(skkcode);    //指定施設の施設情報をオープンしタンク数を取り出す
            SelectTankDDL.Items.Clear();
            for (int i = 1; i <= tno; i++)          //タンク数分タンク番号リスト作成
            {
                SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
            }

            SSNameLabelTH.Text = sitename + " " + skkcode;
            Session[GlobalVar.STankNo] = 1;
            Session[GlobalVar.SSiteNo] = siteno;
            Session[GlobalVar.SSiteName] = sitename;
            Session[GlobalVar.SSkkcode] = skkcode;
            ShowSSTankHistory(sitename, skkcode, 1);    //タンク履歴表示
            Page.DataBind();
        }

        //在庫履歴ダウンロード
        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dtnow = DateTime.Now;                          //起点となる日付
                DateTime dt = dtnow.AddDays(-31);                       //直近31日分の履歴を表示するため31日前を起点とする
                string sendtext = "";

                //施設選択
                //sitename = SelectSSDDL.Text;    //施設名取り出し
                sitename = (string)Session[GlobalVar.SSiteName];
                string sDownloadFileName = sitename + "施設履歴" + dtnow.ToString("yyyyMMdd") + ".csv"; //ダウンロードするファイル名
                companyno = (int)Session[GlobalVar.SCompanyNo];
                DataUnitTable dttable = new DataUnitTable();    //指定施設名のデータを開く
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode);
                string skcode = dttable.GetSSCodeBySitename(sitename);  //SKKコード取り出し
                if (skcode != "")
                {
                    //ヘッダー "集信日時,タンク番号,液種,全容量,在庫,水位,水量\r\n"
                    sendtext += ZaikoDataTable.GetInventoryStringHeaderWOSSName();
                    ZHistData zhist = new ZHistData();
                    zhist.OpenTableSkkcodeByMonth(skcode);  //指定SSの在庫履歴を開く
                    sendtext += zhist.GetInventroyHistoryStringWOWtr(sitename, skcode);  //ダウンロード文字列に指定SSの在庫履歴追加 (水情報はすべて0とする)
                    WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "データがありません");
                    return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //----------------------- 液種毎施設履歴表示画面処理 ------------------------------------------------
        protected void ShowSSOilHistory(string sitename, string skkcode, string oltype)
        {
            SiteDat = new SiteInfData();
            SiteDat.OpenTableSkkcode(skkcode);
            try
            {
                foreach (string file in Directory.GetFiles(HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\", "*.png"))
                {       //ComponentOne グラフ表示用のテンポラリファイル削除
                    File.Delete(file);
                }
                //液種ごと履歴グラフ作成、表示
                DateTime spcdt = DateTime.Now;
                DrawGraph.ShowChartOilHistory(C1WebChartOH1, OilTypeLabel, sitename, skkcode, oltype, true, spcdt);

                //本日の在庫管理データ
                SHistDat = new SiraHistory();
                SHistDat.OpenTableSkkcodeRecord(skkcode);
                DateTime kurodt = SHistDat.BuildLatestKurohonDT(skkcode);   //本日の在庫表作成、表示
                DateTime dt = DateTime.Now.ToLocalTime();
                if (dt.ToString("yyyyMMdd") == kurodt.ToString("yyyyMMdd"))
                    Label13.Text = "本日の在庫管理データ";
                else
                    Label13.Text = kurodt.ToString("M月d日") + "の在庫管理データ";
                C1GridViewOH1.DataSource = SHistDat.GetKurohonTable();

                //過去ひと月分の平均
                SiraHistory shis1m = new SiraHistory();
                shis1m.OpenTableSkkcodeRecordPastMonth(skkcode);    //過去1月分の平均販売量表作成
                shis1m.BuildAveSalesTableDT(skkcode);
                C1GridViewOH2.DataSource = shis1m.GetAveSalesTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //TankLabel.Text = "在庫履歴表示: " + sitename;
                //TankLabel.Text += "在庫データが無効です";
            }
        }

        //油種毎の履歴表示
        private void ShowOilHistory(string oltype)
        {
            companyno = (int)Session[GlobalVar.SCompanyNo];
            GetSiteSkkcode();
            ShowSSOilHistory(sitename, skkcode, oltype);    //指定油種の履歴のグラフ表示

            SSNameLabelOH.Text = sitename + " " + skkcode;
            SHistDat = new SiraHistory();
            SHistDat.OpenTableSkkcodeRecord(skkcode);   //施設油種の履歴を取り出す

            DateTime kurodt = SHistDat.BuildLatestKurohonDT(skkcode); //最新の黒本(在庫、販売、荷下ろし)データ作成
            DateTime dt = DateTime.Now.ToLocalTime();
            if (dt.ToString("yyyyMMdd") == kurodt.ToString("yyyyMMdd"))
                Label13.Text = "本日の在庫管理データ";
            else
                Label13.Text = kurodt.ToString("M月d日") + "の在庫管理データ";
            C1GridViewOH1.DataSource = SHistDat.GetKurohonTable();  //黒本データを表表示

            SHistDat.BuildAveSalesTableDT(skkcode);
            C1GridViewOH2.DataSource = SHistDat.GetAveSalesTable(); //平均販売量表作成、表表示
            Page.DataBind();
        }

        //レギュラー選択
        protected void SelectRegularButton_Click(object sender, EventArgs e)
        {
            string oltype = "レギュラー";
            Session[GlobalVar.SOiltype] = oltype;
            ShowOilHistory(oltype);
        }

        //ハイオク選択
        protected void SelectPremiumButton_Click(object sender, EventArgs e)
        {
            string oltype = "ハイオク";
            Session[GlobalVar.SOiltype] = oltype;
            ShowOilHistory(oltype);
        }

        //軽油選択
        protected void SelectDieselButton_Click(object sender, EventArgs e)
        {
            string oltype = "軽油";
            Session[GlobalVar.SOiltype] = oltype;
            ShowOilHistory(oltype);
        }

        //灯油選択
        protected void SelectKeroseneButton_Click(object sender, EventArgs e)
        {
            string oltype = "灯油";
            Session[GlobalVar.SOiltype] = oltype;
            ShowOilHistory(oltype);
        }

        //会社番号、情報取り出し
        protected void GetCompanyProfile()
        {
            companyno = (int)Session[GlobalVar.SCompanyNo];
            lvcprof = new LeveCompanyProfile();
            lvcprof.OpenTableById(companyno);
        }

        //-------- ボタン処理 -------------
        //ログアウト
        protected void Button2_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
            FormsAuthentication.SignOut();
            Response.Redirect("~/Account/Login.aspx");
        }

        //-------- セッション処理 -------------
        protected void GetSiteSkkcode()
        {
            try
            {
                sitename = (string)Session[GlobalVar.SSiteName];
                skkcode = (string)Session[GlobalVar.SSkkcode];
                sitenum = (int)Session[GlobalVar.SSiteNo];
                invpageno = (int)Session[GlobalVar.SIPageNo];
                tankno = (int)Session[GlobalVar.STankNo];
            }
            catch (Exception ex)
            {
                SetDefSiteSkkcode();
            }
        }
        protected void SetDefSiteSkkcode()
        {
            try
            {
                Session[GlobalVar.SSiteName] = sitename = lvcprof.defssname;
                Session[GlobalVar.SSkkcode] = skkcode = lvcprof.defskkcode;
                Session[GlobalVar.SSiteNo] = sitenum = 0;
                Session[GlobalVar.STankNo] = tankno = 1;
                Session[GlobalVar.SIPageNo] = invpageno = 0;
            }
            catch (Exception ex)
            {
                SetDefSiteSkkcode();
            }
        }
        protected void SetDispSession(int dispno)
        {
            Session[GlobalVar.SDisplayNo] = dispno;
            Session[GlobalVar.SDispWrgOnly] = false;
        }

        //------------------ 表示コンポーネントの処理 ----------------
        //コンポーネントの表示
        protected void ShowTableComponent(int num)
        {
            if ((num >= 0) && (num < NUMSSPERPAGE))
            {
                C1GVArr[num].DisplayVisible = true;
                C1WCArr[num].Visible = true;
                C1SPArr[num].DisplayVisible = true;
                SSNLArr[num].Visible = true;
            }
        }
        //コンポーネントの非表示
        protected void HideTableComponent(int num)
        {
            if ((num >= 0) && (num < NUMSSPERPAGE))
            {
                C1GVArr[num].DisplayVisible = false;
                C1WCArr[num].Visible = false;
                C1SPArr[num].DisplayVisible = false;
                SSNLArr[num].Visible = false;
            }
        }

        //表表示に必要なコンポーネントをセット
        protected void SetTableComponents()
        {
            C1GVArr = new C1.Web.Wijmo.Controls.C1GridView.C1GridView[NUMSSPERPAGE];
            C1WCArr = new C1.Web.C1WebChart.C1WebChart[NUMSSPERPAGE];
            C1SPArr = new C1.Web.Wijmo.Controls.C1Splitter.C1Splitter[NUMSSPERPAGE];
            SSNLArr = new Label[NUMSSPERPAGE];
            //site1
            C1GVArr[0] = C1GridViewZ1;
            C1WCArr[0] = C1WebChartZ1;
            C1SPArr[0] = C1SplitterZ1;
            SSNLArr[0] = SSNameLabelZ1;
            C1GVArr[0].DisplayVisible = false;
            C1WCArr[0].Visible = false;
            C1SPArr[0].DisplayVisible = false;
            SSNLArr[0].Visible = false;
            //site2

            C1GVArr[1] = C1GridViewZ2;
            C1WCArr[1] = C1WebChartZ2;
            C1SPArr[1] = C1SplitterZ2;
            SSNLArr[1] = SSNameLabelZ2;
            C1GVArr[1].DisplayVisible = false;
            C1WCArr[1].Visible = false;
            C1SPArr[1].DisplayVisible = false;
            SSNLArr[1].Visible = false;
            //site3
            C1GVArr[2] = C1GridViewZ3;
            C1WCArr[2] = C1WebChartZ3;
            C1SPArr[2] = C1SplitterZ3;
            SSNLArr[2] = SSNameLabelZ3;
            C1GVArr[2].DisplayVisible = false;
            C1WCArr[2].Visible = false;
            C1SPArr[2].DisplayVisible = false;
            SSNLArr[2].Visible = false;
            //site4
            C1GVArr[3] = C1GridViewZ4;
            C1WCArr[3] = C1WebChartZ4;
            C1SPArr[3] = C1SplitterZ4;
            SSNLArr[3] = SSNameLabelZ4;
            C1GVArr[3].DisplayVisible = false;
            C1WCArr[3].Visible = false;
            C1SPArr[3].DisplayVisible = false;
            SSNLArr[3].Visible = false;
            //site5
            C1GVArr[4] = C1GridViewZ5;
            C1WCArr[4] = C1WebChartZ5;
            C1SPArr[4] = C1SplitterZ5;
            SSNLArr[4] = SSNameLabelZ5;
            C1GVArr[4].DisplayVisible = false;
            C1WCArr[4].Visible = false;
            C1SPArr[4].DisplayVisible = false;
            SSNLArr[4].Visible = false;
            //site6
            C1GVArr[5] = C1GridViewZ6;
            C1WCArr[5] = C1WebChartZ6;
            C1SPArr[5] = C1SplitterZ6;
            SSNLArr[5] = SSNameLabelZ6;
            C1GVArr[5].DisplayVisible = false;
            C1WCArr[5].Visible = false;
            C1SPArr[5].DisplayVisible = false;
            SSNLArr[5].Visible = false;
            //site7
            C1GVArr[6] = C1GridViewZ7;
            C1WCArr[6] = C1WebChartZ7;
            C1SPArr[6] = C1SplitterZ7;
            SSNLArr[6] = SSNameLabelZ7;
            C1GVArr[6].DisplayVisible = false;
            C1WCArr[6].Visible = false;
            C1SPArr[6].DisplayVisible = false;
            SSNLArr[6].Visible = false;
            //site8
            C1GVArr[7] = C1GridViewZ8;
            C1WCArr[7] = C1WebChartZ8;
            C1SPArr[7] = C1SplitterZ8;
            SSNLArr[7] = SSNameLabelZ8;
            C1GVArr[7].DisplayVisible = false;
            C1WCArr[7].Visible = false;
            C1SPArr[7].DisplayVisible = false;
            SSNLArr[7].Visible = false;
            //site9
            C1GVArr[8] = C1GridViewZ9;
            C1WCArr[8] = C1WebChartZ9;
            C1SPArr[8] = C1SplitterZ9;
            SSNLArr[8] = SSNameLabelZ9;
            C1GVArr[8].DisplayVisible = false;
            C1WCArr[8].Visible = false;
            C1SPArr[8].DisplayVisible = false;
            SSNLArr[8].Visible = false;
            //site10
            C1GVArr[9] = C1GridViewZ10;
            C1WCArr[9] = C1WebChartZ10;
            C1SPArr[9] = C1SplitterZ10;
            SSNLArr[9] = SSNameLabelZ10;
            C1GVArr[9].DisplayVisible = false;
            C1WCArr[9].Visible = false;
            C1SPArr[9].DisplayVisible = false;
            SSNLArr[9].Visible = false;
        }

    }
}