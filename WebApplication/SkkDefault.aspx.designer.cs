﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace WebApplication {
    
    
    public partial class SkkDefault {
        
        /// <summary>
        /// Panel1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;
        
        /// <summary>
        /// Button2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button2;
        
        /// <summary>
        /// C1Tabs1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1Tabs C1Tabs1;
        
        /// <summary>
        /// C1Tabs1_Tab1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab1;
        
        /// <summary>
        /// Label15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label15;
        
        /// <summary>
        /// ZaikoSelectLB コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox ZaikoSelectLB;
        
        /// <summary>
        /// NxtPageBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button NxtPageBtn;
        
        /// <summary>
        /// PrvPageBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PrvPageBtn;
        
        /// <summary>
        /// ZDownloadBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ZDownloadBtn;
        
        /// <summary>
        /// SSNameLabelZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ1;
        
        /// <summary>
        /// C1SplitterZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ1;
        
        /// <summary>
        /// C1GridViewZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ1;
        
        /// <summary>
        /// C1WebChartZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ1;
        
        /// <summary>
        /// SSNameLabelZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ2;
        
        /// <summary>
        /// C1SplitterZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ2;
        
        /// <summary>
        /// C1GridViewZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ2;
        
        /// <summary>
        /// C1WebChartZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ2;
        
        /// <summary>
        /// SSNameLabelZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ3;
        
        /// <summary>
        /// C1SplitterZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ3;
        
        /// <summary>
        /// C1GridViewZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ3;
        
        /// <summary>
        /// C1WebChartZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ3;
        
        /// <summary>
        /// SSNameLabelZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ4;
        
        /// <summary>
        /// C1SplitterZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ4;
        
        /// <summary>
        /// C1GridViewZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ4;
        
        /// <summary>
        /// C1WebChartZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ4;
        
        /// <summary>
        /// SSNameLabelZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ5;
        
        /// <summary>
        /// C1SplitterZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ5;
        
        /// <summary>
        /// C1GridViewZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ5;
        
        /// <summary>
        /// C1WebChartZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ5;
        
        /// <summary>
        /// SSNameLabelZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ6;
        
        /// <summary>
        /// C1SplitterZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ6;
        
        /// <summary>
        /// C1GridViewZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ6;
        
        /// <summary>
        /// C1WebChartZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ6;
        
        /// <summary>
        /// SSNameLabelZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ7;
        
        /// <summary>
        /// C1SplitterZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ7;
        
        /// <summary>
        /// C1GridViewZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ7;
        
        /// <summary>
        /// C1WebChartZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ7;
        
        /// <summary>
        /// SSNameLabelZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ8;
        
        /// <summary>
        /// C1SplitterZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ8;
        
        /// <summary>
        /// C1GridViewZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ8;
        
        /// <summary>
        /// C1WebChartZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ8;
        
        /// <summary>
        /// SSNameLabelZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ9;
        
        /// <summary>
        /// C1SplitterZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ9;
        
        /// <summary>
        /// C1GridViewZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ9;
        
        /// <summary>
        /// C1WebChartZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ9;
        
        /// <summary>
        /// SSNameLabelZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ10;
        
        /// <summary>
        /// C1SplitterZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ10;
        
        /// <summary>
        /// C1GridViewZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ10;
        
        /// <summary>
        /// C1WebChartZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ10;
        
        /// <summary>
        /// C1Tabs1_Tab2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab2;
        
        /// <summary>
        /// SSNameLabelTH コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelTH;
        
        /// <summary>
        /// Label2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;
        
        /// <summary>
        /// SelectSSDDL コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList SelectSSDDL;
        
        /// <summary>
        /// Label1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// SelectTankDDL コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList SelectTankDDL;
        
        /// <summary>
        /// Button3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button3;
        
        /// <summary>
        /// SelectTankLabel0 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SelectTankLabel0;
        
        /// <summary>
        /// THHisDateLabel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label THHisDateLabel;
        
        /// <summary>
        /// C1WebChartTH1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartTH1;
        
        /// <summary>
        /// C1Tabs1_Tab3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab3;
        
        /// <summary>
        /// Label3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label3;
        
        /// <summary>
        /// SelectOHSSList コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList SelectOHSSList;
        
        /// <summary>
        /// SSNameLabelOH コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelOH;
        
        /// <summary>
        /// SelectRegularButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SelectRegularButton;
        
        /// <summary>
        /// SelectPremiumButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SelectPremiumButton;
        
        /// <summary>
        /// SelectDieselButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SelectDieselButton;
        
        /// <summary>
        /// SelectKeroseneButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SelectKeroseneButton;
        
        /// <summary>
        /// OilTypeLabel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label OilTypeLabel;
        
        /// <summary>
        /// C1WebChartOH1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartOH1;
        
        /// <summary>
        /// C1SplitterOH1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterOH1;
        
        /// <summary>
        /// Label13 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label13;
        
        /// <summary>
        /// C1GridViewOH1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewOH1;
        
        /// <summary>
        /// Label14 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label14;
        
        /// <summary>
        /// C1GridViewOH2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewOH2;
        
        /// <summary>
        /// Timer1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.Timer Timer1;
    }
}
